using System;
using System.Collections.Generic;
using Outworld.Game.Components.Visuals;
using Outworld.Game.Extensions;
using Outworld.Game.Memory;
using Outworld.Game.Utilities;
using Unity.Collections;
using UnityEngine;

namespace Outworld.Game.Weapons
{
    public class Beam : IDisposable
    {
        private const int MaxReflections = 10;

        private static NativeArray<Vector3> pointsCache = new NativeArray<Vector3>(MaxReflections, Allocator.Persistent, NativeArrayOptions.UninitializedMemory);

        public readonly List<GameObject> HitUnits = new List<GameObject>();
        public readonly List<Vector3> HitPoints = new List<Vector3>();

        private readonly float range;
        private readonly int reflectionCount;

        private readonly Configuration configuration;

        private readonly BeamComponent beam;
        private readonly VisualEffectComponent originParticles;
        private readonly VisualEffectComponent[] targetParticles;

        public Beam(float range, int reflectionCount, string beamPrefab, string originParticlesPrefab, string targetParticlesPrefab)
        {
            this.configuration = Context.Get<Configuration>();

            this.range = range;
            this.reflectionCount = reflectionCount;

            this.beam = ResourcesPool.Get<BeamComponent>(beamPrefab);
            this.beam.gameObject.SetActive(false);

            this.originParticles = ResourcesPool.Get<VisualEffectComponent>(originParticlesPrefab);
            this.originParticles.gameObject.SetActive(false);

            this.targetParticles = new VisualEffectComponent[this.reflectionCount + 1];
            for (var i = 0; i < this.targetParticles.Length; i++)
            {
                this.targetParticles[i] = ResourcesPool.Get<VisualEffectComponent>(targetParticlesPrefab);
                this.targetParticles[i].gameObject.SetActive(false);
            }
        }

        public void Dispose()
        {
            this.originParticles.Release();
            this.beam.Release();

            foreach (var targetParticle in this.targetParticles)
            {
                targetParticle.Release();
            }
        }

        public void OnStart(Transform origin, Vector2 direction, float pitch)
        {
            this.beam.gameObject.SetActive(true);
            this.originParticles.gameObject.SetActive(true);

            OnUpdate(origin, direction, pitch, 0);
        }

        public void OnUpdate(Transform origin, Vector2 direction, float pitch, float deltaTime)
        {
            this.originParticles.transform.position = origin.position;

            this.HitPoints.Clear();
            this.HitUnits.Clear();

            var rayOrigin = origin.position;
            var rayDirection = direction;
            var pointsCount = 0;

            pointsCache[0] = rayOrigin;
            pointsCount += 1;

            for (var i = 0; i < Mathf.Min(this.reflectionCount + 1, MaxReflections); i++)
            {
                var hit = Physics2D.Raycast(rayOrigin, rayDirection, this.range, this.configuration.collisionMask | this.configuration.unitMask);
                var target = hit ? hit.point : (Vector2) rayOrigin + rayDirection * this.range;

                pointsCache[pointsCount] = target;
                pointsCount += 1;

                if (hit.collider == null)
                {
                    break;
                }

                if (this.configuration.unitMask.ContainsLayer(hit.collider.gameObject.layer))
                {
                    this.HitUnits.Add(hit.collider.gameObject);
                }
                else
                {
                    this.HitPoints.Add(hit.point);
                }

                rayOrigin = target;
                rayDirection = Vector3.Reflect(rayDirection, hit.normal);

                this.targetParticles[i].transform.position = target;
            }

            for (var i = 0; i < this.targetParticles.Length; i++)
            {
                this.targetParticles[i].gameObject.SetActive(i < pointsCount);
            }

            this.beam.SetPositions(new NativeSlice<Vector3>(pointsCache, 0, pointsCount));
        }

        public void OnStop(Transform origin, Vector2 direction, float pitch)
        {
            this.beam.gameObject.SetActive(false);
            this.originParticles.gameObject.SetActive(false);

            foreach (var targetParticle in this.targetParticles)
            {
                targetParticle.gameObject.SetActive(false);
            }
        }
    }
}