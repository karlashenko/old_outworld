﻿using System.Collections.Generic;

namespace Outworld.Game.AI
{
    public abstract class BehaviourTreeNodeContainer : BehaviourTreeNode
    {
        // ReSharper disable once HeapView.ObjectAllocation.Evident
        public readonly List<BehaviourTreeNode> Nodes = new List<BehaviourTreeNode>();

        public void AddNode(BehaviourTreeNode node)
        {
            this.Nodes.Add(node);
        }
    }
}
