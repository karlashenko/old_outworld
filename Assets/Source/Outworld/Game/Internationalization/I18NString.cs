using Outworld.Game.Internationalization;
using Outworld.Game.Utilities;

namespace Outworld.Game.Localization
{
    public class I18NString
    {
        private readonly string key;
        private readonly I18n i18N;

        public I18NString(string key)
        {
            this.key = key;
            this.i18N = Context.Get<I18n>();
        }

        public override string ToString()
        {
            return this.i18N.Translate(this.key);
        }

        public static implicit operator string(I18NString i18NString)
        {
            return i18NString.ToString();
        }
    }
}