using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Outworld.Game.Components;
using UnityEngine;

namespace Outworld.Game.Memory
{
    [SuppressMessage("ReSharper", "HeapView.ObjectAllocation.Evident")]
    public static class ResourcesPool
    {
        private static readonly Transform container = new GameObject("ObjectPool").transform;
        private static readonly Dictionary<string, object> pools = new Dictionary<string, object>();

        public static T Get<T>(string path) where T : Component, IPoolableComponent<T>
        {
            if (!pools.ContainsKey(path))
            {
                pools.Add(path, new ResourcesObjectPool<T>(container, path));
            }

            return ((ResourcesObjectPool<T>) pools[path]).Get();
        }

        public static void Clear()
        {
            foreach (var pool in pools.Values)
            {
                ((IDisposable) pool).Dispose();
            }
        }
    }
}