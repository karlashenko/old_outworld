using System;
using System.Collections.Generic;
using Outworld.Game.Components;
using Outworld.Game.States;
using Outworld.Game.Utilities;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Outworld.Game.Managers
{
    public class GameStateManager : MonoBehaviour
    {
        private static readonly Dictionary<Type, GameState> persistentGameStates = new Dictionary<Type, GameState>();

        private GameState activeGameState;
        private HubGameState hubGameState;
        private LevelGameState levelGameState;

        private void Start()
        {
            ToMainMenu();
        }

        public void ToMainMenu()
        {
            Context.Get<CharacterManager>().DestroyAll();
            Context.Get<PlayerComponent>().EnableEventSystem();
            DestroyPlayerInputs();
            DestroyPersistentStates();

            SwitchGameState(new MainMenuGameState());
        }

        public void ToCharacterSelection()
        {
            SwitchGameState(new CharacterSelectionGameState());
        }

        public void ToHub()
        {
            if (!persistentGameStates.ContainsKey(typeof(HubGameState)))
            {
                persistentGameStates.Add(typeof(HubGameState), new HubGameState());
            }

            SwitchGameState(persistentGameStates[typeof(HubGameState)]);
        }

        public void ToLevel()
        {
            if (!persistentGameStates.ContainsKey(typeof(LevelGameState)))
            {
                persistentGameStates.Add(typeof(LevelGameState), new LevelGameState());
            }

            SwitchGameState(persistentGameStates[typeof(LevelGameState)]);
        }

        private void SwitchGameState(GameState gameState)
        {
            if (this.activeGameState != null)
            {
                this.activeGameState.Exit();

                if (!persistentGameStates.ContainsKey(gameState.GetType()))
                {
                    this.activeGameState.Destroy();
                }
            }

            this.activeGameState = gameState;
            this.activeGameState.Enter();
        }

        private void DestroyPlayerInputs()
        {
            foreach (var playerInput in PlayerInput.all)
            {
                if (playerInput.playerIndex == 0)
                {
                    continue;
                }

                Destroy(playerInput.gameObject);
            }
        }

        private void DestroyPersistentStates()
        {
            foreach (var gameState in persistentGameStates.Values)
            {
                if (gameState == this.activeGameState)
                {
                    gameState.Exit();
                    this.activeGameState = null;
                }

                gameState.Destroy();
            }

            persistentGameStates.Clear();
        }

        private void Update()
        {
            this.activeGameState?.Update(Time.deltaTime);
        }
    }
}