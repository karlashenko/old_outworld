using Outworld.Data;
using UnityEngine;

namespace Outworld.Game.Validators
{
    public abstract class Validator
    {
        protected readonly ValidatorData Data;

        protected Validator(ValidatorData data)
        {
            this.Data = data;
        }

        public abstract bool Validate(GameObject caster, GameObject target);

        public abstract bool Validate(GameObject caster, Vector3 target);
    }
}