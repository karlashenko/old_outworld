using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

namespace Outworld.Game.UI.Views
{
    public class CharacterSelectionView : View
    {
        public event Action ContinueButtonClickEvent;

        [SerializeField]
        private CharacterSelectionViewPlayerPanel playerPanelPrefab;

        [SerializeField]
        private Transform container;

        [SerializeField]
        private Button button;

        private readonly List<CharacterSelectionViewPlayerPanel> playerPanels = new List<CharacterSelectionViewPlayerPanel>();

        private void Start()
        {
            this.button.Select();
            this.button.onClick.AddListener(OnButtonClicked);
        }

        private void OnButtonClicked()
        {
            ContinueButtonClickEvent?.Invoke();
        }

        private void OnDestroy()
        {
            this.button.onClick.RemoveAllListeners();
        }

        public void OnPlayerJoined(PlayerInput playerInput)
        {
            var playerPanel = Instantiate(this.playerPanelPrefab, this.container);
            playerPanel.Construct(playerInput);

            this.playerPanels.Add(playerPanel);
        }

        public void OnPlayerLeft(PlayerInput player)
        {
            foreach (var playerPanel in this.playerPanels)
            {
                if (playerPanel.playerInput.playerIndex != player.playerIndex)
                {
                    continue;
                }

                Destroy(playerPanel.gameObject);
                this.playerPanels.Remove(playerPanel);
                break;
            }
        }
    }
}