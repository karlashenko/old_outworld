using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Outworld.Game.UI.Views
{
    public class CharacterSelectionViewPlayerPanel : MonoBehaviour
    {
        public PlayerInput playerInput;

        [SerializeField]
        private TextMeshProUGUI text;

        public void Construct(PlayerInput playerInput)
        {
            this.playerInput = playerInput;
            this.text.text = $"{playerInput.currentControlScheme} - {playerInput.playerIndex.ToString()}";
        }
    }
}