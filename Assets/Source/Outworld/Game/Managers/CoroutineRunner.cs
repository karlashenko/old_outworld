using System;
using System.Collections;
using UnityEngine;

namespace Outworld.Game.Managers
{
    public class CoroutineRunner : MonoBehaviour
    {
        public void Wait(float duration, Action callback)
        {
            StartCoroutine(WaitCoroutine(duration, callback));
        }

        private static IEnumerator WaitCoroutine(float duration, Action callback)
        {
            yield return new WaitForSeconds(duration);
            callback?.Invoke();
        }
    }
}