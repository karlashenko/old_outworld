using Outworld.Game.Values;
using UnityEngine;

namespace Outworld.Game.Components.Units
{
    public class HealthComponent : MonoBehaviour
    {
        private float health = 100f;

        public void Damage(Damage damage)
        {
            // TODO: Resistances

            this.health -= damage.Amount;

            if (this.health < 0)
            {
                Destroy(gameObject);
            }
        }
    }
}