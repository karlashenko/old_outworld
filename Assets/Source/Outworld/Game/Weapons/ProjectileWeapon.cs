using Outworld.Data;
using Outworld.Game.Memory;
using Outworld.Game.Utilities;
using UnityEngine;

namespace Outworld.Game.Weapons
{
    public class ProjectileWeapon : Weapon, ProjectileComponent.ICollisionHandler
    {
        public ProjectileWeapon(in WeaponData data, GameObject owner) : base(data, owner)
        {
        }

        protected override void OnStart(Transform origin, Vector2 direction, float pitch)
        {
        }

        protected override void OnShoot(Transform origin, Vector2 direction, float pitch)
        {
            var projectileData = Context.Get<Library>().Projectiles[this.Data.ProjectileId];
            var projectile = ResourcesPool.Get<ProjectileComponent>(projectileData.Prefab);
            projectile.transform.position = origin.position;
            projectile.transform.rotation = origin.rotation;
            projectile.Construct(projectileData, this.Owner, direction, pitch, this.Data.Range, this);
        }

        protected override void OnStop(Transform origin, Vector2 direction, float pitch)
        {
        }

        protected override void OnUpdate(Transform origin, Vector2 direction, float pitch, float deltaTime)
        {
        }

        public bool HandleUnitCollision(ProjectileComponent projectile, GameObject unit)
        {
            HitUnit(unit);

            return true;
        }

        public bool HandleEnvironmentCollision(ProjectileComponent projectile, Vector2 point)
        {
            HitEnvironment(point);

            return true;
        }
    }
}