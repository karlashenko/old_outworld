using Outworld.Data;
using Outworld.Game.Components.Units;
using Outworld.Game.Utilities;
using Outworld.Game.Values;
using UnityEngine;

namespace Outworld.Game.Effects
{
    public class DamageEffect : Effect
    {
        private float damage;

        public DamageEffect(EffectData data) : base(data)
        {
        }

        public override void CreateSnapshot(GameObject caster, GameObject target)
        {
            this.damage = Formula.Evaluate(this.Data.DamageFormula, caster, target);
        }

        protected override void OnApply(GameObject caster, GameObject target)
        {
            target.GetComponent<HealthComponent>().Damage(new Damage(this.damage, this.Data.DamageType));
        }
    }
}