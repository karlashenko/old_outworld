using Outworld.Data;
using Outworld.Game.Effects;
using Outworld.Game.Utilities;

namespace Outworld.Game.Behaviours
{
    public class PeriodicBehaviour : Behaviour
    {
        private readonly EffectFactory effectFactory;

        private float periodCounter;

        public PeriodicBehaviour(BehaviourData data) : base(data)
        {
            this.effectFactory = Context.Get<EffectFactory>();
        }

        protected override void OnApply()
        {
            this.periodCounter = this.Data.BuffTickPeriod;
        }

        protected override void OnUpdate(float deltaTime)
        {
            this.periodCounter -= deltaTime;

            if (this.periodCounter > 0)
            {
                return;
            }

            this.periodCounter = this.Data.BuffTickPeriod;
            this.effectFactory.Create(this.Data.BuffTickEffectId).Apply(this.Caster, this.Target);
        }
    }
}