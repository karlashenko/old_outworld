﻿using System.Collections;
using Outworld.Game.Managers;
using Outworld.Game.Utilities;
using UnityEngine;

namespace Outworld.Game.Extensions
{
    public static class RendererExtensions
    {
        public static void FadeOut(this Renderer renderer, float duration)
        {
            Context.Get<CoroutineRunner>().StartCoroutine(FadeOutCoroutine(renderer, duration));
        }

        public static void FadeIn(this Renderer renderer, float duration)
        {
            Context.Get<CoroutineRunner>().StartCoroutine(FadeInCoroutine(renderer, duration));
        }

        private static IEnumerator FadeOutCoroutine(Renderer renderer, float duration)
        {
            while (renderer != null && renderer.material.color.a > 0)
            {
                var color = renderer.material.color;
                color.a -= Time.deltaTime / duration;
                renderer.material.color = color;
                yield return null;
            }
        }

        private static IEnumerator FadeInCoroutine(Renderer renderer, float duration)
        {
            renderer.material.color = renderer.material.color.With(a: 0);

            while (renderer != null && renderer.material.color.a < 1)
            {
                var color = renderer.material.color;
                color.a += Time.deltaTime / duration;
                renderer.material.color = color;
                yield return null;
            }
        }
    }
}