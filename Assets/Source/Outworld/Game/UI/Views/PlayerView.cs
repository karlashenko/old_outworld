using System.Linq;
using Outworld.Game.Components.Units;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Outworld.Game.UI.Views
{
    public class PlayerView : View
    {
        [Header("Input")]
        [SerializeField]
        private InputActionReference nextTabAction;

        [SerializeField]
        private InputActionReference previousTabAction;

        [Header("Components")]
        [SerializeField]
        private InventoryView inventoryView;

        [SerializeField]
        private ModulesView modulesView;

        [SerializeField]
        private PlayerViewTab[] tabs;

        private PlayerViewTab activeTab;
        private int activeTabIndex;

        public void Construct(CharacterComponent character)
        {
            this.inventoryView.Construct(this.PlayerInput, character.GetComponent<InventoryComponent>());
            this.modulesView.Construct(this.PlayerInput, character);

            foreach (var tab in this.tabs)
            {
                tab.Deactivate();
                tab.ClickEvent += TabClickEvent;
            }

            TabClickEvent(this.tabs.First());
        }

        protected override void OnShow()
        {
            this.PlayerInput.actions[this.nextTabAction.action.name].performed += OnNextTabAction;
            this.PlayerInput.actions[this.previousTabAction.action.name].performed += OnPreviousTabAction;
        }

        protected override void OnHide()
        {
            this.PlayerInput.actions[this.nextTabAction.action.name].performed -= OnNextTabAction;
            this.PlayerInput.actions[this.previousTabAction.action.name].performed -= OnPreviousTabAction;
        }

        private void OnNextTabAction(InputAction.CallbackContext context)
        {
            this.activeTabIndex += 1;

            if (this.activeTabIndex >= this.tabs.Length)
            {
                this.activeTabIndex = 0;
            }

            TabClickEvent(this.tabs[this.activeTabIndex]);
        }

        private void OnPreviousTabAction(InputAction.CallbackContext context)
        {
            this.activeTabIndex -= 1;

            if (this.activeTabIndex < 0)
            {
                this.activeTabIndex = this.tabs.Length - 1;
            }

            TabClickEvent(this.tabs[this.activeTabIndex]);
        }

        private void TabClickEvent(PlayerViewTab tab)
        {
            this.activeTab?.Deactivate();
            this.activeTab = tab;
            this.activeTab.Activate(this.PlayerInput);
        }
    }
}