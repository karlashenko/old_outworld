using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Outworld.Game.UI.Views
{
    public class View : MonoBehaviour
    {
        public static event Action<View> AnyViewShowEvent;
        public static event Action<View> AnyViewHideEvent;

        public event Action<View> ShowEvent;
        public event Action<View> HideEvent;

        protected PlayerInput PlayerInput;

        public void Show(PlayerInput playerInput)
        {
            this.PlayerInput = playerInput;

            gameObject.SetActive(true);

            OnShow();

            ShowEvent?.Invoke(this);
            AnyViewShowEvent?.Invoke(this);

            Debug.Log($"{GetType().Name} shown");
        }

        public void Hide()
        {
            OnHide();

            gameObject.SetActive(false);

            this.PlayerInput = null;

            HideEvent?.Invoke(this);
            AnyViewHideEvent?.Invoke(this);

            Debug.Log($"{GetType().Name} hidden");
        }

        protected virtual void OnShow()
        {
        }

        protected virtual void OnHide()
        {
        }
    }
}