using UnityEngine;

namespace Outworld.Game.Components.Units
{
    public class TeamComponent : MonoBehaviour
    {
        public const int NeutralTeamId = -1;

        public int TeamId;
    }
}