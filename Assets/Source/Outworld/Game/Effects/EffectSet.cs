using System.Collections.Generic;
using Outworld.Data;
using Outworld.Game.Utilities;
using UnityEngine;

namespace Outworld.Game.Effects
{
    public class EffectSet : Effect
    {
        private readonly List<Effect> effects = new List<Effect>();

        public EffectSet(EffectData data) : base(data)
        {
            var effectFactory = Context.Get<EffectFactory>();

            foreach (var effectId in data.Effects)
            {
                this.effects.Add(effectFactory.Create(effectId));
            }
        }

        public override void CreateSnapshot(GameObject caster, GameObject target)
        {
            foreach (var effect in this.effects)
            {
                effect.CreateSnapshot(caster, target);
            }
        }

        protected override void OnApply(GameObject caster, GameObject target)
        {
            foreach (var effect in this.effects)
            {
                effect.Apply(caster, target);
            }
        }

        protected override void OnApply(GameObject caster, Vector3 target)
        {
            foreach (var effect in this.effects)
            {
                effect.Apply(caster, target);
            }
        }
    }
}