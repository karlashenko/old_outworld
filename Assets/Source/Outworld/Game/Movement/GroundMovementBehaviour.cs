using Outworld.Game.Components.Units;
using Outworld.Game.Extensions;
using Outworld.Game.Managers;
using Outworld.Game.Utilities;
using UnityEngine;

namespace Outworld.Game.Movement
{
    public class GroundMovementBehaviour : MovementBehaviour
    {
        private const float JumpInputTolerance = 0.1f;
        private const float JumpLedgeTolerance = 0.2f;

        private readonly GroundMovementBehaviourSettings settings;
        private readonly Configuration configuration;
        private readonly CoroutineRunner coroutineRunner;

        private float jumpInputToleranceTimer;
        private float jumpLedgeToleranceTimer;
        private float velocityXSmoothing;
        private int airJumpsRemaining;
        private int horizontalInputDirection;
        private int wallDirection;
        private bool isWallSliding;
        private bool isCrouching;
        private Vector3? previousPlatformPosition;
        private Collider2D previousPlatform;

        public GroundMovementBehaviour(Transform transform, BoxCollider2D collider, MovementSystem movementSystem, GroundMovementBehaviourSettings settings)
            : base(transform, collider, movementSystem, settings.impulseDamping)
        {
            this.settings = settings;
            this.configuration = Context.Get<Configuration>();
            this.coroutineRunner = Context.Get<CoroutineRunner>();
        }

        public float GetGravity()
        {
            return this.settings.gravity;
        }

        protected override void OnUpdate(float deltaTime)
        {
            DetermineHorizontalInputDirection();

            HandleDash();
            HandleCrouch();
            HandleWallSliding();
            HandleFallThroughPlatforms();
            HandleMovingPlatforms();

            CalculateHorizontalVelocity();
            CalculateVerticalVelocity(deltaTime);

            ApplyJumpVelocity(deltaTime);
        }

        private void HandleDash()
        {
            if (!this.Input.DashPressed)
            {
                return;
            }

            // TODO: How to get facing direction?
            var facingDirection = this.Transform.GetComponent<AimComponent>().FacingDirection;

            var direction = this.horizontalInputDirection == 0 ? facingDirection : this.horizontalInputDirection;

            AddImpulse(new Vector2(16f * this.settings.impulseDamping * direction, 5f * this.settings.impulseDamping));
        }

        private void HandleMovingPlatforms()
        {
            // ReSharper disable once Unity.PerformanceCriticalCodeNullComparison
            if (this.MovementSystem.PlatformStandingOn == null)
            {
                this.previousPlatformPosition = null;
                return;
            }

            if (this.MovementSystem.PlatformStandingOn != this.previousPlatform)
            {
                this.previousPlatformPosition = this.MovementSystem.PlatformStandingOn.transform.position;
            }

            var currentPlatformPosition = this.MovementSystem.PlatformStandingOn.transform.position;

            if (this.previousPlatformPosition.HasValue)
            {
                this.Transform.Translate(currentPlatformPosition - this.previousPlatformPosition.Value);
            }

            this.previousPlatformPosition = currentPlatformPosition;
            this.previousPlatform = this.MovementSystem.PlatformStandingOn;
        }

        private void HandleFallThroughPlatforms()
        {
            if (!this.Input.MoveDown || this.isWallSliding || this.MovementSystem.IsFallingThroughPlatforms || !this.MovementSystem.CollisionBottom)
            {
                return;
            }

            this.Velocity.y = -2;

            // TODO: Replace time controller
            this.MovementSystem.IsFallingThroughPlatforms = true;
            this.coroutineRunner.Wait(0.25f, () => this.MovementSystem.IsFallingThroughPlatforms = false);
        }

        private void HandleWallSliding()
        {
            var collidesWithTheWall = Mathf.Approximately(Mathf.Abs(this.MovementSystem.CollisionNormalHorizontal.x), 1);

            if (!this.isWallSliding)
            {
                this.wallDirection = this.MovementSystem.CollisionRight ? 1 : -1;
                this.isWallSliding = collidesWithTheWall && this.horizontalInputDirection == this.wallDirection;
            }

            this.isWallSliding = this.isWallSliding && collidesWithTheWall && !this.MovementSystem.CollisionBottom;

            if (!this.isWallSliding)
            {
                return;
            }

            this.Velocity.y = Mathf.Max(this.Velocity.y, -this.settings.wallSlideSpeed);
        }

        private void HandleCrouch()
        {
            if (this.Input.Crouch)
            {
                if (this.isCrouching)
                {
                    return;
                }

                this.isCrouching = true;
                this.Collider.size *= new Vector2(1, 0.5f);
                this.Transform.position -= Vector3.up * 0.25f;
                return;
            }

            if (!this.isCrouching || HasTopCollision(0.5f))
            {
                return;
            }

            this.isCrouching = false;
            this.Collider.size *= new Vector2(1, 2);
            this.Transform.position += Vector3.up * 0.25f;
        }

        private void CalculateHorizontalVelocity()
        {
            if (this.settings.disableAirControl && Mathf.Abs(this.Velocity.y) > 0)
            {
                return;
            }

            var targetVelocityX = this.isWallSliding ? 0 : this.horizontalInputDirection * GetHorizontalVelocity();
            var smoothVelocityX = Mathf.SmoothDamp(this.Velocity.x, targetVelocityX, ref this.velocityXSmoothing,
                this.MovementSystem.CollisionBottom ? this.settings.accelerationTimeGrounded : this.settings.accelerationTimeAirborne);

            this.Velocity.x = smoothVelocityX;
        }

        private void CalculateVerticalVelocity(float deltaTime)
        {
            if (this.MovementSystem.CollisionBottom && this.Velocity.y < 0)
            {
                this.Velocity.y = 0;
            }
            else if (this.MovementSystem.CollisionTop)
            {
                this.Velocity.y = -0.05f;
            }
            else
            {
                this.Velocity.y -= this.settings.gravity * deltaTime;
            }
        }

        private void DetermineHorizontalInputDirection()
        {
            this.horizontalInputDirection = 0;

            if (this.Input.MoveLeft)
            {
                this.horizontalInputDirection -= 1;
            }

            if (this.Input.MoveRight)
            {
                this.horizontalInputDirection += 1;
            }
        }

        private float GetHorizontalVelocity()
        {
            return this.isCrouching ? this.settings.crouchSpeed : this.settings.moveSpeed;
        }

        private void ApplyJumpVelocity(float deltaTime)
        {
            this.jumpInputToleranceTimer = Mathf.Max(0, this.jumpInputToleranceTimer - deltaTime);

            if (this.Input.JumpPressed)
            {
                if (this.airJumpsRemaining > 0 && !this.MovementSystem.CollisionBottom && !this.isWallSliding)
                {
                    this.airJumpsRemaining -= 1;
                    this.Velocity.y += this.settings.jumpForce;
                    return;
                }

                this.jumpInputToleranceTimer = JumpInputTolerance;
            }

            if (this.Input.JumpReleased && this.Velocity.y > 0 && !this.isWallSliding)
            {
                this.Velocity.y *= 0.33f;
            }

            this.jumpLedgeToleranceTimer = Mathf.Max(0, this.jumpLedgeToleranceTimer - deltaTime);

            if (this.MovementSystem.CollisionBottom || this.isWallSliding)
            {
                this.jumpLedgeToleranceTimer = JumpLedgeTolerance;
                this.airJumpsRemaining = this.settings.airJumpCount;
            }

            if (this.jumpInputToleranceTimer == 0 || this.jumpLedgeToleranceTimer == 0)
            {
                return;
            }

            if (this.isWallSliding)
            {
                var force = this.horizontalInputDirection == this.wallDirection ? this.settings.wallJumpForceClimb : this.settings.wallJumpForceLeap;
                this.Velocity = new Vector2(force.x * -this.wallDirection, force.y);
            }
            else
            {
                this.Velocity.y += this.settings.jumpForce;
            }

            this.jumpLedgeToleranceTimer = 0;
            this.jumpInputToleranceTimer = 0;
        }

        public bool HasTopCollision(float length, int rayCount = 3)
        {
            var bounds = this.Collider.bounds.Expanded(-0.04f);
            var spacing = bounds.size.x / (rayCount - 1);

            return PhysicsUtility.RaycastMultiple(bounds.GetTopLeft(), Vector2.up, Vector2.right, rayCount, spacing, length, this.configuration.collisionMask);
        }
    }
}