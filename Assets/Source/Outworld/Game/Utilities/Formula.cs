using System;
using System.Collections.Generic;
using UnityEngine;

namespace Outworld.Game.Utilities
{
    public static class Formula
    {
        private static readonly Dictionary<string, Expression> expressions = new Dictionary<string, Expression>();

        private static readonly Dictionary<string, Func<GameObject, GameObject, float>> parameters =
            new Dictionary<string, Func<GameObject, GameObject, float>>
            {
                {"AP_SCALE_2", (caster, target) => 0.5f},
            };

        public static float Evaluate(string formula, GameObject caster, GameObject target)
        {
            var expression = ParseAndCacheExpression(formula);

            foreach (var parameter in parameters)
            {
                if (!expression.Parameters.ContainsKey(parameter.Key))
                {
                    continue;
                }

                expression.Parameters[parameter.Key].Value = parameter.Value(caster, target);
            }

            return (float) expression.Value;
        }

        private static Expression ParseAndCacheExpression(string formula)
        {
            if (expressions.ContainsKey(formula))
            {
                return expressions[formula];
            }

            try
            {
                expressions.Add(formula, new ExpressionParser().EvaluateExpression(formula));
            }
            catch (ExpressionParser.ParseException exception)
            {
                Debug.LogError($"Error parsing formula: \"{formula}\" {exception.Message}");
            }

            return expressions[formula];
        }
    }
}