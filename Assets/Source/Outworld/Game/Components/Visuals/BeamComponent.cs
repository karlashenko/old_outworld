using Outworld.Game.Memory;
using Unity.Collections;
using UnityEngine;

namespace Outworld.Game.Components.Visuals
{
    [RequireComponent(typeof(LineRenderer))]
    public class BeamComponent : MonoBehaviour, IPoolableComponent<BeamComponent>
    {
        [SerializeField] private LineRenderer lineRenderer;

        private ResourcesObjectPool<BeamComponent> pool;

        public void OnCreated(ResourcesObjectPool<BeamComponent> pool)
        {
            this.pool = pool;
        }

        public void OnTaken()
        {
        }

        public void OnReleased()
        {
        }

        public void Release()
        {
            this.pool.Release(this);
        }

        public void SetPosition(int index, Vector3 position)
        {
            this.lineRenderer.SetPosition(index, position);
        }

        public void SetPositions(Vector3[] points)
        {
            if (this.lineRenderer.positionCount != points.Length)
            {
                this.lineRenderer.positionCount = points.Length;
            }

            this.lineRenderer.SetPositions(points);
        }

        public void SetPositions(NativeSlice<Vector3> points)
        {
            this.lineRenderer.positionCount = points.Length;
            this.lineRenderer.SetPositions(points);
        }
    }
}