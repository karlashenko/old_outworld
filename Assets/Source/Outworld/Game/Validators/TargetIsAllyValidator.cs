using Outworld.Data;
using Outworld.Game.Components.Units;
using UnityEngine;

namespace Outworld.Game.Validators
{
    public class TargetIsAllyValidator : Validator
    {
        public TargetIsAllyValidator(ValidatorData data) : base(data)
        {
        }

        public override bool Validate(GameObject caster, GameObject target)
        {
            var casterTeam = caster.GetComponent<TeamComponent>();
            var targetTeam = target.GetComponent<TeamComponent>();

            if (casterTeam.TeamId == TeamComponent.NeutralTeamId || targetTeam.TeamId == TeamComponent.NeutralTeamId)
            {
                return true;
            }

            return casterTeam.TeamId == targetTeam.TeamId;
        }

        public override bool Validate(GameObject caster, Vector3 target)
        {
            return false;
        }
    }
}