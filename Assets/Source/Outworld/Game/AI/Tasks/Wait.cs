﻿namespace Outworld.Game.AI.Tasks
{
    public class Wait : BehaviourTreeLogicNode
    {
        private readonly float duration;

        private float counter;

        public Wait(BehaviourTreePropertiesData properties) : base(properties)
        {
            this.duration = properties.waitDuration;
        }

        protected override void OnStop(BehaviourTreeContext context)
        {
            this.counter = 0;
        }

        protected override BehaviourTreeStatus OnTick(float deltaTime, BehaviourTreeContext context)
        {
            this.counter += deltaTime;

            return this.counter >= this.duration ? BehaviourTreeStatus.Success : BehaviourTreeStatus.Running;
        }
    }
}