using System;
using System.Diagnostics.CodeAnalysis;
using Outworld.Game.Components;
using UnityEngine;
using UnityEngine.Pool;
using Object = UnityEngine.Object;

namespace Outworld.Game.Memory
{
    public class ResourcesObjectPool<T> : IDisposable where T : Component, IPoolableComponent<T>
    {
        private readonly Transform container;
        private readonly string path;
        private readonly ObjectPool<T> pool;

        [SuppressMessage("ReSharper", "HeapView.DelegateAllocation")]
        [SuppressMessage("ReSharper", "HeapView.ObjectAllocation.Evident")]
        public ResourcesObjectPool(Transform container, string path, int capacity = 2)
        {
            this.container = container;
            this.path = path;
            this.pool = new ObjectPool<T>(Create, actionOnDestroy: Destroy, defaultCapacity: capacity);
        }

        public T Get()
        {
            var poolable = this.pool.Get();
            poolable.transform.parent = null;
            poolable.gameObject.SetActive(true);
            poolable.OnTaken();

            return poolable;
        }

        public void Release(T poolable)
        {
            poolable.transform.parent = this.container;
            poolable.gameObject.SetActive(false);
            poolable.OnReleased();

            this.pool.Release(poolable);
        }

        public void Dispose()
        {
            this.pool.Clear();
        }

        private T Create()
        {
            var poolable = Object.Instantiate(Resources.Load<T>(this.path));
            poolable.OnCreated(this);

            return poolable;
        }

        private static void Destroy(T poolable)
        {
            Object.Destroy(poolable.gameObject);
        }
    }
}