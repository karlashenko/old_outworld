using Outworld.Game.Input;
using Outworld.Game.Utilities;
using Outworld.Game.Weapons;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Outworld.Game.Components.Units
{
    public class WeaponComponent : MonoBehaviour
    {
        private InputState input;
        private AimComponent aim;
        private Weapon weapon;
        private WeaponView weaponView;
        private Configuration configuration;
        private WeaponFactory weaponFactory;

        private int weaponId;

        private void Start()
        {
            this.aim = GetComponent<AimComponent>();
            this.configuration = Context.Get<Configuration>();
            this.weaponFactory = Context.Get<WeaponFactory>();

            SwitchWeapon(1);
        }

        public void SwitchWeapon(int weaponId)
        {
            Debug.Log(weaponId);

            this.weapon?.UnEquip();
            this.weapon = this.weaponFactory.Create(weaponId, gameObject);
            this.weapon.Equip();

            if (this.weaponView != null)
            {
                Destroy(this.weaponView.gameObject);
            }

            this.weaponView = Instantiate(Resources.Load<WeaponView>(this.weapon.Data.Prefab), this.aim.GetWeaponSlot());
        }

        public void SetInput(in InputState input)
        {
            this.input = input;
        }

        public bool IsInRange(Vector2 target)
        {
            var origin = (Vector2) this.weaponView.GetBarrel().position;
            return (origin - target).sqrMagnitude < this.weapon.Data.Range * this.weapon.Data.Range;
        }

        public bool IsOnLineOfSight(Vector2 target)
        {
            var origin = (Vector2) this.weaponView.GetBarrel().position;
            return !Physics2D.Raycast(origin, this.aim.PitchDirection, (origin - target).magnitude, this.configuration.collisionMask);
        }

        public bool IsInRangeAndOnLineOfSight(Vector2 target)
        {
            return IsInRange(target) && IsOnLineOfSight(target);
        }

        private void Update()
        {
            if (this.weapon == null)
            {
                return;
            }

            if (this.input.Action0Pressed)
            {
                StartFire();
            }

            if (this.input.Action0Released)
            {
                StopFire();
            }

            if (Keyboard.current.f1Key.wasPressedThisFrame)
            {
                this.weaponId += 1;
                SwitchWeapon(this.weaponId);
            }

            if (Keyboard.current.f2Key.wasPressedThisFrame)
            {
                this.weaponId -= 1;
                SwitchWeapon(this.weaponId);
            }

            this.weapon.Update(this.weaponView.GetBarrel(), this.aim.PitchDirection, this.aim.Pitch, Time.deltaTime);
        }

        public void StartFire()
        {
            this.weapon.Start(this.weaponView.GetBarrel(), this.aim.PitchDirection, this.aim.Pitch);
        }

        public void StopFire()
        {
            this.weapon.Stop(this.weaponView.GetBarrel(), this.aim.PitchDirection, this.aim.Pitch);
        }
    }
}