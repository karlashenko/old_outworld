using System;
using Outworld.Data;
using Outworld.Data.Types;

namespace Outworld.Game.Properties
{
    public class PropertyModifier
    {
        public readonly PropertyModifierData Data;

        public PropertyModifier(PropertyModifierData data)
        {
            this.Data = data;
        }

        public float Modify(float value)
        {
            switch (this.Data.Type)
            {
                case ModifierType.Add:
                    return value + this.Data.Amount;
                case ModifierType.Multiply:
                    return value * this.Data.Amount;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}