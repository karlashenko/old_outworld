﻿namespace Outworld.Game.AI
{
    public enum BehaviourTreeStatus
    {
        None,
        Success,
        Failure,
        Running,
        Waiting,
    }
}
