﻿namespace Outworld.Game.AI.Conditions
{
    public class IsTargetPointReachable : BehaviourTreeLogicNode
    {
        public IsTargetPointReachable(BehaviourTreePropertiesData properties) : base(properties)
        {
        }

        protected override BehaviourTreeStatus OnTick(float deltaTime, BehaviourTreeContext context)
        {
            return BehaviourTreeStatus.Success;
        }
    }
}