using Outworld.Game.Extensions;
using Outworld.Game.Input;
using UnityEngine;

namespace Outworld.Game.Movement
{
    public abstract class MovementBehaviour
    {
        public Vector2 Velocity;

        protected InputState Input;

        protected readonly Transform Transform;
        protected readonly BoxCollider2D Collider;
        protected readonly MovementSystem MovementSystem;

        private Vector2 impulse;
        private Vector2 impulseBase;
        private readonly float impulseDamping;

        protected MovementBehaviour(Transform transform, BoxCollider2D collider, MovementSystem movementSystem, float impulseDamping)
        {
            this.Transform = transform;
            this.Collider = collider;
            this.MovementSystem = movementSystem;
            this.impulseDamping = impulseDamping;
        }

        public Vector2 GetImpulse()
        {
            return this.impulse;
        }

        public void SetImpulse(float? x = null, float? y = null)
        {
            this.impulse = this.impulse.With(x, y);
            this.impulseBase = this.impulse;
        }

        public void AddImpulse(Vector2 impulse)
        {
            this.impulse += impulse;
            this.impulseBase = this.impulse;
        }

        public void SetInput(in InputState input)
        {
            this.Input = input;
        }

        public void Update(float deltaTime)
        {
            OnUpdate(deltaTime);

            this.MovementSystem.Translate(this.Transform, this.Collider, (this.Velocity + this.impulse) * deltaTime);

            ApplyImpulseDrag(deltaTime);
        }

        public void Enable()
        {
            OnEnable();
        }

        public void Disable()
        {
            OnDisable();
        }

        protected virtual void OnEnable()
        {
        }

        protected virtual void OnDisable()
        {
        }

        protected virtual void OnUpdate(float deltaTime)
        {
        }

        private void ApplyImpulseDrag(float deltaTime)
        {
            if (this.impulse == Vector2.zero)
            {
                return;
            }

            this.impulse *= 1 - deltaTime * this.impulseDamping;

            if (this.impulse.x.CompareTo(0) != this.Velocity.x.CompareTo(0))
            {
                this.impulse.x -= Mathf.Abs(this.Velocity.x) * deltaTime * 15 * Mathf.Sign(this.impulse.x);
            }

            if (this.impulse.y.CompareTo(0) != this.Velocity.y.CompareTo(0))
            {
                this.impulse.y -= Mathf.Abs(this.Velocity.y) * deltaTime * 15 * Mathf.Sign(this.impulse.y);
            }

            if (this.impulse.x.CompareTo(0) != this.impulseBase.x.CompareTo(0))
            {
                this.impulse.x = 0;
            }

            if (this.impulse.y.CompareTo(0) != this.impulseBase.y.CompareTo(0))
            {
                this.impulse.y = 0;
            }

            if (this.MovementSystem.CollisionLeft || this.MovementSystem.CollisionRight)
            {
                this.impulse.x = 0;
            }

            if (this.MovementSystem.CollisionTop || this.MovementSystem.CollisionBottom)
            {
                this.impulse.y = 0;
            }
        }
    }
}