﻿namespace Outworld.Game.AI.Composites
{
    public class Tree : BehaviourTreeNodeContainer
    {
        protected override BehaviourTreeStatus OnTick(float deltaTime, BehaviourTreeContext context)
        {
            foreach (var node in this.Nodes)
            {
                node.Tick(deltaTime, context);
            }

            return BehaviourTreeStatus.Success;
        }
    }
}