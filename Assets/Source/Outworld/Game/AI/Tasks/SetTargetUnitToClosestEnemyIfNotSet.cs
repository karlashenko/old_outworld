using System.Diagnostics.CodeAnalysis;
using Outworld.Game.Components.Units;
using Outworld.Game.Utilities;
using UnityEngine;

namespace Outworld.Game.AI.Tasks
{
    public class SetTargetUnitToClosestEnemyIfNotSet : BehaviourTreeLogicNode
    {
        private readonly Configuration configuration;

        public SetTargetUnitToClosestEnemyIfNotSet(BehaviourTreePropertiesData properties) : base(properties)
        {
            this.configuration = Context.Get<Configuration>();
        }

        [SuppressMessage("ReSharper", "Unity.PerformanceCriticalCodeNullComparison")]
        protected override BehaviourTreeStatus OnTick(float deltaTime, BehaviourTreeContext context)
        {
            if (context.TargetUnit != null)
            {
                return BehaviourTreeStatus.Success;
            }

            var colliders = Physics2D.OverlapCircleNonAlloc(context.Unit.transform.position, 10f, ColliderCache, this.configuration.unitMask);

            for (var i = 0; i < colliders; i++)
            {
                var player = ColliderCache[i].gameObject.GetComponent<CharacterComponent>();

                if (player == null)
                {
                    continue;
                }

                context.TargetUnit = ColliderCache[i].gameObject;
                return BehaviourTreeStatus.Success;
            }

            return BehaviourTreeStatus.Failure;
        }
    }
}