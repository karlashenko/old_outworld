using Outworld.Game.UI.Views;
using Outworld.Game.Utilities;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Outworld.Game.States
{
    public class MainMenuGameState : GameState
    {
        private readonly MainMenuView mainMenuView;

        public MainMenuGameState()
        {
            this.mainMenuView = Context.InstantiatePrefab<MainMenuView>();
        }

        protected override void OnEnter()
        {
            this.mainMenuView.Show(PlayerInput.all[0]);
        }

        protected override void OnExit()
        {
            this.mainMenuView.Hide();
        }

        protected override void OnDestroy()
        {
            Object.Destroy(this.mainMenuView.gameObject);
        }

        public override void Update(float deltaTime)
        {
        }
    }
}