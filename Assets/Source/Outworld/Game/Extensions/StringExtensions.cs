using System;
using System.Text;
using System.Text.RegularExpressions;

namespace Outworld.Game.Extensions
{
    public static class StringExtensions
    {
        public static bool LikeIgnoreCase(this string toSearch, string toFind)
        {
            return toSearch.ToLower().Like(toFind.ToLower());
        }

        public static bool Like(this string toSearch, string toFind)
        {
            return new Regex(@"\A" + new Regex(@"\.|\$|\^|\{|\[|\(|\||\)|\*|\+|\?|\\")
                    .Replace(toFind, ch => @"\" + ch)
                    .Replace('_', '.')
                    .Replace("%", ".*") + @"\z",
                RegexOptions.Singleline
            ).IsMatch(toSearch);
        }

        public static string Splice(this string text, int maxLength)
        {
            var stringBuilder = new StringBuilder(text);

            for (var i = 0; i <= stringBuilder.Length / maxLength; i++)
            {
                stringBuilder.Insert(i * maxLength, Environment.NewLine);
            }

            return stringBuilder.ToString();
        }

        public static string WrapWords(this string text, int maxLength)
        {
            var stringBuilder = new StringBuilder();
            var lineLength = 0;

            foreach (var word in text.Split(' '))
            {
                if (lineLength + word.Length + 1 >= maxLength)
                {
                    stringBuilder.AppendFormat("{0}{1}", Environment.NewLine, word);
                    lineLength = word.Length;
                    continue;
                }

                if (lineLength == 0)
                {
                    stringBuilder.Append(word);
                    lineLength += word.Length;
                    continue;
                }

                stringBuilder.AppendFormat(" {0}", word);
                lineLength += word.Length + 1;
            }

            return stringBuilder.ToString().TrimEnd(Environment.NewLine.ToCharArray());
        }
    }
}