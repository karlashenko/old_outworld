using System;

namespace Outworld.Game.Utilities
{
    public static class RNG
    {
        public static readonly Random Random = new Random();

        public static int Range(int min, int max)
        {
            return Random.Next(min, max + 1);
        }

        public static float Range(float min, float max)
        {
            return (float) Random.NextDouble() * (max - min) + min;
        }

        public static float Float()
        {
            return (float) Random.NextDouble();
        }

        public static bool Test(float number)
        {
            return Float() <= number;
        }
    }
}