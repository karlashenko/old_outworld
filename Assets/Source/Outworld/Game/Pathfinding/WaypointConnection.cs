using System;

namespace Outworld.Game.Pathfinding
{
    public readonly struct WaypointConnection : IEquatable<WaypointConnection>
    {
        public static WaypointConnection Empty = new WaypointConnection(-1, WaypointConnectionType.Walk);

        public readonly int WaypointIndex;
        public readonly WaypointConnectionType Type;

        public WaypointConnection(int waypointIndex, WaypointConnectionType type)
        {
            this.WaypointIndex = waypointIndex;
            this.Type = type;
        }

        public bool IsEmpty()
        {
            return this.WaypointIndex == Empty.WaypointIndex;
        }

        public bool Equals(WaypointConnection other)
        {
            return this.WaypointIndex == other.WaypointIndex;
        }
    }
}