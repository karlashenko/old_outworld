using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;

// ReSharper disable ForeachCanBePartlyConvertedToQueryUsingAnotherGetEnumerator
// ReSharper disable InvertIf
// ReSharper disable SwapViaDeconstruction
// ReSharper disable UseMethodAny.2
// ReSharper disable UseMethodAny.0

namespace Outworld.Game.Pathfinding
{
    [BurstCompile]
    public struct PathfinderJob : IJob
    {
        [ReadOnly] public Waypoint Origin;
        [ReadOnly] public Waypoint Target;
        [ReadOnly] public NativeArray<Waypoint> Waypoints;
        [ReadOnly] public NativeMultiHashMap<int, WaypointConnection> Connections;

        public NativeList<PathfinderNode> Result;

        public void Execute()
        {
            var visited = new NativeList<int>(16, Allocator.Temp);
            var queue = new NativeQueue<PathfinderNode>(Allocator.Temp);
            var nodes = new NativeHashMap<int, PathfinderNode>(16, Allocator.Temp);

            queue.Enqueue(CreateNode(this.Origin, this.Target, WaypointConnectionType.Jump, null));

            while (queue.Count > 0)
            {
                var current = queue.Dequeue();

                if (!nodes.ContainsKey(current.WaypointIndex))
                {
                    nodes.Add(current.WaypointIndex, current);
                }

                foreach (var connection in this.Connections.GetValuesForKey(current.WaypointIndex))
                {
                    var waypoint = this.Waypoints[connection.WaypointIndex];

                    if (visited.Contains(waypoint.Index))
                    {
                        continue;
                    }

                    visited.Add(waypoint.Index);

                    var node = CreateNode(this.Waypoints[connection.WaypointIndex], this.Target, connection.Type, current);
                    queue.Enqueue(node);
                }

                if (current.WaypointIndex == this.Target.Index)
                {
                    BuildPath(this.Origin.Index, this.Target.Index, nodes);
                    break;
                }
            }

            visited.Dispose();
            queue.Dispose();
            nodes.Dispose();
        }

        private void BuildPath(int originWaypointIndex, int targetWaypointIndex, NativeHashMap<int, PathfinderNode> nodes)
        {
            if (nodes.Count() == 0 || !nodes.ContainsKey(targetWaypointIndex))
            {
                return;
            }

            var current = nodes[targetWaypointIndex];

            while (current.WaypointIndex != originWaypointIndex)
            {
                this.Result.Add(current);
                current = nodes[current.ParentWaypointIndex];
            }

            var last = this.Result[this.Result.Length - 1];
            this.Result.Add(nodes[originWaypointIndex].WithConnectionType(last.WaypointConnectionType));

            Reverse(this.Result);
        }

        private void Reverse(NativeList<PathfinderNode> list)
        {
            var a = 0;
            var b = list.Length - 1;

            while (a < b)
            {
                var temp = list[a];
                list[a] = list[b];
                list[b] = temp;

                a += 1;
                b -= 1;
            }
        }

        private PathfinderNode CreateNode(in Waypoint waypoint, in Waypoint destination, WaypointConnectionType connectionType, in PathfinderNode? parent)
        {
            var g = 0f;

            if (parent.HasValue)
            {
                g = parent.Value.G + (waypoint.Position - this.Waypoints[parent.Value.WaypointIndex].Position).sqrMagnitude;
            }

            var h = (waypoint.Position - destination.Position).sqrMagnitude;
            var f = g + h;

            return new PathfinderNode(waypoint.Position, waypoint.Index, parent?.WaypointIndex ?? -1, connectionType, g, h, f);
        }
    }
}