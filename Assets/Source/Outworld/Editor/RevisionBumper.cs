using System;
using UnityEditor;
using UnityEngine;

namespace Outworld.Editor
{
    public static class RevisionBumper
    {
        [UnityEditor.Callbacks.DidReloadScripts]
        public static void OnScriptsReloaded()
        {
            var version = PlayerSettings.bundleVersion.Split('.');

            var major = Convert.ToInt32(version[0]);
            var minor = Convert.ToInt32(version[1]);
            var build = Convert.ToInt32(version[2]) + 1;

            PlayerSettings.bundleVersion = $"{major.ToString()}.{minor.ToString()}.{build.ToString()}";

            Debug.Log($"Build: {build.ToString()}");
        }
    }
}