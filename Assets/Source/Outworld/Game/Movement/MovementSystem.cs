using Outworld.Game.Extensions;
using Outworld.Game.Utilities;
using UnityEngine;

namespace Outworld.Game.Movement
{
    public class MovementSystem
    {
        private const int HorizontalRayCount = 4;
        private const int VerticalRayCount = 3;
        private const float MaxSlopeAngle = 60;
        private const float BoundingBoxInset = 0.2f;

        public bool CollisionTop;
        public bool CollisionBottom;
        public bool CollisionLeft;
        public bool CollisionRight;
        public bool IsStandingOnPlatform;
        public float SlopeAngle;
        public bool MovingUpTheSlope;
        public bool MovingDownTheSlope;
        public bool SlidingDownTheSlope;
        public Collider2D PlatformStandingOn;
        public Vector2 CollisionNormalHorizontal;
        public Vector2 CollisionNormalVertical;
        public bool IsCollisionEnabled = true;
        public bool IsFallingThroughPlatforms;
        public Vector2Int RaycastDirection;

        private readonly float maxSlopeAngleTangent = Mathf.Tan(MaxSlopeAngle * Mathf.Deg2Rad);
        private readonly Configuration configuration;

        public MovementSystem()
        {
            this.configuration = Context.Get<Configuration>();
        }

        public void Translate(Transform transform, BoxCollider2D collider, Vector2 translation)
        {
            this.RaycastDirection.x = Mathf.Approximately(translation.x, 0) ? this.RaycastDirection.x : translation.x.CompareTo(0);
            this.RaycastDirection.y = Mathf.Approximately(translation.y, 0) ? this.RaycastDirection.y : translation.y.CompareTo(0);

            ResetState();

            if (this.IsCollisionEnabled)
            {
                var bounds = collider.bounds.Expanded(-BoundingBoxInset * 2);

                TranslateUpTheSlope(ref translation, bounds);

                if (!this.MovingUpTheSlope)
                {
                    CollideHorizontally(ref translation, bounds);
                }

                TranslateDownTheSlope(ref translation, bounds);

                if (!this.MovingDownTheSlope && !this.SlidingDownTheSlope)
                {
                    CollideVertically(ref translation, bounds);
                }
            }

            transform.Translate(translation);
        }

        private void ResetState()
        {
            this.CollisionTop = false;
            this.CollisionBottom = false;
            this.CollisionLeft = false;
            this.CollisionRight = false;
            this.MovingUpTheSlope = false;
            this.MovingDownTheSlope = false;
            this.SlidingDownTheSlope = false;
            this.IsStandingOnPlatform = false;
            this.PlatformStandingOn = null;
            this.SlopeAngle = 0;
            this.CollisionNormalVertical = new Vector2();
            this.CollisionNormalHorizontal = new Vector2();
        }

        private void CollideHorizontally(ref Vector2 translation, in Bounds bounds)
        {
            var raySpacing = bounds.size.y / (HorizontalRayCount - 1);
            var rayLength = Mathf.Max(BoundingBoxInset + Mathf.Abs(translation.x), BoundingBoxInset + 0.05f);
            var rayOrigin = this.RaycastDirection.x > 0 ? bounds.GetBottomRight() : bounds.GetBottomLeft();
            var rayDirection = Vector2.right * this.RaycastDirection.x;

            var raycastHit = PhysicsUtility.RaycastMultiple(rayOrigin, rayDirection, Vector2.up, HorizontalRayCount, raySpacing, rayLength, this.configuration.collisionMask);

            if (!raycastHit)
            {
                return;
            }

            this.CollisionNormalHorizontal = raycastHit.normal;
            this.CollisionRight = this.RaycastDirection.x > 0;
            this.CollisionLeft = this.RaycastDirection.x < 0;

            translation.x = (Mathf.Abs(rayOrigin.x - raycastHit.point.x) - BoundingBoxInset) * this.RaycastDirection.x;
        }

        private void CollideVertically(ref Vector2 translation, in Bounds bounds)
        {
            var raySpacing = bounds.size.x / (VerticalRayCount - 1);
            var rayLength = Mathf.Max(BoundingBoxInset + Mathf.Abs(translation.y), BoundingBoxInset + 0.05f);
            var rayOrigin = this.RaycastDirection.y > 0 ? bounds.GetTopLeft() : bounds.GetBottomLeft();
            var rayDirection = Vector2.up * this.RaycastDirection.y;

            var raycastHit = PhysicsUtility.RaycastMultiple(rayOrigin, rayDirection, Vector2.right, VerticalRayCount, raySpacing, rayLength, this.configuration.collisionMask | this.configuration.platformMask);

            if (!raycastHit)
            {
                return;
            }

            this.CollisionNormalVertical = raycastHit.normal;

            var hitsPlatform = this.configuration.platformMask.ContainsLayer(raycastHit.collider.gameObject.layer);

            if (hitsPlatform)
            {
                if (translation.y < 0)
                {
                    var slopeDirection = raycastHit.normal.x.CompareTo(0);
                    var slopeContactPointX = slopeDirection > 0 ? bounds.min.x : bounds.max.x;

                    if (!Mathf.Approximately(slopeDirection, 0) &&
                        !Mathf.Approximately(slopeContactPointX, raycastHit.point.x))
                    {
                        return;
                    }
                }

                if (rayDirection.y > 0 || this.IsFallingThroughPlatforms)
                {
                    return;
                }
            }

            this.CollisionTop = this.RaycastDirection.y > 0;
            this.CollisionBottom = this.RaycastDirection.y < 0;
            this.IsStandingOnPlatform = hitsPlatform && this.CollisionBottom;
            this.PlatformStandingOn = this.IsStandingOnPlatform ? raycastHit.collider : null;

            translation.y = (Mathf.Abs(rayOrigin.y - raycastHit.point.y) - BoundingBoxInset) * this.RaycastDirection.y;
        }

        private void TranslateUpTheSlope(ref Vector2 translation, in Bounds bounds)
        {
            if (this.IsFallingThroughPlatforms || Mathf.Approximately(translation.x, 0))
            {
                return;
            }

            var rayLength = BoundingBoxInset + Mathf.Abs(translation.x);
            var rayOrigin = this.RaycastDirection.x > 0 ? bounds.GetBottomRight() : bounds.GetBottomLeft();

            var raycastHit = Physics2D.Raycast(rayOrigin, Vector2.right * this.RaycastDirection.x, rayLength, this.configuration.collisionMask | this.configuration.platformMask);

            if (!raycastHit)
            {
                return;
            }

            var slopeAngle = Vector2.Angle(raycastHit.normal, Vector2.up);

            if (slopeAngle > MaxSlopeAngle)
            {
                return;
            }

            var step = Mathf.Abs(translation.x);
            var translationY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * step;

            // Jumping
            if (translation.y > translationY)
            {
                return;
            }

            this.MovingUpTheSlope = true;
            this.SlopeAngle = slopeAngle;
            this.CollisionBottom = true;
            this.PlatformStandingOn = raycastHit.collider;
            this.IsStandingOnPlatform = this.configuration.platformMask.ContainsLayer(raycastHit.collider.gameObject.layer);

            translation.x = Mathf.Cos(this.SlopeAngle * Mathf.Deg2Rad) * step * this.RaycastDirection.x;
            translation.y = translationY;
        }

        private void TranslateDownTheSlope(ref Vector2 translation, in Bounds bounds)
        {
            if (SlideDownTheSlope(ref translation, bounds.GetBottomLeft()) ||
                SlideDownTheSlope(ref translation, bounds.GetBottomRight()))
            {
                return;
            }

            if (this.IsFallingThroughPlatforms || Mathf.Approximately(translation.x, 0) || translation.y > 0)
            {
                return;
            }

            var raycastOrigin = this.RaycastDirection.x > 0 ? bounds.GetBottomLeft() : bounds.GetBottomRight();
            var raycastHit = Physics2D.Raycast(raycastOrigin, Vector2.down, Mathf.Infinity, this.configuration.collisionMask | this.configuration.platformMask);

            if (!raycastHit || !Mathf.Approximately(Mathf.Sign(raycastHit.normal.x), this.RaycastDirection.x))
            {
                return;
            }

            if (raycastHit.distance - BoundingBoxInset > this.maxSlopeAngleTangent * Mathf.Abs(translation.x))
            {
                return;
            }

            var slopeAngle = Vector2.Angle(Vector2.up, raycastHit.normal);

            if (Mathf.Approximately(slopeAngle, 0))
            {
                return;
            }

            this.MovingDownTheSlope = true;
            this.SlopeAngle = slopeAngle;
            this.CollisionBottom = true;
            this.PlatformStandingOn = raycastHit.collider;
            this.IsStandingOnPlatform = this.configuration.platformMask.ContainsLayer(raycastHit.collider.gameObject.layer);

            translation.y -= Mathf.Abs(raycastHit.point.y - raycastOrigin.y) - BoundingBoxInset;
        }

        private bool SlideDownTheSlope(ref Vector2 translation, in Vector2 origin)
        {
            var raycastHit = Physics2D.Raycast(origin, Vector3.down, Mathf.Abs(translation.y) + BoundingBoxInset, this.configuration.collisionMask | this.configuration.platformMask);

            if (!raycastHit)
            {
                return false;
            }

            var slopeAngle = Vector2.Angle(raycastHit.normal, Vector2.up);

            if (slopeAngle < MaxSlopeAngle)
            {
                return false;
            }

            this.SlopeAngle = slopeAngle;
            this.SlidingDownTheSlope = true;

            translation.x = (Mathf.Abs(translation.y) - Mathf.Abs(raycastHit.point.x - origin.x)) / Mathf.Tan(this.SlopeAngle * Mathf.Deg2Rad) * Mathf.Sign(raycastHit.normal.x);

            return true;
        }
    }
}