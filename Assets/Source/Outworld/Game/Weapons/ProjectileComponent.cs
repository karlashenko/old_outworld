using Outworld.Data;
using Outworld.Game.Components;
using Outworld.Game.Components.Visuals;
using Outworld.Game.Extensions;
using Outworld.Game.Memory;
using Outworld.Game.Utilities;
using UnityEngine;

namespace Outworld.Game.Weapons
{
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(VisualEffectComponent))]
    public class ProjectileComponent : MonoBehaviour, IPoolableComponent<ProjectileComponent>, VisualEffectComponent.IStopCallback
    {
        public interface ICollisionHandler
        {
            bool HandleUnitCollision(ProjectileComponent projectile, GameObject unit);

            bool HandleEnvironmentCollision(ProjectileComponent projectile, Vector2 point);
        }

        [SerializeField] private VisualEffectComponent visualEffect;
        [SerializeField] private new Rigidbody2D rigidbody;

        private ResourcesObjectPool<ProjectileComponent> pool;
        private Configuration configuration;

        private ICollisionHandler collisionHandler;
        private GameObject owner;
        private GameObject homingTarget;
        private ProjectileData projectile;
        private float lifetime;
        private bool isDestroyed;

        public void Construct(in ProjectileData projectile, GameObject owner, in Vector3 direction, float angle, float range, ICollisionHandler collisionHandler)
        {
            this.projectile = projectile;
            this.owner = owner;
            this.homingTarget = null;
            this.isDestroyed = false;
            this.lifetime = this.projectile.Lifetime > 0 ? this.projectile.Lifetime : range / this.projectile.Speed;
            this.collisionHandler = collisionHandler;

            this.rigidbody.simulated = true;
            this.rigidbody.velocity = direction * this.projectile.Speed;

            transform.localRotation = Quaternion.Euler(0, 0, angle);
        }

        public void OnCreated(ResourcesObjectPool<ProjectileComponent> pool)
        {
            this.pool = pool;
            this.configuration = Context.Get<Configuration>();
        }

        public void OnTaken()
        {
            this.visualEffect.OnTaken();
        }

        public void OnReleased()
        {
            this.visualEffect.OnReleased();
        }

        public void Release()
        {
            this.pool.Release(this);
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (!this.configuration.collisionMask.ContainsLayer(collision.gameObject.layer))
            {
                return;
            }

            if (this.projectile.RicochetOnCollision)
            {
                var reflected = Vector3.Reflect(transform.right, collision.contacts[0].normal);

                this.rigidbody.SetRotation(reflected.DirectionToDegrees360());
                this.rigidbody.velocity = reflected * this.projectile.Speed;

                return;
            }

            if (!this.projectile.DestroyOnCollision)
            {
                return;
            }

            if (this.collisionHandler.HandleEnvironmentCollision(this, collision.contacts[0].point))
            {
                OnImpact();
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!this.configuration.unitMask.ContainsLayer(other.gameObject.layer) || other.gameObject == this.owner)
            {
                return;
            }

            if (this.collisionHandler.HandleUnitCollision(this, other.gameObject))
            {
                OnImpact();
            }
        }

        private void OnImpact()
        {
            this.visualEffect.HideSprites();
            this.visualEffect.Stop(this);

            var impactEffect = ResourcesPool.Get<VisualEffectComponent>(this.projectile.ImpactPrefab);
            impactEffect.transform.position = transform.position;
            impactEffect.transform.rotation = transform.rotation;
            impactEffect.StopAndRelease();

            this.rigidbody.simulated = false;
            this.isDestroyed = true;
            this.homingTarget = null;
        }

        public void OnVisualEffectStopped(VisualEffectComponent visualEffect)
        {
            Release();
        }

        private void Update()
        {
            if (this.isDestroyed)
            {
                return;
            }

            ReduceLifetime(Time.deltaTime);
            DoHoming();
        }

        private void ReduceLifetime(float amount)
        {
            this.lifetime -= amount;

            if (this.lifetime < 0)
            {
                OnImpact();
            }
        }

        private void DoHoming()
        {
            if (!this.projectile.IsHoming)
            {
                return;
            }

            DetectHomingTarget();
            MoveTowardsHomingTarget();
        }

        private void DetectHomingTarget()
        {
            if (this.homingTarget != null)
            {
                return;
            }

            foreach (var target in PhysicsUtility.OverlapCircle(transform.position, this.projectile.HomingRadius, this.configuration.unitMask))
            {
                if (target.gameObject == this.owner)
                {
                    continue;
                }

                var magnitude = (target.transform.position - transform.position).magnitude;
                var direction = (target.transform.position - transform.position).normalized;

                if (Physics2D.Raycast(transform.position, direction, magnitude, this.configuration.collisionMask))
                {
                    continue;
                }

                this.homingTarget = target.gameObject;
                break;
            }
        }

        private void MoveTowardsHomingTarget()
        {
            if (this.homingTarget == null)
            {
                return;
            }

            var direction = (this.homingTarget.transform.position - transform.position).normalized;
            var rotation = Vector3.Cross(direction, transform.right).z;

            this.rigidbody.angularVelocity = -rotation * this.projectile.HomingSpeed;
            this.rigidbody.velocity = transform.right * this.projectile.Speed;
        }
    }
}