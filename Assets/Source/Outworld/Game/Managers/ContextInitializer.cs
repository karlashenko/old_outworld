using System.Collections.Generic;
using Outworld.Data;
using Outworld.Data.Types;
using Outworld.Game.Components;
using Outworld.Game.Effects;
using Outworld.Game.Internationalization;
using Outworld.Game.Levels;
using Outworld.Game.Localization;
using Outworld.Game.Pathfinding;
using Outworld.Game.UI.Elements;
using Outworld.Game.UI.Views;
using Outworld.Game.Utilities;
using Outworld.Game.Validators;
using Outworld.Game.Weapons;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Outworld.Game.Managers
{
    public class ContextInitializer : MonoBehaviour
    {
        private void Awake()
        {
            var config = FindObjectOfType<Configuration>();

            Context.RegisterInstance(this);
            Context.RegisterInstance(config);
            Context.RegisterInstance(CreateDataset());
            Context.RegisterInstance(FindObjectOfType<LevelGenerator>(true));
            Context.RegisterInstance(FindObjectOfType<Pathfinder>(true));
            Context.RegisterInstance(FindObjectOfType<PathfinderTest>(true));
            Context.RegisterInstance(FindObjectOfType<CoroutineRunner>(true));
            Context.RegisterInstance(FindObjectOfType<HubManager>(true));
            Context.RegisterInstance(FindObjectOfType<PlayerInputManager>(true));
            Context.RegisterInstance(FindObjectOfType<Tooltip>(true));
            Context.RegisterInstance(FindObjectOfType<PlayerComponent>(true));
            Context.RegisterInstance(FindObjectOfType<GameStateManager>(true));

            Context.RegisterInstance(new CharacterManager());
            Context.RegisterInstance(new I18n());
            Context.RegisterInstance(new EffectFactory());
            Context.RegisterInstance(new ValidatorFactory());
            Context.RegisterInstance(new WeaponFactory());

            Context.RegisterPrefab<MainMenuView>($"Prefabs/UI/{nameof(MainMenuView)}", config.mainPlayerCanvas.transform, false);
            Context.RegisterPrefab<HubView>($"Prefabs/UI/{nameof(HubView)}", config.mainPlayerCanvas.transform, false);
            Context.RegisterPrefab<LevelView>($"Prefabs/UI/{nameof(LevelView)}", config.mainPlayerCanvas.transform, false);
            Context.RegisterPrefab<CharacterSelectionView>($"Prefabs/UI/{nameof(CharacterSelectionView)}", config.mainPlayerCanvas.transform, false);

            Context.RegisterPrefab<PlayerView>($"Prefabs/UI/{nameof(PlayerView)}", config.overlayCanvas.transform, false);
        }

        private Library CreateDataset()
        {
            var library = new Dataset();

            library.Projectiles.Add(new ProjectileData()
                {
                    Id = 1,
                    Prefab = "Prefabs/Projectiles/Projectile_Rocket",
                    ImpactPrefab = "Prefabs/Projectiles/Projectile_Grenade_Impact",
                    Speed = 20f,
                    RicochetOnCollision = true,
                }
            );

            library.Projectiles.Add(new ProjectileData()
                {
                    Id = 2,
                    Prefab = "Prefabs/Projectiles/Projectile_Rocket",
                    ImpactPrefab = "Prefabs/Projectiles/Projectile_Grenade_Impact",
                    Speed = 20f,
                    DestroyOnCollision = true,
                    IsHoming = true,
                    HomingSpeed = 300f,
                    HomingRadius = 15f,
                }
            );

            library.Projectiles.Add(new ProjectileData()
                {
                    Id = 3,
                    Prefab = "Prefabs/Projectiles/Projectile_Plasma",
                    ImpactPrefab = "Prefabs/Projectiles/Projectile_Plasma_Impact",
                    Speed = 40f,
                    DestroyOnCollision = true,
                }
            );

            library.Projectiles.Add(new ProjectileData()
                {
                    Id = 4,
                    Prefab = "Prefabs/Projectiles/Projectile_Grenade",
                    ImpactPrefab = "Prefabs/Projectiles/Projectile_Grenade_Impact",
                    Speed = 25f,
                    DestroyOnCollision = false,
                }
            );

            library.Weapon.Add(new WeaponData
                {
                    Id = 1,
                    Type = WeaponType.Hitscan,
                    Prefab = "Prefabs/Weapon/Shotgun",
                    Range = 25,
                    CooldownTime = 0.09f,
                    OriginParticlesPrefab = "Prefabs/Muzzles/Muzzle_Rifle",
                    TargetParticlesPrefab = "Prefabs/Projectiles/Projectile_Bullet_Impact",
                }
            );

            library.Weapon.Add(new WeaponData
                {
                    Id = 2,
                    Type = WeaponType.Hitscan,
                    Prefab = "Prefabs/Weapon/Shotgun",
                    Range = 25,
                    CooldownTime = 0.09f,
                    OriginParticlesPrefab = "Prefabs/Muzzles/Muzzle_Rifle",
                    TargetParticlesPrefab = "Prefabs/Projectiles/Projectile_Bullet_Impact",
                }
            );

            library.Weapon.Add(new WeaponData
                {
                    Id = 3,
                    Type = WeaponType.Projectile,
                    Prefab = "Prefabs/Weapon/Shotgun",
                    Range = 25,
                    CooldownTime = 0.1f,
                    ProjectileId = 2,
                }
            );

            library.Weapon.Add(new WeaponData
                {
                    Id = 4,
                    Type = WeaponType.Projectile,
                    Prefab = "Prefabs/Weapon/Shotgun",
                    Range = 25,
                    CooldownTime = 0.1f,
                    ProjectileId = 3,
                }
            );

            library.Weapon.Add(new WeaponData
                {
                    Id = 5,
                    Type = WeaponType.Projectile,
                    Prefab = "Prefabs/Weapon/Shotgun",
                    Range = 25,
                    CooldownTime = 0.5f,
                    ProjectileId = 4,
                }
            );

            library.Weapon.Add(new WeaponData
                {
                    Id = 6,
                    Type = WeaponType.Hitscan,
                    Prefab = "Prefabs/Weapon/Shotgun",
                    Range = 25,
                    CooldownTime = 1f,
                    ShotSpreadCount = 5,
                    ShotSpreadDegrees = 3,
                    OriginParticlesPrefab = "Prefabs/Muzzles/Muzzle_Shotgun",
                    TargetParticlesPrefab = "Prefabs/Projectiles/Projectile_Bullet_Impact",
                }
            );

            library.Weapon.Add(new WeaponData
                {
                    Id = 7,
                    Type = WeaponType.Projectile,
                    Prefab = "Prefabs/Weapon/Shotgun",
                    Range = 25,
                    CooldownTime = 1f,
                    ProjectileId = 1,
                }
            );

            library.Weapon.Add(new WeaponData
                {
                    Id = 8,
                    Type = WeaponType.Beam,
                    Prefab = "Prefabs/Weapon/Shotgun",
                    Range = 25,
                    CooldownTime = 0.1f,
                    ShotSpreadCount = 3,
                    ShotSpreadDegrees = 30,
                    BeamReflectionCount = 2,
                    BeamPrefab = "Prefabs/Beams/Beam_Railgun",
                    OriginParticlesPrefab = "Prefabs/Beams/Beam_Railgun_Origin",
                    TargetParticlesPrefab = "Prefabs/Beams/Beam_Railgun_Target",
                }
            );

            return new Library(library);
        }
    }
}