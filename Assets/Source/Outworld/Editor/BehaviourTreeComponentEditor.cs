using System;
using System.Linq;
using Outworld.Game.AI;
using Outworld.Game.Components.Units;
using UnityEditor;
using UnityEngine;

namespace Outworld.Editor
{
    [CustomEditor(typeof(BehaviourTreeComponent))]
    public class BehaviourTreeComponentEditor : UnityEditor.Editor
    {
        private BehaviourTreeComponent behaviourTree;

        private void OnEnable()
        {
            this.behaviourTree = target as BehaviourTreeComponent;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (this.behaviourTree.BehaviourTree == null)
            {
                return;
            }

            GUILayout.Space(25);
            EditorGUILayout.LabelField("Target Entity", this.behaviourTree.BehaviourTree.Context.TargetUnit == null ? "Null" : this.behaviourTree.BehaviourTree.Context.TargetUnit.name);
            EditorGUILayout.LabelField("Target Point", this.behaviourTree.BehaviourTree.Context.TargetPoint.HasValue ? this.behaviourTree.BehaviourTree.Context.TargetPoint.Value.ToString() : "Null");
            EditorGUILayout.LabelField("Opened Nodes", string.Join(", ", this.behaviourTree.BehaviourTree.Context.RunningNodes.Select(node => node.GetType().Name)));
            GUILayout.Space(25);
            DisplayParentNode(this.behaviourTree.BehaviourTree, this.behaviourTree.BehaviourTree.Root, 0);
            GUILayout.Space(25);
        }

        private void DisplayParentNode(BehaviourTree tree, BehaviourTreeNodeContainer node, int level)
        {
            var style = new GUIStyle(GUI.skin.label)
            {
                richText = true,
                padding = new RectOffset(10 * level, 0, 0, 0),
                fontStyle = tree.GetFirstRunningNode().Equals(node) ? FontStyle.Bold : FontStyle.Normal
            };

            GUILayout.Label($"<color=#{GetColorByStatus(node.GetLastStatus())}>{node.GetType().Name}</color>", style);

            foreach (var child in node.Nodes)
            {
                if (child is BehaviourTreeNodeContainer subParent)
                {
                    DisplayParentNode(tree, subParent, level + 1);
                    continue;
                }

                style.padding = new RectOffset(10 * (level + 1), 0, 0, 0);
                style.fontStyle = tree.GetFirstRunningNode().Equals(child) ? FontStyle.Bold : FontStyle.Normal;

                GUILayout.Label($"<color=#{GetColorByStatus(child.GetLastStatus())}>{child.GetType().Name}</color>", style);
            }
        }

        private static string GetColorByStatus(BehaviourTreeStatus status)
        {
            switch (status)
            {
                case BehaviourTreeStatus.Success:
                    return "008800";
                case BehaviourTreeStatus.Failure:
                    return "880000";
                case BehaviourTreeStatus.Running:
                    return "000088";
                case BehaviourTreeStatus.None:
                    return "333333";
                case BehaviourTreeStatus.Waiting:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return "ff00ff";
        }
    }
}