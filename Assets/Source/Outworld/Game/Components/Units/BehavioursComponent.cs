using System;
using System.Collections.Generic;
using UnityEngine;
using Behaviour = Outworld.Game.Behaviours.Behaviour;

namespace Outworld.Game.Components.Units
{
    public class BehavioursComponent : MonoBehaviour
    {
        public event Action<Behaviour> BehaviourApplyEvent;
        public event Action<Behaviour> BehaviourRemoveEvent;

        public readonly List<Behaviour> Behaviours = new List<Behaviour>();

        public void Apply(GameObject caster, Behaviour behaviour)
        {
            behaviour.Apply(caster, gameObject);
            behaviour.RemoveEvent += OnBehaviourRemoved;

            this.Behaviours.Add(behaviour);

            BehaviourApplyEvent?.Invoke(behaviour);
        }

        private void OnBehaviourRemoved(Behaviour behaviour)
        {
            this.Behaviours.Remove(behaviour);

            BehaviourRemoveEvent?.Invoke(behaviour);
        }

        private void Update()
        {
            foreach (var behaviour in this.Behaviours)
            {
                behaviour.Update(Time.deltaTime);
            }
        }
    }
}