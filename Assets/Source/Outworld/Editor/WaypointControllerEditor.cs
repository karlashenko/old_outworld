using Outworld.Game.Pathfinding;
using UnityEditor;
using UnityEngine;

namespace Outworld.Editor
{
    [CustomEditor(typeof(WaypointGenerator))]
    public class WaypointControllerEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            GUILayout.Space(25);

            if (!GUILayout.Button("Generate"))
            {
                return;
            }

            ((WaypointGenerator) target).Generate();
        }
    }
}