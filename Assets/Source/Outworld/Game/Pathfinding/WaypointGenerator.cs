using System;
using Outworld.Game.Extensions;
using Unity.Collections;
using UnityEngine;

namespace Outworld.Game.Pathfinding
{
    [ExecuteInEditMode]
    public class WaypointGenerator : MonoBehaviour
    {
        public static WaypointGenerator Instance { get; private set; }

        [SerializeField]
        private Vector2 origin;

        [SerializeField]
        private int width;

        [SerializeField]
        private int height;

        [SerializeField]
        private int cellSize;

        [SerializeField]
        private int jumpRange;

        [SerializeField]
        private LayerMask collisionMask;

        private Vector2 size;
        private NativeArray<Waypoint> waypoints;
        private NativeMultiHashMap<int, WaypointConnection> connections;

        private void Start()
        {
            Instance = this;

            Generate();
        }

        private void OnDestroy()
        {
            Dispose();
        }

        public int GetCellSize()
        {
            return this.cellSize;
        }

        public void Generate(Vector2 origin, int width, int height)
        {
            this.origin = origin;
            this.width = width;
            this.height = height;

            Generate();
        }

        public void Generate()
        {
            Dispose();
            CreateWaypoints();
            CreateWaypointConnections();
        }

        private void Dispose()
        {
            if (this.waypoints.IsCreated)
            {
                this.waypoints.Dispose();
            }

            if (this.connections.IsCreated)
            {
                this.connections.Dispose();
            }
        }

        private void CreateWaypoints()
        {
            this.size = new Vector2(this.width * this.cellSize, this.height * this.cellSize);
            this.waypoints = new NativeArray<Waypoint>(this.width * this.height, Allocator.Persistent);

            for (var y = 0; y < this.height; y++)
            {
                for (var x = 0; x < this.width; x++)
                {
                    var index = x + y * this.width;
                    var type = WaypointType.Empty;
                    var position = new Vector2(x * this.cellSize + this.cellSize / 2f, y * this.cellSize + this.cellSize / 2f) + this.origin;
                    var isEdge = false;
                    var isSlope = false;

                    if (Physics2D.Raycast(position, Vector2.down, 0, this.collisionMask))
                    {
                        type = WaypointType.Solid;
                    }
                    else
                    {
                        var raycastHit2D = Physics2D.Raycast(position, Vector2.down, this.cellSize, this.collisionMask);

                        if (raycastHit2D)
                        {
                            position = raycastHit2D.point;
                            type = WaypointType.Walkable;
                            isSlope = Mathf.Abs(raycastHit2D.normal.x) > 0.01f;
                        }
                    }

                    if (x > 0)
                    {
                        var left = x - 1 + y * this.width;

                        isEdge = type == WaypointType.Walkable && this.waypoints[left].Type == WaypointType.Empty;

                        if (!this.waypoints[left].IsEdge && this.waypoints[left].Type == WaypointType.Walkable && type == WaypointType.Empty)
                        {
                            this.waypoints[left] = this.waypoints[left].WithIsEdge(true);
                        }
                    }

                    this.waypoints[index] = new Waypoint(index, position, type, isEdge, isSlope);
                }
            }
        }

        private void CreateWaypointConnections()
        {
            this.connections = new NativeMultiHashMap<int, WaypointConnection>(4, Allocator.Persistent);

            foreach (var cell in this.waypoints)
            {
                if (cell.Type != WaypointType.Walkable)
                {
                    continue;
                }

                CreateWalkConnections(cell);
                CreateFallConnections(cell);
                CreateJumpConnections(cell);
            }
        }

        private void CreateJumpConnections(Waypoint waypoint)
        {
            if (FirstAbove(WaypointType.Solid, IndexX(waypoint.Index), IndexY(waypoint.Index), this.jumpRange) > 0)
            {
                return;
            }

            for (var yOffset = -this.jumpRange; yOffset <= this.jumpRange; yOffset++)
            {
                for (var xOffset = -this.jumpRange; xOffset <= this.jumpRange; xOffset++)
                {
                    var x = IndexX(waypoint.Index) + xOffset;
                    var y = IndexY(waypoint.Index) + yOffset;
                    var i = IndexAt(x, y);

                    if (!ValidateWaypointType(i, WaypointType.Walkable) || this.waypoints[i].IsSlope)
                    {
                        continue;
                    }

                    if (Math.Abs(waypoint.Position.x - this.waypoints[i].Position.x) < this.cellSize || waypoint.Position.y - this.waypoints[i].Position.y > this.cellSize * 2)
                    {
                        continue;
                    }

                    if (Mathf.Abs(IndexX(waypoint.Index) - x) < 2)
                    {
                        continue;
                    }

                    if (IndexX(waypoint.Index) > x && ValidateWaypointType(IndexAt(x + 1, y - 1), WaypointType.Solid) ||
                        IndexX(waypoint.Index) < x && ValidateWaypointType(IndexAt(x - 1, y - 1), WaypointType.Solid))
                    {
                        continue;
                    }

                    var sqrMagnitude = (this.waypoints[i].Position - waypoint.Position).sqrMagnitude;

                    if (sqrMagnitude > this.jumpRange * this.jumpRange)
                    {
                        continue;
                    }

                    AddConnection(waypoint.Index, new WaypointConnection(i, WaypointConnectionType.Jump));
                    AddConnection(i, new WaypointConnection(waypoint.Index, WaypointConnectionType.Jump));
                }
            }
        }

        private void CreateFallConnections(Waypoint cell)
        {
            if (!this.waypoints[cell.Index].IsEdge)
            {
                return;
            }

            var x = IndexX(cell.Index);
            var y = IndexY(cell.Index);

            var rightNeighbourIndex = IndexAt(x + 1, y);
            var leftNeighbourIndex = IndexAt(x - 1, y);

            if (ValidateWaypointType(leftNeighbourIndex, WaypointType.Empty))
            {
                var index = FirstBelow(WaypointType.Walkable, x - 1, y, 0);

                if (index >= 0 && !(cell.IsSlope && this.waypoints[index].IsSlope))
                {
                    AddConnection(cell.Index, new WaypointConnection(index, WaypointConnectionType.Fall));
                }
            }

            if (ValidateWaypointType(rightNeighbourIndex, WaypointType.Empty))
            {
                var index = FirstBelow(WaypointType.Walkable, x + 1, y, 0);

                if (index >= 0 && !(cell.IsSlope && this.waypoints[index].IsSlope))
                {
                    AddConnection(cell.Index, new WaypointConnection(index, WaypointConnectionType.Fall));
                }
            }
        }

        private void CreateWalkConnections(Waypoint cell)
        {
            var x = IndexX(cell.Index);
            var y = IndexY(cell.Index);

            var rightNeighbourIndex = IndexAt(x + 1, y);

            if (ValidateWaypointType(rightNeighbourIndex, WaypointType.Walkable))
            {
                AddConnection(cell.Index, new WaypointConnection(rightNeighbourIndex, WaypointConnectionType.Walk));
                AddConnection(rightNeighbourIndex, new WaypointConnection(cell.Index, WaypointConnectionType.Walk));
                return;
            }

            var topRightNeighbour = IndexAt(x + 1, y + 1);

            if (ValidateWaypointType(topRightNeighbour, WaypointType.Walkable))
            {
                if (Mathf.Abs(this.waypoints[topRightNeighbour].Position.y - cell.Position.y) <= this.cellSize / 2f || cell.IsSlope && this.waypoints[topRightNeighbour].IsSlope)
                {
                    AddConnection(cell.Index, new WaypointConnection(topRightNeighbour, WaypointConnectionType.Walk));
                    AddConnection(topRightNeighbour, new WaypointConnection(cell.Index, WaypointConnectionType.Walk));
                }
            }

            var bottomRightNeighbour = IndexAt(x + 1, y - 1);

            if (ValidateWaypointType(bottomRightNeighbour, WaypointType.Walkable))
            {
                if (Mathf.Abs(this.waypoints[bottomRightNeighbour].Position.y - cell.Position.y) <= this.cellSize / 2f || cell.IsSlope && this.waypoints[bottomRightNeighbour].IsSlope)
                {
                    AddConnection(cell.Index, new WaypointConnection(bottomRightNeighbour, WaypointConnectionType.Walk));
                    AddConnection(bottomRightNeighbour, new WaypointConnection(cell.Index, WaypointConnectionType.Walk));
                }
            }
        }

        private void AddConnection(int index, WaypointConnection connection)
        {
            this.connections.Add(index, connection);
        }

        public Waypoint GetWaypointAt(int index)
        {
            if (this.waypoints.IndexInBounds(index))
            {
                return this.waypoints[index];
            }

            Debug.LogError($"No waypoint at index {index.ToString()}");
            return Waypoint.Empty;
        }

        public NativeArray<Waypoint> GetWaypoints()
        {
            return this.waypoints;
        }

        public NativeMultiHashMap<int, WaypointConnection> GetConnections()
        {
            return this.connections;
        }

        public Waypoint FindClosestWalkableToWorldPoint(Vector2 point, int range = 5)
        {
            var relative = point - this.origin;

            var x = Math.Min((int) (relative.x / this.cellSize), this.width - 1);
            var y = Math.Min((int) (relative.y / this.cellSize), this.height - 1);

            var distanceMin = Mathf.Infinity;
            var closestIndex = -1;

            for (var yOffset = -range; yOffset < range; yOffset++)
            {
                for (var xOffset = -range; xOffset < range; xOffset++)
                {
                    var index = IndexAt(x + xOffset, y + yOffset);

                    if (!ValidateWaypointType(index, WaypointType.Walkable))
                    {
                        continue;
                    }

                    var distance = (point - this.waypoints[index].Position).sqrMagnitude;

                    if (distance > distanceMin)
                    {
                        continue;
                    }

                    distanceMin = distance;
                    closestIndex = index;
                }
            }

            return closestIndex == -1 ? Waypoint.Empty : this.waypoints[closestIndex];
        }

        private bool ValidateWaypointType(int index, WaypointType type)
        {
            if (!this.waypoints.IndexInBounds(index))
            {
                return false;
            }

            return this.waypoints[index].Type == type;
        }

        private int IndexAt(int x, int y)
        {
            return x + y * this.width;
        }

        private int IndexX(int index)
        {
            return index % this.width;
        }

        private int IndexY(int index)
        {
            return index / this.width;
        }

        private int FirstBelow(WaypointType type, int cellX, int cellY, int minY)
        {
            for (var y = cellY; y > minY; y--)
            {
                var index = cellX + y * this.width;

                if (!this.waypoints.IndexInBounds(index))
                {
                    break;
                }

                if (this.waypoints[index].Type == type)
                {
                    return index;
                }
            }

            return -1;
        }

        private int FirstAbove(WaypointType type, int cellX, int cellY, int maxY)
        {
            for (var yOffset = 0; yOffset < maxY; yOffset++)
            {
                var index = cellX + (cellY + yOffset) * this.width;

                if (!this.waypoints.IndexInBounds(index))
                {
                    break;
                }

                if (this.waypoints[index].Type == type)
                {
                    return index;
                }
            }

            return -1;
        }

        private void OnDrawGizmosSelected()
        {
            if (!this.waypoints.IsCreated || !this.connections.IsCreated)
            {
                return;
            }

            foreach (var waypoint in this.waypoints)
            {
                if (waypoint.Type != WaypointType.Walkable)
                {
                    continue;
                }

                Gizmos.DrawIcon(waypoint.Position, "winbtn_mac_max@2x");

                foreach (var connection in this.connections.GetValuesForKey(waypoint.Index))
                {
                    Gizmos.color = connection.Type switch
                    {
                        WaypointConnectionType.Fall => Color.yellow,
                        WaypointConnectionType.Walk => Color.green,
                        WaypointConnectionType.Jump => Color.blue,
                        _ => Gizmos.color
                    };

                    Gizmos.DrawLine(waypoint.Position, this.waypoints[connection.WaypointIndex].Position);
                }
            }

            var topLeft = new Vector3(this.origin.x, this.size.y + this.origin.y);
            var topRight = new Vector3(this.size.x + this.origin.x, this.size.y + this.origin.y);
            var bottomLeft = new Vector3(this.origin.x, this.origin.y);
            var bottomRight = new Vector3(this.size.x + this.origin.x, this.origin.y);

            Gizmos.color = Color.cyan;
            Gizmos.DrawLine(topLeft, topRight);
            Gizmos.DrawLine(topRight, bottomRight);
            Gizmos.DrawLine(bottomRight, bottomLeft);
            Gizmos.DrawLine(bottomLeft, topLeft);

            Gizmos.color = Color.cyan.With(a: 0.1f);

            for (var i = 1; i < this.width; i++)
            {
                Gizmos.DrawLine(topLeft + new Vector3(i * this.cellSize, 0), bottomLeft + new Vector3(i * this.cellSize, 0));
            }

            for (var i = 1; i < this.height; i++)
            {
                Gizmos.DrawLine(bottomLeft + new Vector3(0, i * this.cellSize), bottomRight + new Vector3(0, i * this.cellSize));
            }
        }
    }
}