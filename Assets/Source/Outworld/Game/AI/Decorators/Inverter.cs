﻿using System;
using System.Linq;

namespace Outworld.Game.AI.Decorators
{
    public class Inverter : BehaviourTreeNodeContainer
    {
        protected override BehaviourTreeStatus OnTick(float deltaTime, BehaviourTreeContext context)
        {
            if (this.Nodes.Count == 0)
            {
                throw new Exception($"{GetType().Name} must have a child node!");
            }

            var status = this.Nodes.First().Tick(deltaTime, context);

            if (status == BehaviourTreeStatus.Failure)
            {
                return BehaviourTreeStatus.Success;
            }

            if (status == BehaviourTreeStatus.Success)
            {
                return BehaviourTreeStatus.Failure;
            }

            return status;
        }

        public virtual void Open()
        {
        }
    }
}
