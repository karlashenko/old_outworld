using Outworld.Game.Levels;
using Outworld.Game.UI.Views;
using Outworld.Game.Utilities;
using UnityEngine;

namespace Outworld.Game.States
{
    public class LevelGameState : GameState
    {
        private readonly LevelGenerator levelGenerator;
        private readonly LevelView levelView;

        public LevelGameState()
        {
            this.levelGenerator = Context.Get<LevelGenerator>();
            this.levelView = Context.InstantiatePrefab<LevelView>();
        }

        protected override void OnEnter()
        {
            //this.levelView.Show(PlayerInput.all[0]);
            this.levelGenerator.GeneratorConfig.RootGameObject.SetActive(true);
            this.levelGenerator.Generate();
        }

        protected override void OnExit()
        {
            //this.levelView.Hide();
            this.levelGenerator.GeneratorConfig.RootGameObject.SetActive(false);
        }

        protected override void OnDestroy()
        {
            Object.Destroy(this.levelView.gameObject);
        }

        public override void Update(float deltaTime)
        {
        }
    }
}