﻿using System;
using System.Linq;

namespace Outworld.Game.AI.Decorators
{
    public class Succeeder : BehaviourTreeNodeContainer
    {
        protected override BehaviourTreeStatus OnTick(float deltaTime, BehaviourTreeContext context)
        {
            if (this.Nodes.Count == 0)
            {
                throw new ApplicationException($"{GetType().Name} must have a child node!");
            }

            return this.Nodes.First().Tick(deltaTime, context) == BehaviourTreeStatus.Running
                ? BehaviourTreeStatus.Running
                : BehaviourTreeStatus.Success;
        }
    }
}
