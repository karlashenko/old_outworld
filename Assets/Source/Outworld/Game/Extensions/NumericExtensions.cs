using UnityEngine;

namespace Outworld.Game.Extensions
{
    public static class NumericExtensions
    {
        public static bool InRange(this int value, int minimum, int maximum)
        {
            return value >= minimum && value <= maximum;
        }

        public static bool InRange(this float value, float minimum, float maximum)
        {
            return value >= minimum && value <= maximum;
        }

        public static float Snap(this float value, float snap)
        {
            return Mathf.RoundToInt(value / snap) * snap;
        }

        public static int Clamp(this int value, int min, int max)
        {
            if (value < min)
            {
                value = min;
            }
            else if (value > max)
            {
                value = max;
            }

            return value;
        }
    }
}