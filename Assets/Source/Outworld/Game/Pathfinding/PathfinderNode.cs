using System;
using UnityEngine;

namespace Outworld.Game.Pathfinding
{
    public readonly struct PathfinderNode : IEquatable<PathfinderNode>
    {
        public static readonly PathfinderNode Empty = new PathfinderNode(default, -1, -1, WaypointConnectionType.Walk, 0, 0, 0);

        public readonly Vector2 Position;
        public readonly int WaypointIndex;
        public readonly int ParentWaypointIndex;
        public readonly WaypointConnectionType WaypointConnectionType;
        public readonly float G;
        public readonly float H;
        public readonly float F;

        public PathfinderNode(Vector2 position, int waypointIndex, int parentWaypointIndex, WaypointConnectionType waypointConnectionType, float g, float h, float f)
        {
            this.Position = position;
            this.WaypointIndex = waypointIndex;
            this.ParentWaypointIndex = parentWaypointIndex;
            this.WaypointConnectionType = waypointConnectionType;
            this.G = g;
            this.H = h;
            this.F = f;
        }

        public PathfinderNode WithParentWaypointIndex(int parentWaypointIndex)
        {
            return new PathfinderNode(this.Position, this.WaypointIndex, parentWaypointIndex, this.WaypointConnectionType, this.G, this.H, this.F);
        }

        public PathfinderNode WithConnectionType(WaypointConnectionType waypointConnectionType)
        {
            return new PathfinderNode(this.Position, this.WaypointIndex, this.ParentWaypointIndex, waypointConnectionType, this.G, this.H, this.F);
        }

        public PathfinderNode WithParentWaypointIndexAndConnectionType(int parentWaypointIndex, WaypointConnectionType waypointConnectionType)
        {
            return new PathfinderNode(this.Position, this.WaypointIndex, parentWaypointIndex, waypointConnectionType, this.G, this.H, this.F);
        }

        public bool IsEmpty()
        {
            return this.WaypointIndex == Empty.WaypointIndex;
        }

        public static bool operator ==(PathfinderNode a, PathfinderNode b)
        {
            return a.Equals(b);
        }

        public static bool operator !=(PathfinderNode a, PathfinderNode b)
        {
            return !a.Equals(b);
        }

        public override bool Equals(object other)
        {
            return other is PathfinderNode node && node.Equals(this);
        }

        public bool Equals(PathfinderNode other)
        {
            return other.ParentWaypointIndex == this.ParentWaypointIndex;
        }

        public override int GetHashCode()
        {
            return this.ParentWaypointIndex;
        }
    }
}