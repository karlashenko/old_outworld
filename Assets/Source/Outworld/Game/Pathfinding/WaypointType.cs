namespace Outworld.Game.Pathfinding
{
    public enum WaypointType
    {
        Empty,
        Solid,
        Walkable,
    }
}