using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using Newtonsoft.Json;
using Outworld.Data;
using Outworld.Game.Utilities;
using UnityEngine;

// ReSharper disable InconsistentNaming

namespace Outworld.Game.Internationalization
{
    public class I18n
    {
        private const string FallbackLocale = "en-US";

        public static event Action<I18n> LocaleChangeEvent;

        public string CurrentLocale { get; private set; }

        private readonly Dictionary<string, I18nData> i18n;

        private Dictionary<string, string> dictionary;

        public I18n()
        {
            this.i18n = Context.Get<Library>().I18n;

            CurrentLocale = this.i18n.ContainsKey(CultureInfo.CurrentCulture.Name) ? CultureInfo.CurrentCulture.Name : FallbackLocale;

            ChangeLocale(CurrentLocale);
        }

        public string Translate(string key)
        {
            if (!this.i18n.ContainsKey(key))
            {
                return key;
            }

            var data = this.i18n[key];

            // TODO:
            // 1. fallback dictionary
            // 2. string variables

            return this.dictionary[data.Key];
        }

        public void ChangeLocale(string locale)
        {
            var filename = $"{Application.streamingAssetsPath}/{locale}.json";

            if (!File.Exists(filename))
            {
                Debug.Log($"Cannot find translation file: '{filename}'");
                return;
            }

            CurrentLocale = locale;

            this.dictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(File.ReadAllText(filename));

            LocaleChangeEvent?.Invoke(this);
        }
    }
}