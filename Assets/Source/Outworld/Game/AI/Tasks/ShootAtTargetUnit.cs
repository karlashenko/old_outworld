using Outworld.Game.Components.Units;
using Outworld.Game.Extensions;

namespace Outworld.Game.AI.Tasks
{
    public class ShootAtTargetUnit : BehaviourTreeLogicNode
    {
        private WeaponComponent weapon;
        private AimComponent aim;

        public ShootAtTargetUnit(BehaviourTreePropertiesData properties) : base(properties)
        {
        }

        protected override void OnStart(BehaviourTreeContext context)
        {
            this.weapon = context.Unit.GetComponent<WeaponComponent>();
            this.aim = context.Unit.GetComponent<AimComponent>();
        }

        protected override void OnStop(BehaviourTreeContext context)
        {
            this.weapon.StopFire();
        }

        protected override BehaviourTreeStatus OnTick(float deltaTime, BehaviourTreeContext context)
        {
            var target = context.TargetUnit.transform.position;
            var origin = context.Unit.transform.position;

            if (!this.weapon.IsInRangeAndOnLineOfSight(context.TargetUnit.transform.position))
            {
                return BehaviourTreeStatus.Failure;
            }

            var direction = (target - origin).normalized;

            this.aim.Aim(direction.DirectionToDegrees(), direction);
            this.weapon.StartFire();

            return BehaviourTreeStatus.Running;
        }
    }
}