using UnityEngine;

namespace Outworld.Game.Components.Environment
{
    [RequireComponent(typeof(EdgeCollider2D))]
    public class MovingPlatformComponent : MonoBehaviour
    {
        [SerializeField] private float velocity;
        [SerializeField] private Transform[] checkpoints;

        private int checkpointIndex;
        private int checkpointDirection = 1;

        private void Update()
        {
            var distance = (transform.position - this.checkpoints[this.checkpointIndex].position).magnitude;

            if (distance < this.velocity * Time.deltaTime * 2)
            {
                this.checkpointIndex += this.checkpointDirection;

                // ReSharper disable once InvertIf
                if (this.checkpointIndex == -1 || this.checkpointIndex == this.checkpoints.Length)
                {
                    this.checkpointIndex = Mathf.Clamp(this.checkpointIndex, 0, this.checkpoints.Length - 1);
                    this.checkpointDirection *= -1;
                }

                return;
            }

            var direction = (this.checkpoints[this.checkpointIndex].position - transform.position).normalized;
            var translation = direction * this.velocity * Time.deltaTime;

            transform.Translate(translation);
        }
    }
}