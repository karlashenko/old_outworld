using Outworld.Game.Components;
using Outworld.Game.Components.Units;
using Outworld.Game.Items;
using Outworld.Game.Managers;
using Outworld.Game.UI.Views;
using Outworld.Game.Utilities;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Outworld.Game.States
{
    public class CharacterSelectionGameState : GameState
    {
        private readonly CharacterManager characterManager;
        private readonly PlayerInputManager playerInputManager;
        private readonly CharacterSelectionView characterSelectionView;

        public CharacterSelectionGameState()
        {
            this.characterManager = Context.Get<CharacterManager>();
            this.playerInputManager = Context.Get<PlayerInputManager>();
            this.characterSelectionView = Context.InstantiatePrefab<CharacterSelectionView>();
        }

        protected override void OnEnter()
        {
            this.characterSelectionView.Show(PlayerInput.all[0]);
            this.characterSelectionView.ContinueButtonClickEvent += ContinueButtonClickEvent;

            this.playerInputManager.onPlayerJoined += OnPlayerJoined;
            this.playerInputManager.onPlayerLeft += OnPlayerLeft;
            this.playerInputManager.EnableJoining();

            foreach (var player in PlayerInput.all)
            {
                OnPlayerJoined(player);
            }
        }

        protected override void OnExit()
        {
            this.characterSelectionView.Hide();
            this.characterSelectionView.ContinueButtonClickEvent -= ContinueButtonClickEvent;

            this.playerInputManager.onPlayerJoined -= OnPlayerJoined;
            this.playerInputManager.onPlayerLeft -= OnPlayerLeft;
            this.playerInputManager.DisableJoining();
        }

        protected override void OnDestroy()
        {
            Object.Destroy(this.characterSelectionView.gameObject);
        }

        private void ContinueButtonClickEvent()
        {
            foreach (var playerInput in PlayerInput.all)
            {
                var character = CreateCharacter(playerInput);
                this.characterManager.Characters.Add(character);

                var player = playerInput.GetComponent<PlayerComponent>();
                player.Construct(character);
            }

            Context.Get<GameStateManager>().ToHub();
        }

        private static CharacterComponent CreateCharacter(PlayerInput playerInput)
        {
            var character = Object.Instantiate(Resources.Load<CharacterComponent>("Prefabs/Character"));
            character.playerInput = playerInput;

            var inventoryComponent = character.gameObject.AddComponent<InventoryComponent>();
            inventoryComponent.Construct(16, 8);

            inventoryComponent.Pickup(new Item(1, 1, 1, "Sprites/Items/Anatomical_Knowledge_inventory_icon"));
            inventoryComponent.Pickup(new Item(2, 2, 2, "Sprites/Items/Atziri%27s_Acuity_inventory_icon"));
            inventoryComponent.Pickup(new Item(3, 1, 3, "Sprites/Items/Aurumvorax_inventory_icon"));
            inventoryComponent.Pickup(new Item(4, 1, 3, "Sprites/Items/The_Goddess_Bound_inventory_icon"));
            inventoryComponent.Pickup(new Item(5, 2, 4, "Sprites/Items/The_Harvest_inventory_icon"));
            return character;
        }

        private void OnPlayerJoined(PlayerInput playerInput)
        {
            playerInput.gameObject.name = $"Player_{playerInput.playerIndex.ToString()}";
            playerInput.transform.parent = GameObject.Find("Players").transform;

            this.characterSelectionView.OnPlayerJoined(playerInput);
        }

        private void OnPlayerLeft(PlayerInput playerInput)
        {
            this.characterSelectionView.OnPlayerLeft(playerInput);
        }
    }
}