﻿using System.Linq;
using UnityEngine;

namespace Outworld.Game.AI
{
    public class BehaviourTree
    {
        public readonly BehaviourTreeContext Context;
        public readonly BehaviourTreeNodeContainer Root;

        public BehaviourTree(BehaviourTreeNodeContainer root, GameObject unit)
        {
            this.Root = root;
            this.Context = new BehaviourTreeContext(unit);
        }

        public BehaviourTreeNode GetFirstRunningNode()
        {
            return this.Context.RunningNodes.Count > 0 ? this.Context.RunningNodes.First() : this.Root;
        }

        public void Tick(float deltaTime)
        {
            GetFirstRunningNode().Tick(deltaTime, this.Context);
        }

        public void DrawGizmos()
        {
            GetFirstRunningNode().DrawGizmos();
        }
    }
}