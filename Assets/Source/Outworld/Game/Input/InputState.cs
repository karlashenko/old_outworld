using UnityEngine;

namespace Outworld.Game.Input
{
    public struct InputState
    {
        public bool IsUsingGamepad;
        public Vector2 Pointer;
        public Vector2 RightStickVector;
        public float RightStickAngle;
        public Vector2 LeftStickVector;
        public float LeftStickAngle;
        public bool MoveLeft;
        public bool MoveRight;
        public bool MoveUp;
        public bool MoveDown;
        public bool Crouch;
        public bool DashPressed;
        public bool JumpPressed;
        public bool JumpReleased;
        public bool Action0Pressed;
        public bool Action1Pressed;
        public bool Action2Pressed;
        public bool Action3Pressed;
        public bool Action4Pressed;
        public bool Action5Pressed;
        public bool Action0Released;
        public bool Action1Released;
        public bool Action2Released;
        public bool Action3Released;
        public bool Action4Released;
        public bool Action5Released;

        public void CleanupTriggers()
        {
            this.DashPressed = false;

            this.JumpPressed = false;
            this.JumpReleased = false;

            this.Action0Pressed = false;
            this.Action1Pressed = false;
            this.Action2Pressed = false;
            this.Action3Pressed = false;
            this.Action4Pressed = false;
            this.Action5Pressed = false;

            this.Action0Released = false;
            this.Action1Released = false;
            this.Action2Released = false;
            this.Action3Released = false;
            this.Action4Released = false;
            this.Action5Released = false;
        }
    }
}