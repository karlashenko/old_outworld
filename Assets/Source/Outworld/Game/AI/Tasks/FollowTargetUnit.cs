﻿using System;
using Outworld.Game.Components.Units;
using Outworld.Game.Extensions;
using Outworld.Game.Input;
using Outworld.Game.Movement;
using Outworld.Game.Pathfinding;
using Outworld.Game.Utilities;
using Unity.Collections;
using UnityEngine;

namespace Outworld.Game.AI.Tasks
{
    public class FollowTargetUnit : BehaviourTreeLogicNode
    {
        private readonly Configuration configuration;
        private readonly Pathfinder pathfinder;
        private readonly PathfinderTest pathfinderTest;

        private Transform unit;
        private Transform target;
        private InputState input;
        private bool isJumping;
        private int pathNodeIndex;
        private NativeList<PathfinderNode>? path;
        private AimComponent aim;
        private MovementComponent movement;

        private BehaviourTreeStatus status;

        public FollowTargetUnit(BehaviourTreePropertiesData properties) : base(properties)
        {
            this.configuration = Context.Get<Configuration>();
            this.pathfinder = Context.Get<Pathfinder>();
            this.pathfinderTest = Context.Get<PathfinderTest>();
        }

        protected override void OnStart(BehaviourTreeContext context)
        {
            this.unit = context.Unit.transform;
            this.target = context.TargetUnit.transform;
            this.input = new InputState();
            this.isJumping = false;
            this.pathNodeIndex = 0;
            this.aim = context.Unit.GetComponent<AimComponent>();
            this.movement = context.Unit.GetComponent<MovementComponent>();

            this.status = BehaviourTreeStatus.Running;
        }

        protected override BehaviourTreeStatus OnTick(float deltaTime, BehaviourTreeContext context)
        {
            if (this.status == BehaviourTreeStatus.Running)
            {
                FollowTarget();
                FaceTargetDirection();
            }

            return this.status;
        }

        protected override void OnStop(BehaviourTreeContext context)
        {
            this.path?.Dispose();
            this.path = null;

            if (this.movement.movementBehaviourType == MovementBehaviourType.Ground)
            {
                this.movement.SetInput(new InputState());
            }
            else
            {
                this.movement.SetImpulse(this.movement.GetVelocity());
                this.movement.SetVelocity(Vector2.zero);
            }
        }

        private void FaceTargetDirection()
        {
            var facingDirection = this.target.position.x > this.unit.position.x ? 1 : -1;

            if (this.aim.FacingDirection == facingDirection)
            {
                return;
            }

            var direction = facingDirection > 0 ? Vector2.right : Vector2.left;
            this.aim.Aim(direction.DirectionToDegrees(), direction);
        }

        private void FollowTarget()
        {
            var distance = (this.unit.position - this.target.position).magnitude;

            if (distance <= this.Properties.followTargetStopDistance)
            {
                var direction = (this.target.position - this.unit.position).normalized;
                var hit = Physics2D.Raycast(this.unit.position, direction, distance, this.configuration.collisionMask);

                if (!hit)
                {
                    this.status = BehaviourTreeStatus.Success;
                    return;
                }
            }

            switch (this.movement.movementBehaviourType)
            {
                case MovementBehaviourType.Ground:
                    FollowTargetGround();
                    this.movement.SetInput(this.input);
                    break;
                case MovementBehaviourType.Jetpack:
                    FollowTargetJetpack();
                    break;
                case MovementBehaviourType.Kamikaze:
                    FollowTargetKamikaze();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void FollowTargetKamikaze()
        {
            const float maxSpeed = 12.0f;
            const float maxSteer = 0.03f;

            var direction = (Vector2) (this.target.position - this.unit.position).normalized;
            var desired = direction * maxSpeed;
            var steering = Vector2.ClampMagnitude(desired - this.movement.GetVelocity(), maxSteer);

            this.movement.AddVelocity(steering);
        }

        private void FollowTargetJetpack()
        {
            const float maxSpeed = 12.0f;
            const float maxSteer = 0.03f;
            const float angularSpeed = 8f;

            var approachRange = this.Properties.followTargetStopDistance;
            var sqrApproachRange = approachRange * approachRange;

            var sqrMagnitude = (this.target.position - this.unit.position).sqrMagnitude;
            var speedFactor = Mathf.Min(sqrMagnitude / sqrApproachRange, 1);

            var direction = (Vector2) (this.target.position - this.unit.position).normalized;
            var angular = Vector2.Perpendicular(direction) * angularSpeed;
            var desired = direction * (maxSpeed * speedFactor) + angular * Mathf.Sign(direction.x);

            var steering = Vector2.ClampMagnitude(desired - this.movement.GetVelocity(), maxSteer);

            this.movement.AddVelocity(steering);
        }

        private void FollowTargetGround()
        {
            this.input.MoveRight = false;
            this.input.MoveLeft = false;
            this.input.MoveUp = false;
            this.input.MoveDown = false;

            if (!this.path.HasValue || this.pathNodeIndex > 2 || this.pathNodeIndex >= this.path.Value.Length)
            {
                this.path?.Dispose();
                this.path = this.pathfinder.FindPath(this.unit.position, this.target.position);
                this.pathNodeIndex = 0;

                if (this.path == null)
                {
                    this.status = BehaviourTreeStatus.Failure;
                }

                return;
            }

            var destination = this.path.Value[this.pathNodeIndex];

            var magnitude = (destination.Position - (Vector2) this.unit.position).magnitude;

            if (magnitude < 0.5f)
            {
                this.pathNodeIndex += 1;
                return;
            }

            if (this.isJumping && this.movement.IsGrounded())
            {
                this.isJumping = false;
                this.path?.Dispose();
                this.path = null;
                return;
            }

            if (destination.WaypointConnectionType == WaypointConnectionType.Jump && this.movement.IsGrounded())
            {
                this.isJumping = true;
                JumpTo(destination.Position);
                return;
            }

            if (this.isJumping)
            {
                return;
            }

            this.input.MoveRight = destination.Position.x >= this.unit.position.x;
            this.input.MoveLeft = destination.Position.x < this.unit.position.x;
        }

        private void JumpTo(Vector3 target)
        {
            var origin = this.unit.position - new Vector3(0, 1);
            var height = Mathf.Max(1, target.y - origin.y) + 0.5f;

            this.movement.SetVelocity(CalculateJumpVelocity(origin, target, height, this.movement.GetGravity()));
        }

        private static Vector3 CalculateJumpVelocity(Vector3 origin, Vector3 target, float height, float gravity)
        {
            var delta = target - origin;
            var time = Mathf.Sqrt(2 * height / gravity) + Mathf.Sqrt(2 * Mathf.Abs(delta.y - height) / gravity);

            var velocityX = delta.x / time;
            var velocityY = Mathf.Sqrt(2 * gravity * height);

            return new Vector3(velocityX, velocityY);
        }

        public override void DrawGizmos()
        {
            if (!this.path.HasValue)
            {
                return;
            }

            var capacity = this.path.Value.Length - this.pathNodeIndex - 1;

            if (capacity < 1)
            {
                return;
            }

            var temp = new NativeList<PathfinderNode>(capacity, Allocator.Temp);

            for (var i = this.pathNodeIndex; i < this.path.Value.Length; i++)
            {
                temp.Add(this.path.Value[i]);
            }

            this.pathfinderTest.GizmoDrawPath(temp);
        }
    }
}