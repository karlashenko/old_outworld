using System;
using System.Collections.Generic;
using Outworld.Data;
using Outworld.Data.Types;
using Outworld.Game.Utilities;

namespace Outworld.Game.Behaviours
{
    public class BehaviourFactory
    {
        private readonly Dictionary<int, BehaviourData> behaviours;

        public BehaviourFactory()
        {
            this.behaviours = Context.Get<Library>().Behaviours;
        }

        public Behaviour Create(int behaviourId)
        {
            var data = this.behaviours[behaviourId];

            switch (data.Type)
            {
                case BehaviourType.Buff:
                    return new PeriodicBehaviour(data);
                case BehaviourType.ModifyProperties:
                    return new ModifyPropertiesBehaviour(data);
            }

            throw new ArgumentOutOfRangeException();
        }
    }
}