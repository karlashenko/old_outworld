﻿namespace Outworld.Game.AI.Composites
{
    public class Race : BehaviourTreeNodeContainer
    {
        protected override BehaviourTreeStatus OnTick(float deltaTime, BehaviourTreeContext context)
        {
            var failed = 0;

            foreach (var node in this.Nodes)
            {
                var status = node.Tick(deltaTime, context);

                if (status == BehaviourTreeStatus.Success)
                {
                    return status;
                }

                if (status == BehaviourTreeStatus.Failure)
                {
                    failed += 1;
                }
            }

            return failed == this.Nodes.Count ? BehaviourTreeStatus.Failure : BehaviourTreeStatus.Running;
        }
    }
}