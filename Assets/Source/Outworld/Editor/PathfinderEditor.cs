using Outworld.Game.Pathfinding;
using UnityEditor;

namespace Outworld.Editor
{
    [CustomEditor(typeof(Pathfinder))]
    public class PathfinderEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
        }
    }
}