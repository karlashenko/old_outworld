namespace Outworld.Game.Items
{
    public struct ItemSlot
    {
        public Item Item;
        public int Index;
        public int X;
        public int Y;
        public int ParentIndex;

        public bool IsEmpty()
        {
            return this.Item.IsEmpty() && this.ParentIndex == -1;
        }

        public void Occupy(Item item, int index)
        {
            this.Item = item;
            this.ParentIndex = index;
        }

        public void Clear()
        {
            this.Item = Item.Empty;
            this.ParentIndex = -1;
        }
    }
}