using System;
using System.Collections.Generic;
using Outworld.Game.Extensions;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Outworld.Game.Utilities
{
    public static class Context
    {
        private readonly struct PrefabInfo
        {
            public readonly string Path;
            public readonly Transform Parent;
            public readonly bool IsEnabled;

            public PrefabInfo(string path, Transform parent, bool isEnabled)
            {
                this.Path = path;
                this.Parent = parent;
                this.IsEnabled = isEnabled;
            }
        }

        // ReSharper disable once HeapView.ObjectAllocation.Evident
        private static readonly Dictionary<Type, object> instances = new Dictionary<Type, object>();

        // ReSharper disable once HeapView.ObjectAllocation.Evident
        private static readonly Dictionary<Type, PrefabInfo> prefabs = new Dictionary<Type, PrefabInfo>();

        public static T InstantiatePrefab<T>(Transform parent = null) where T : MonoBehaviour
        {
            var prefabInfo = prefabs.GetValueOrDefault(typeof(T));
            var prefabInstance = Object.Instantiate(Resources.Load<T>(prefabInfo.Path), parent ?? prefabInfo.Parent);
            prefabInstance.gameObject.SetActive(prefabInfo.IsEnabled);

            return prefabInstance;
        }

        public static T Get<T>() where T : class
        {
            return (T) instances.GetValueOrDefault(typeof(T));
        }

        public static void RegisterInstance<T>(T element) where T : class
        {
            #if UNITY_EDITOR
            if (element == null)
            {
                Debug.LogError($"Context::RegisterInstance '{nameof(T)}' is null");
                return;
            }
            #endif

            instances.Add(typeof(T), element);
        }

        public static void RegisterPrefab<T>(string path, Transform parent = null, bool isEnabled = true) where T : MonoBehaviour
        {
            #if UNITY_EDITOR
            var prefab = Resources.Load<T>(path);

            if (prefab == null)
            {
                Debug.LogError($"Context::RegisterPrefab cannot find prefab of type '{nameof(T)}' at path '{path}'");
                return;
            }
            #endif

            prefabs.Add(typeof(T), new PrefabInfo(path, parent, isEnabled));
        }
    }
}