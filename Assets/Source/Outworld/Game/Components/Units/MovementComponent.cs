using System.Diagnostics.CodeAnalysis;
using Outworld.Game.Input;
using Outworld.Game.Movement;
using UnityEngine;

namespace Outworld.Game.Components.Units
{
    [RequireComponent(typeof(BoxCollider2D))]
    public class MovementComponent : MonoBehaviour
    {
        [SerializeField]
        private GroundMovementBehaviourSettings groundMovementBehaviourSettings;

        public MovementBehaviourType movementBehaviourType;

        private GroundMovementBehaviour groundMovementBehaviour;
        private CustomMovementBehaviour customMovementBehaviour;
        private MovementBehaviour currentMovementBehaviour;
        private MovementSystem movementSystem;

        [SuppressMessage("ReSharper", "HeapView.DelegateAllocation")]
        [SuppressMessage("ReSharper", "HeapView.ObjectAllocation.Evident")]
        private void Start()
        {
            this.groundMovementBehaviourSettings.Prepare();

            var boxCollider2D = GetComponent<BoxCollider2D>();

            this.movementSystem = new MovementSystem();
            this.groundMovementBehaviour = new GroundMovementBehaviour(transform, boxCollider2D, this.movementSystem, this.groundMovementBehaviourSettings);
            this.customMovementBehaviour = new CustomMovementBehaviour(transform, boxCollider2D, this.movementSystem, this.groundMovementBehaviourSettings);

            if (this.movementBehaviourType == MovementBehaviourType.Ground)
            {
                SwitchToGroundMovementBehaviour();
            }
            else
            {
                SwitchToCustomMovementBehaviour();
            }
        }

        private void Update()
        {
            this.currentMovementBehaviour.Update(Time.smoothDeltaTime);
        }

        public bool IsGrounded()
        {
            return this.movementSystem.CollisionBottom;
        }

        public bool IsAtTheWall()
        {
            return this.movementSystem.CollisionLeft || this.movementSystem.CollisionRight;
        }

        public void EnableCollision()
        {
            this.movementSystem.IsCollisionEnabled = true;
        }

        public void DisableCollision()
        {
            this.movementSystem.IsCollisionEnabled = false;
        }

        public float GetGravity()
        {
            return this.groundMovementBehaviour.GetGravity();
        }

        public Vector2 GetVelocity()
        {
            return this.currentMovementBehaviour.Velocity;
        }

        public void SetImpulse(Vector2 impulse)
        {
            this.currentMovementBehaviour.AddImpulse(impulse);
        }

        public void SetVelocity(Vector2 velocity)
        {
            this.currentMovementBehaviour.Velocity = velocity;
        }

        public void AddVelocity(Vector2 velocity)
        {
            this.currentMovementBehaviour.Velocity += velocity;
        }

        public void SetInput(InputState input)
        {
            this.currentMovementBehaviour.SetInput(input);
            ;
        }

        public void SwitchToGroundMovementBehaviour()
        {
            SwitchMovementBehaviour(this.groundMovementBehaviour);
        }

        public void SwitchToCustomMovementBehaviour()
        {
            SwitchMovementBehaviour(this.customMovementBehaviour);
        }

        private void SwitchMovementBehaviour(MovementBehaviour movementBehaviour)
        {
            if (this.currentMovementBehaviour == movementBehaviour)
            {
                return;
            }

            this.currentMovementBehaviour?.Disable();
            this.currentMovementBehaviour = movementBehaviour;
            this.currentMovementBehaviour.Enable();
        }
    }
}