using Outworld.Game.Managers;
using Outworld.Game.Utilities;
using UnityEngine;
using UnityEngine.UI;

namespace Outworld.Game.UI.Views
{
    public class OptionsView : View
    {
        [SerializeField]
        private Button resumeButton;

        [SerializeField]
        private Button settingsButton;

        [SerializeField]
        private Button keybindsButton;

        [SerializeField]
        private Button mainMenuButton;

        [SerializeField]
        private Button exitButton;

        private void Start()
        {
            this.resumeButton.onClick.AddListener(OnResumeButtonClicked);
            this.settingsButton.onClick.AddListener(OnSettingsButtonClicked);
            this.keybindsButton.onClick.AddListener(OnKeybindsButtonClicked);
            this.mainMenuButton.onClick.AddListener(OnMainMenuButtonClicked);
            this.exitButton.onClick.AddListener(OnExitButtonClicked);
        }

        protected override void OnShow()
        {
            this.resumeButton.Select();
        }

        private void OnResumeButtonClicked()
        {
            Hide();
        }

        private void OnSettingsButtonClicked()
        {
            Hide();
        }

        private void OnKeybindsButtonClicked()
        {
            Hide();
        }

        private void OnMainMenuButtonClicked()
        {
            Hide();
            Context.Get<GameStateManager>().ToMainMenu();
        }

        private void OnExitButtonClicked()
        {
            Application.Quit();
        }
    }
}