using System;
using Outworld.Game.Items;
using Outworld.Game.UI.Elements;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Outworld.Game.UI.Views
{
    public class InventoryViewSlot : Clickable
    {
        public event Action<InventoryViewSlot> SlotClickEvent;
        public event Action<InventoryViewSlot> SlotPointerEnterEvent;
        public event Action<InventoryViewSlot> SlotPointerExitEvent;

        public int index;
        public int x;
        public int y;
        public RectTransform container;
        public Item Item;
        public Image icon;
        public Image outline;

        private int cellSize;
        private int cellSpacing;

        [SerializeField]
        private TextMeshProUGUI text;

        public void Construct(Item item, int index, int x, int y, int cellSize, int cellSpacing)
        {
            this.index = index;
            this.x = x;
            this.y = y;
            this.cellSize = cellSize;
            this.cellSpacing = cellSpacing;
            this.text.text = index.ToString();

            ChangeItem(item);
        }

        public void ChangeItem(Item item)
        {
            this.Item = item;

            if (this.Item.IsEmpty())
            {
                this.icon.gameObject.SetActive(false);
                return;
            }

            this.container.sizeDelta = new Vector2(
                this.Item.Width * this.cellSize + this.cellSpacing * (this.Item.Width - 1),
                this.Item.Height * this.cellSize + this.cellSpacing * (this.Item.Height - 1)
            );

            this.icon.sprite = Resources.Load<Sprite>(this.Item.Icon);
            this.icon.SetNativeSize();
            this.icon.gameObject.SetActive(true);
        }

        public override void OnPointerEnter(PointerEventData eventData)
        {
            base.OnPointerEnter(eventData);

            SlotPointerEnterEvent?.Invoke(this);
        }

        public override void OnPointerExit(PointerEventData eventData)
        {
            base.OnPointerExit(eventData);

            SlotPointerExitEvent?.Invoke(this);
        }

        public override void OnSelect(BaseEventData eventData)
        {
            base.OnSelect(eventData);

            SlotPointerEnterEvent?.Invoke(this);
        }

        public override void OnDeselect(BaseEventData eventData)
        {
            base.OnDeselect(eventData);

            SlotPointerExitEvent?.Invoke(this);
        }

        public override void OnPointerClick(PointerEventData eventData)
        {
            base.OnPointerClick(eventData);

            SlotClickEvent?.Invoke(this);
        }

        public override void OnSubmit(BaseEventData eventData)
        {
            base.OnSubmit(eventData);

            SlotClickEvent?.Invoke(this);
        }
    }
}