namespace Outworld.Game.Movement
{
    public enum MovementBehaviourType
    {
        None,
        Ground,
        Jetpack,
        Kamikaze,
    }
}