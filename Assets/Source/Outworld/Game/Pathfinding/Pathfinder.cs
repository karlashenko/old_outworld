using Unity.Collections;
using Unity.Jobs;
using UnityEngine;

namespace Outworld.Game.Pathfinding
{
    public class Pathfinder : MonoBehaviour
    {
        [SerializeField] private WaypointGenerator waypointGenerator;

        public PathfinderNode? FindNextNode(Vector2 origin, Vector2 target)
        {
            var path = FindPath(origin, target, Allocator.TempJob);

            if (!path.HasValue)
            {
                return null;
            }

            var node = path.Value.Length > 1 ? path.Value[1] : new PathfinderNode?();
            path.Value.Dispose();

            return node;
        }

        public NativeList<PathfinderNode>? FindPath(Vector2 origin, Vector2 target, Allocator allocator = Allocator.Persistent)
        {
            var originWaypoint = this.waypointGenerator.FindClosestWalkableToWorldPoint(origin);
            var targetWaypoint = this.waypointGenerator.FindClosestWalkableToWorldPoint(target);

            if (originWaypoint.IsEmpty() || targetWaypoint.IsEmpty() || originWaypoint == targetWaypoint)
            {
                return null;
            }

            var job = new PathfinderJob();
            job.Origin = originWaypoint;
            job.Target = targetWaypoint;
            job.Waypoints = this.waypointGenerator.GetWaypoints();
            job.Connections = this.waypointGenerator.GetConnections();
            job.Result = new NativeList<PathfinderNode>(16, allocator);
            job.Schedule().Complete();

            if (job.Result.Length > 1)
            {
                return job.Result;
            }

            job.Result.Dispose();
            return null;

        }
    }
}