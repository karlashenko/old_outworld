using System;
using Outworld.Game.Items;
using UnityEngine;

namespace Outworld.Game.Components.Units
{
    public class InventoryComponent : MonoBehaviour
    {
        private static readonly int[] indexCache = new int[16];

        public event Action<Item, int, InventoryComponent> ItemPickEvent;
        public event Action<int, InventoryComponent> ItemRemoveEvent;

        public ItemSlot[] Slots;

        public int width;
        public int height;

        public void Construct(int width, int height)
        {
            this.width = width;
            this.height = height;

            this.Slots = new ItemSlot[this.width * this.height];

            for (var i = 0; i < this.Slots.Length; i++)
            {
                var x = i % this.width;
                var y = i / this.width;

                this.Slots[i].X = x;
                this.Slots[i].Y = y;
                this.Slots[i].Index = i;
                this.Slots[i].Clear();
            }
        }

        public bool Pickup(Item item)
        {
            var index = FindEmptySlot(item);

            if (index == -1)
            {
                return false;
            }

            Pickup(item, index);
            return true;
        }

        private void Pickup(Item item, int index)
        {
            this.Slots[index].Occupy(item, index);
            ItemPickEvent?.Invoke(item, index, this);

            foreach (var i in GetOccupyingSlotIndices(item, index))
            {
                if (i == index)
                {
                    continue;
                }

                this.Slots[i].Occupy(Item.Empty, index);
                ItemPickEvent?.Invoke(Item.Empty, i, this);
            }
        }

        public bool MoveOrSwap(int indexFrom, int indexTo)
        {
            return Move(indexFrom, indexTo) || Swap(indexFrom, indexTo);
        }

        private bool Move(int indexFrom, int indexTo)
        {
            Debug.Log($"Move {indexFrom} => {indexTo}");

            if (indexFrom == indexTo)
            {
                return true;
            }

            if (!this.Slots[indexTo].IsEmpty() && this.Slots[indexFrom].ParentIndex != this.Slots[indexTo].ParentIndex)
            {
                return false;
            }

            var itemFrom = this.Slots[this.Slots[indexFrom].ParentIndex].Item;

            Remove(indexFrom);

            if (!CanBePlaced(itemFrom, indexTo))
            {
                Pickup(itemFrom, indexFrom);
                return false;
            }

            MaybeRemove(indexFrom);
            Pickup(itemFrom, indexTo);

            return true;
        }

        private bool Swap(int indexFrom, int indexTo)
        {
            Debug.Log($"Swap {indexFrom} => {indexTo}");

            if (this.Slots[indexFrom].ParentIndex == this.Slots[indexTo].ParentIndex)
            {
                return false;
            }

            var indexFromParent = this.Slots[indexFrom].ParentIndex;
            var itemFrom = this.Slots[indexFromParent].Item;

            var firstBlockingParent = -1;

            foreach (var i in GetOccupyingSlotIndices(itemFrom, indexTo))
            {
                var x = i % this.width;
                var y = i / this.width;

                if (!XYInBounds(x, y))
                {
                    return false;
                }

                if (this.Slots[i].IsEmpty() || this.Slots[i].ParentIndex == indexFromParent)
                {
                    continue;
                }

                if (firstBlockingParent != -1 && firstBlockingParent != this.Slots[i].ParentIndex)
                {
                    return false;
                }

                if (firstBlockingParent == -1)
                {
                    firstBlockingParent = this.Slots[i].ParentIndex;
                }
            }

            if (firstBlockingParent == -1)
            {
                return false;
            }

            var firstBlockingItem = this.Slots[firstBlockingParent].Item;

            Remove(indexFromParent);
            Remove(firstBlockingParent);

            if (CanBePlaced(firstBlockingItem, indexFromParent))
            {
                Pickup(firstBlockingItem, indexFromParent);

                if (CanBePlaced(itemFrom, indexTo))
                {
                    Pickup(itemFrom, indexTo);
                    return true;
                }
            }

            MaybeRemove(indexFromParent);
            MaybeRemove(firstBlockingParent);

            Pickup(itemFrom, indexFromParent);
            Pickup(firstBlockingItem, firstBlockingParent);

            return false;
        }

        private void MaybeRemove(int index)
        {
            if (!this.Slots[index].IsEmpty())
            {
                Remove(index);
            }
        }

        public bool Remove(Item item)
        {
            var index = FindContainingSlot(item);

            if (index == -1)
            {
                return false;
            }

            Remove(index);
            return true;
        }

        private void Remove(int index)
        {
            index = this.Slots[index].ParentIndex;

            foreach (var i in GetOccupyingSlotIndices(this.Slots[index].Item, index))
            {
                this.Slots[i].Clear();
                ItemRemoveEvent?.Invoke(i, this);
            }
        }

        private ReadOnlySpan<int> GetOccupyingSlotIndices(Item item, int index)
        {
            var count = 0;

            for (var yOffset = 0; yOffset < item.Height; yOffset++)
            {
                for (var xOffset = 0; xOffset < item.Width; xOffset++)
                {
                    var x = this.Slots[index].X + xOffset;
                    var y = this.Slots[index].Y - yOffset;
                    var i = x + y * this.width;

                    indexCache[count] = i;
                    count += 1;
                }
            }

            return new ReadOnlySpan<int>(indexCache, 0, count);
        }

        private int FindContainingSlot(Item item)
        {
            for (var i = 0; i < this.Slots.Length; i++)
            {
                if (this.Slots[i].Item == item)
                {
                    return i;
                }
            }

            return -1;
        }

        private int FindEmptySlot(Item item)
        {
            foreach (var slot in this.Slots)
            {
                if (CanBePlaced(item, slot.Index))
                {
                    return slot.Index;
                }
            }

            return -1;
        }

        private bool CanBePlaced(Item item, int index, bool ignoreOccupied = false)
        {
            foreach (var i in GetOccupyingSlotIndices(item, index))
            {
                var x = i % this.width;
                var y = i / this.width;

                if (XYInBounds(x, y) && (ignoreOccupied || this.Slots[i].IsEmpty()))
                {
                    continue;
                }

                return false;
            }

            return true;
        }

        private bool XYInBounds(int x, int y)
        {
            return x >= 0 && x < this.width && y >= 0 && y < this.height;
        }
    }
}