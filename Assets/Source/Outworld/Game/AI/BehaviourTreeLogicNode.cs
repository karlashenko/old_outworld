﻿using UnityEngine;

namespace Outworld.Game.AI
{
    public abstract class BehaviourTreeLogicNode : BehaviourTreeNode
    {
        // ReSharper disable once HeapView.ObjectAllocation.Evident
        protected static readonly Collider2D[] ColliderCache = new Collider2D[32];

        protected readonly BehaviourTreePropertiesData Properties;

        protected BehaviourTreeLogicNode(BehaviourTreePropertiesData properties)
        {
            this.Properties = properties;
        }
    }
}