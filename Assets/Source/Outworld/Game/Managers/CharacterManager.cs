using System.Collections.Generic;
using Outworld.Game.Components.Units;
using UnityEngine;

namespace Outworld.Game.Managers
{
    public class CharacterManager
    {
        public readonly List<CharacterComponent> Characters = new List<CharacterComponent>();

        public CharacterManager()
        {
        }

        public void DestroyAll()
        {
            foreach (var character in this.Characters)
            {
                Object.Destroy(character.gameObject);
            }

            this.Characters.Clear();
        }
    }
}