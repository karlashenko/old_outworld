﻿using Outworld.Game.Extensions;
using TMPro;
using UnityEngine;

namespace Outworld.Game.UI.Elements
{
    public class Tooltip : MonoBehaviour
    {
        [SerializeField] private RectTransform rectTransform;
        [SerializeField] private RectTransform parentRectTransform;
        [SerializeField] private TextMeshProUGUI title;
        [SerializeField] private TextMeshProUGUI text;

        public void Show(string text, RectTransform rect)
        {
            Show("", text, rect);
        }

        public void Show(string title, string text, RectTransform rect)
        {
            gameObject.SetActive(true);

            this.title.gameObject.SetActive(!string.IsNullOrEmpty(title));
            this.title.text = title;

            this.text.gameObject.SetActive(!string.IsNullOrEmpty(text));
            this.text.text = text;

            this.rectTransform.MoveTooltip(rect, this.parentRectTransform);
            this.rectTransform.ClampPositionToParent();
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}