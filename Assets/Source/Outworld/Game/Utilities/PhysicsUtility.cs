using System;
using UnityEngine;

namespace Outworld.Game.Utilities
{
    public static class PhysicsUtility
    {
        // ReSharper disable once HeapView.ObjectAllocation.Evident
        private static readonly RaycastHit2D[] raycastCache = new RaycastHit2D[64];

        // ReSharper disable once HeapView.ObjectAllocation.Evident
        private static readonly Collider2D[] colliderCache = new Collider2D[64];

        public static ReadOnlySpan<Collider2D> OverlapCircle(Vector2 origin, float radius, LayerMask mask)
        {
            var targets = Physics2D.OverlapCircleNonAlloc(origin, radius, colliderCache, mask);

            return new ReadOnlySpan<Collider2D>(colliderCache, 0, targets);
        }

        public static ReadOnlySpan<Collider2D> RaycastAll(Vector3 origin, Vector2 direction, float distance, LayerMask mask)
        {
            var targets = Physics2D.RaycastNonAlloc(origin, direction, raycastCache, distance, mask);

            return new ReadOnlySpan<Collider2D>(colliderCache, 0, targets);
        }

        public static RaycastHit2D RaycastMultiple(Vector2 origin, Vector2 direction, Vector2 offsetDirection, int count, float spacing, float distance, LayerMask mask)
        {
            for (var i = 0; i < count; i++)
            {
                var offset = origin + offsetDirection * (spacing * i);
                raycastCache[i] = Physics2D.Raycast(offset, direction, distance, mask);

                #if UNITY_EDITOR
                var hitDistance = raycastCache[i].collider ? raycastCache[i].distance : distance;
                Debug.DrawRay(offset, direction * hitDistance, Color.green);
                #endif
            }

            var minDistance = 100f;
            var index = 0;

            for (var i = 0; i < count; i++)
            {
                if (!raycastCache[i] || raycastCache[i].distance >= minDistance)
                {
                    continue;
                }

                minDistance = raycastCache[i].distance;
                index = i;
            }

            return raycastCache[index];
        }
    }
}