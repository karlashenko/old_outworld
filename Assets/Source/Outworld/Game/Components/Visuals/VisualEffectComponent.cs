using Outworld.Game.Extensions;
using Outworld.Game.Memory;
using UnityEngine;

namespace Outworld.Game.Components.Visuals
{
    public class VisualEffectComponent : MonoBehaviour, VisualEffectComponent.IStopCallback, IPoolableComponent<VisualEffectComponent>
    {
        public interface IStopCallback
        {
            void OnVisualEffectStopped(VisualEffectComponent visualEffect);
        }

        private const string DeathAnimationName = "death";
        private const string BirthAnimationName = "birth";
        private const float MaxParticleSystemDuration = 10f;

        [SerializeField] private Animator animator;
        [SerializeField] private new ParticleSystem particleSystem;
        [SerializeField] private SpriteRenderer[] spriteRenderers;

        private ResourcesObjectPool<VisualEffectComponent> pool;
        private bool lifetimeCountdownStarted;
        private float lifetimeCountdown;
        private IStopCallback stopCallback;

        public void OnCreated(ResourcesObjectPool<VisualEffectComponent> pool)
        {
            this.pool = pool;
        }

        public void OnTaken()
        {
            if (this.animator)
            {
                this.animator.Play(BirthAnimationName);
            }

            if (this.particleSystem)
            {
                this.particleSystem.Play();
            }

            ShowSprites();
            ResetLifetimeCountdown();
        }

        public void OnReleased()
        {
        }

        public void Release()
        {
            this.pool.Release(this);
        }

        public void StopAndRelease()
        {
            Stop(this);
        }

        public void OnVisualEffectStopped(VisualEffectComponent visualEffect)
        {
            Release();
        }

        public void ShowSprites()
        {
            SetSpritesAlpha(1);
        }

        public void HideSprites()
        {
            SetSpritesAlpha(0);
        }

        private void SetSpritesAlpha(float alpha)
        {
            if (this.spriteRenderers == null)
            {
                return;
            }

            foreach (var spriteRenderer in this.spriteRenderers)
            {
                spriteRenderer.color = spriteRenderer.color.With(a: alpha);
            }
        }

        public void Stop(IStopCallback callback)
        {
            this.stopCallback = callback;

            if (this.lifetimeCountdownStarted)
            {
                return;
            }

            if (this.animator)
            {
                this.lifetimeCountdown = this.animator.GetAnimationClipLength(DeathAnimationName);
                this.animator.Play(DeathAnimationName);
            }

            if (this.particleSystem)
            {
                if (this.particleSystem.main.loop)
                {
                    this.particleSystem.Stop();
                }

                this.lifetimeCountdown = Mathf.Min(this.particleSystem.main.duration, MaxParticleSystemDuration);
            }

            this.lifetimeCountdownStarted = true;
        }

        private void Update()
        {
            if (!this.lifetimeCountdownStarted)
            {
                return;
            }

            this.lifetimeCountdown -= Time.deltaTime;

            if (this.lifetimeCountdown > 0)
            {
                return;
            }

            ResetLifetimeCountdown();

            this.stopCallback.OnVisualEffectStopped(this);
        }

        private void ResetLifetimeCountdown()
        {
            this.lifetimeCountdownStarted = false;
            this.lifetimeCountdown = 0;
        }
    }
}