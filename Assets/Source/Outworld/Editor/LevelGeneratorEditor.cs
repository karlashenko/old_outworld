using Outworld.Game.Levels;
using UnityEditor;
using UnityEngine;

namespace Outworld.Editor
{
    [CustomEditor(typeof(LevelGenerator))]
    public class LevelGeneratorEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            GUILayout.Space(25);

            if (GUILayout.Button("Generate"))
            {
                ((LevelGenerator) target).Generate();
            }

            GUILayout.Space(25);

            base.OnInspectorGUI();
        }
    }
}