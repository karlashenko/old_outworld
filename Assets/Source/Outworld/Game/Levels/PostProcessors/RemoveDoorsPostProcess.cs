using System.Linq;
using Edgar.Unity;
using UnityEngine;

namespace Outworld.Game.Levels.PostProcessors
{
    [CreateAssetMenu(menuName = "Edgar/Post Process - Remove Door Tiles", fileName = "RemoveDoorTiles")]
    public class RemoveDoorsPostProcess : DungeonGeneratorPostProcessBase
    {
        public override void Run(GeneratedLevel level, LevelDescription levelDescription)
        {
            RemoveWallsFromDoors(level);
        }

        private static void RemoveWallsFromDoors(GeneratedLevel level)
        {
            var walls = level.GetSharedTilemaps().Single(x => x.name == "Collision");

            foreach (var room in level.GetRoomInstances())
            {
                foreach (var door in room.Doors)
                {
                    foreach (var point in door.DoorLine.GetPoints())
                    {
                        walls.SetTile(point + room.Position, null);
                    }
                }
            }
        }
    }
}