using Outworld.Game.Memory;
using UnityEngine;

namespace Outworld.Game.Components
{
    public interface IPoolableComponent<T> where T : Component, IPoolableComponent<T>
    {
        void OnCreated(ResourcesObjectPool<T> pool);

        void OnTaken();

        void OnReleased();

        void Release();
    }
}