using Outworld.Data;
using Outworld.Game.Components.Visuals;
using Outworld.Game.Extensions;
using Outworld.Game.Memory;
using Outworld.Game.Utilities;
using UnityEngine;

namespace Outworld.Game.Weapons
{
    public class HitscanWeapon : Weapon
    {
        private readonly Configuration configuration;

        public HitscanWeapon(in WeaponData data, GameObject owner) : base(in data, owner)
        {
            this.configuration = Context.Get<Configuration>();
        }

        protected override void OnStart(Transform origin, Vector2 direction, float pitch)
        {
        }

        protected override void OnShoot(Transform origin, Vector2 direction, float pitch)
        {
            var originParticles = ResourcesPool.Get<VisualEffectComponent>(this.Data.OriginParticlesPrefab);
            originParticles.transform.parent = origin;
            originParticles.transform.localPosition = Vector3.zero;
            originParticles.transform.localRotation = Quaternion.identity;
            originParticles.StopAndRelease();

            Hitscan(origin.position, direction, pitch);
        }

        private void Hitscan(Vector2 origin, Vector2 direction, float pitch)
        {
            var hit = Physics2D.Raycast(origin, direction, this.Data.Range, this.configuration.collisionMask | this.configuration.unitMask);

            if (hit.collider == null)
            {
                return;
            }

            if (this.configuration.unitMask.ContainsLayer(hit.collider.gameObject.layer))
            {
                HitUnit(hit.collider.gameObject);
            }
            else
            {
                HitEnvironment(hit.point);
            }

            var targetParticles = ResourcesPool.Get<VisualEffectComponent>(this.Data.TargetParticlesPrefab);
            targetParticles.transform.position = hit.point;
            targetParticles.transform.rotation = Quaternion.Euler(0, 0, pitch - 180);
            targetParticles.StopAndRelease();
        }

        protected override void OnStop(Transform origin, Vector2 direction, float pitch)
        {
        }

        protected override void OnUpdate(Transform origin, Vector2 direction, float pitch, float deltaTime)
        {
        }
    }
}