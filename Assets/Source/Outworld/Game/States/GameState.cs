using System;
using UnityEngine;

namespace Outworld.Game.States
{
    public abstract class GameState
    {
        private static Action<GameState> onAnyGameStateEnter;
        private static Action<GameState> onAnyGameStateExit;

        public void Enter()
        {
            Debug.Log($"{GetType().Name}::Enter()");
            OnEnter();
            onAnyGameStateEnter?.Invoke(this);
        }

        public void Exit()
        {
            Debug.Log($"{GetType().Name}::Exit()");
            OnExit();
            onAnyGameStateExit?.Invoke(this);
        }

        public void Destroy()
        {
            Debug.Log($"{GetType().Name}::Destroy()");
            OnDestroy();
        }

        protected virtual void OnEnter()
        {
        }

        protected virtual void OnExit()
        {
        }

        protected virtual void OnDestroy()
        {
        }

        public virtual void Update(float deltaTime)
        {
        }
    }
}