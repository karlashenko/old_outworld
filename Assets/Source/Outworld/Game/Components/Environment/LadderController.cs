using Outworld.Game.Components.Units;
using Outworld.Game.Extensions;
using UnityEngine;

namespace Outworld.Game.Components.Environment
{
    public class LadderController : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D other)
        {
            var movementController = other.GetComponent<MovementComponent>();

            if (movementController == null)
            {
                return;
            }

            movementController.transform.position = movementController.transform.position.With(transform.position.x);
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            var movementController = other.GetComponent<MovementComponent>();

            if (movementController == null)
            {
                return;
            }

            movementController.SwitchToGroundMovementBehaviour();
        }
    }
}