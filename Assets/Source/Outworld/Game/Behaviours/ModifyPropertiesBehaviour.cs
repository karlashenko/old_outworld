using System.Collections.Generic;
using Outworld.Data;
using Outworld.Game.Components.Units;
using Outworld.Game.Properties;
using Outworld.Game.Utilities;

namespace Outworld.Game.Behaviours
{
    public class ModifyPropertiesBehaviour : Behaviour
    {
        private readonly List<PropertyModifier> propertyModifiers = new List<PropertyModifier>();

        public ModifyPropertiesBehaviour(BehaviourData data) : base(data)
        {
            var propertyModifierCollection = Context.Get<Library>().PropertyModifiers;

            foreach (var propertyModifierId in data.PropertyModifiers)
            {
                this.propertyModifiers.Add(new PropertyModifier(propertyModifierCollection[propertyModifierId]));
            }
        }

        protected override void OnApply()
        {
            this.Target.GetComponent<PropertiesComponent>().AddModifiers(this.propertyModifiers);
        }

        protected override void OnRemove()
        {
            this.Target.GetComponent<PropertiesComponent>().RemoveModifiers(this.propertyModifiers);
        }
    }
}