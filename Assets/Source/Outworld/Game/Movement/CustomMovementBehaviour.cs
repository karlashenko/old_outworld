using UnityEngine;

namespace Outworld.Game.Movement
{
    public class CustomMovementBehaviour : MovementBehaviour
    {
        public CustomMovementBehaviour(Transform transform, BoxCollider2D collider, MovementSystem movementSystem, GroundMovementBehaviourSettings settings)
            : base(transform, collider, movementSystem, settings.impulseDamping)
        {
        }

        protected override void OnEnable()
        {
            this.MovementSystem.IsCollisionEnabled = false;
        }

        protected override void OnDisable()
        {
            this.MovementSystem.IsCollisionEnabled = true;
        }
    }
}