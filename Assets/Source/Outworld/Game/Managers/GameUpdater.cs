﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

namespace Outworld.Game.Managers
{
    public class GameUpdater : MonoBehaviour
    {
        public event Action StartEvent;
        public event Action CompleteEvent;

        [SerializeField]
        private string host;

        public void StartUpdate()
        {
            StartCoroutine(Download());
        }

        private IEnumerator Download()
        {
            StartEvent?.Invoke();

            yield return DownloadCoroutine($"{Application.streamingAssetsPath}/dataset.json", $"{this.host}/Download");
            yield return DownloadCoroutine($"{Application.streamingAssetsPath}/ru-RU.json", $"{this.host}/Download/I18n?locale=Ru");
            yield return DownloadCoroutine($"{Application.streamingAssetsPath}/en-US.json", $"{this.host}/Download/I18n?locale=En");

            CompleteEvent?.Invoke();
        }

        private static IEnumerable DownloadCoroutine(string url, string filename)
        {
            using var request = UnityWebRequest.Get(url);

            yield return request.SendWebRequest();

            if (request.result == UnityWebRequest.Result.Success)
            {
                File.WriteAllText(filename, request.downloadHandler.text);
            }
            else
            {
                Debug.LogError($"Network error: {url} {request.error}");
            }
        }
    }
}