using UnityEngine;

namespace Outworld.Game.Extensions
{
    public static class CameraExtensions
    {
        public static Vector3 ScreenToWorldPointRaycast(this Camera camera, Vector3 screenPoint)
        {
            return Physics.Raycast(camera.ScreenPointToRay(screenPoint), out var hit) ? hit.point : Vector3.zero;
        }
    }
}