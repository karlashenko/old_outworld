using System;

namespace Outworld.Game.AI
{
    [Serializable]
    public struct BehaviourTreePropertiesData
    {
        public float waitDuration;
        public float followTargetStopDistance;
        public float range;
    }
}