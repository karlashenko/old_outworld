namespace Outworld.Game.AI.Conditions
{
    public class IsTargetUnitInRange : BehaviourTreeLogicNode
    {
        public IsTargetUnitInRange(BehaviourTreePropertiesData properties) : base(properties)
        {
        }

        protected override BehaviourTreeStatus OnTick(float deltaTime, BehaviourTreeContext context)
        {
            var sqrMagnitude = (context.TargetUnit.transform.position - context.Unit.transform.position).sqrMagnitude;
            return sqrMagnitude < this.Properties.range * this.Properties.range ? BehaviourTreeStatus.Success : BehaviourTreeStatus.Failure;
        }
    }
}