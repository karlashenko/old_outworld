using System.Collections.Generic;
using System.Linq;
using Outworld.Data;

// ReSharper disable InconsistentNaming

namespace Outworld.Game
{
    public class Library
    {
        public readonly Dictionary<int, ItemData> Items;
        public readonly Dictionary<int, ItemTypeData> ItemTypes;
        public readonly Dictionary<int, WeaponData> Weapon;
        public readonly Dictionary<int, ProjectileData> Projectiles;
        public readonly Dictionary<int, PropertyModifierData> PropertyModifiers;
        public readonly Dictionary<int, PropertyData> Properties;
        public readonly Dictionary<int, EffectData> Effects;
        public readonly Dictionary<int, BehaviourData> Behaviours;
        public readonly Dictionary<int, ValidatorData> Validators;
        public readonly Dictionary<string, I18nData> I18n;

        public Library(Dataset dataset)
        {
            this.Items = dataset.Items.ToDictionary(x => x.Id);
            this.ItemTypes = dataset.ItemTypes.ToDictionary(x => x.Id);
            this.Weapon = dataset.Weapon.ToDictionary(x => x.Id);
            this.Projectiles = dataset.Projectiles.ToDictionary(x => x.Id);
            this.Properties = dataset.Properties.ToDictionary(x => x.Id);
            this.Effects = dataset.Effects.ToDictionary(x => x.Id);
            this.Behaviours = dataset.Behaviours.ToDictionary(x => x.Id);
            this.Validators = dataset.Validators.ToDictionary(x => x.Id);
            this.I18n = dataset.I18n.ToDictionary(x => x.Key);
            this.PropertyModifiers = dataset.PropertyModifiers.ToDictionary(x => x.Id);
        }
    }
}