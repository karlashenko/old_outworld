using Outworld.Game.Managers;
using Outworld.Game.Utilities;
using UnityEngine;
using UnityEngine.UI;

namespace Outworld.Game.UI.Views
{
    public class LevelView : View
    {
        [SerializeField]
        private Button button;

        private void Start()
        {
            this.button.onClick.AddListener(OnButtonClicked);
        }

        private void OnDestroy()
        {
            this.button.onClick.RemoveAllListeners();
        }

        private void OnButtonClicked()
        {
            Context.Get<GameStateManager>().ToHub();
        }
    }
}