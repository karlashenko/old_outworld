using Outworld.Game.Managers;
using Outworld.Game.UI.Views;
using Outworld.Game.Utilities;
using UnityEngine;

namespace Outworld.Game.States
{
    public class HubGameState : GameState
    {
        private readonly HubManager hubManager;
        private readonly HubView hubView;

        public HubGameState()
        {
            this.hubManager = Context.Get<HubManager>();
            this.hubView = Context.InstantiatePrefab<HubView>();
        }

        protected override void OnEnter()
        {
            this.hubManager.gameObject.SetActive(true);
            // this.hubView.Show(PlayerInput.all[0]);
        }

        protected override void OnExit()
        {
            this.hubManager.gameObject.SetActive(false);
            // this.hubView.gameObject.SetActive(false);
        }

        protected override void OnDestroy()
        {
            Object.Destroy(this.hubView.gameObject);
        }

        public override void Update(float deltaTime)
        {
        }
    }
}