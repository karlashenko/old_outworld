using Unity.Collections;
using UnityEditor;
using UnityEngine;

namespace Outworld.Game.Pathfinding
{
    public class PathfinderTest : MonoBehaviour
    {
        [SerializeField] private WaypointGenerator waypointGenerator;
        [SerializeField] private Pathfinder pathfinder;
        [SerializeField] private bool drawGizmos;
        [SerializeField] private Transform origin;
        [SerializeField] private Transform target;

        private void OnDrawGizmos()
        {
            if (!this.drawGizmos)
            {
                return;
            }

            if (this.pathfinder == null || this.origin == null || this.target == null)
            {
                return;
            }

            var nodes = this.pathfinder.FindPath(this.origin.position, this.target.position);

            if (nodes.HasValue)
            {
                GizmoDrawPath(nodes.Value);
                nodes.Value.Dispose();
            }
        }

        public void GizmoDrawPath(NativeArray<PathfinderNode> nodes)
        {
            var style = new GUIStyle();
            style.alignment = TextAnchor.MiddleCenter;
            style.normal.textColor = Color.white;

            foreach (var node in nodes)
            {
                var color = Color.red;

                if (node.WaypointConnectionType == WaypointConnectionType.Fall)
                {
                    color = Color.yellow;
                }

                if (node.WaypointConnectionType == WaypointConnectionType.Walk)
                {
                    color = Color.green;
                }

                if (node.WaypointConnectionType == WaypointConnectionType.Jump)
                {
                    color = Color.blue;
                }

                Gizmos.color = color;
                Gizmos.DrawIcon(node.Position, "winbtn_mac_max@2x");
                Handles.Label(node.Position - new Vector2(0, 0.3f), $"{node.WaypointIndex.ToString()}\n{node.ParentWaypointIndex.ToString()}", style);

                if (node.ParentWaypointIndex == -1)
                {
                    continue;
                }

                Gizmos.DrawLine(node.Position, this.waypointGenerator.GetWaypointAt(node.ParentWaypointIndex).Position);
            }
        }
    }
}