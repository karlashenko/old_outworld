﻿using UnityEngine;

namespace Outworld.Game.Extensions
{
    public static class AnimatorExtensions
    {
        public static float GetAnimationClipLength(this Animator animator, string name)
        {
            return animator.GetAnimationClip(name)?.length ?? 0;
        }

        public static AnimationClip GetAnimationClip(this Animator animator, string name)
        {
            foreach (var clip in animator.runtimeAnimatorController.animationClips)
            {
                if (clip.name == name)
                {
                    return clip;
                }
            }

            return null;
        }
    }
}