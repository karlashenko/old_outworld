using Outworld.Game.AI;
using Outworld.Game.AI.Composites;
using Outworld.Game.AI.Tasks;
using UnityEngine;

namespace Outworld.Game.Components.Units
{
    public class BehaviourTreeComponent : MonoBehaviour
    {
        public BehaviourTree BehaviourTree { get; private set; }

        private void Start()
        {
            var properties = new BehaviourTreePropertiesData();
            properties.followTargetStopDistance = 1;
            properties.range = 10f;

            BehaviourTree = new BehaviourTreeBuilder()
                .StartContainer(new Sequence())
                .AddNode(new SetTargetUnitToClosestEnemyIfNotSet(properties))
                .StartContainer(new Selector())
                // .AddNode(new ShootAtTargetEntity(properties))
                .StartContainer(new Sequence())
                // .StartContainer(new Inverter())
                // .AddNode(new IsTargetEntityInRange(properties))
                // .EndContainer()
                .AddNode(new FollowTargetUnit(properties))
                .EndContainer()
                .EndContainer()
                .EndContainer()
                .Build(gameObject);
        }

        private void Update()
        {
            BehaviourTree.Tick(Time.deltaTime);
        }

        private void OnDrawGizmos()
        {
            BehaviourTree?.DrawGizmos();
        }
    }
}