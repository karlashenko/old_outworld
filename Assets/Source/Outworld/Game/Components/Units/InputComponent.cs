using Outworld.Game.Extensions;
using Outworld.Game.Input;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Outworld.Game.Components.Units
{
    public class InputComponent : MonoBehaviour
    {
        [SerializeField] private InputActionReference moveAction;
        [SerializeField] private InputActionReference jumpAction;
        [SerializeField] private InputActionReference dashAction;
        [SerializeField] private InputActionReference crouchAction;
        [SerializeField] private InputActionReference pointerAction;
        [SerializeField] private InputActionReference rightStickAction;
        [SerializeField] private InputActionReference action0Action;
        [SerializeField] private InputActionReference action1Action;
        [SerializeField] private InputActionReference action2Action;
        [SerializeField] private InputActionReference action3Action;
        [SerializeField] private InputActionReference action4Action;
        [SerializeField] private InputActionReference action5Action;

        private PlayerInput playerInput;
        private InputState state;
        private Vector2 pointer;

        public void Construct(PlayerInput playerInput)
        {
            this.playerInput = playerInput;
            this.playerInput.onControlsChanged += OnControlsChanged;
            this.playerInput.onActionTriggered += OnAction;

            this.state.LeftStickVector = Vector2.right;
            this.state.LeftStickAngle = this.state.LeftStickVector.DirectionToDegrees();

            this.state.RightStickVector = Vector2.right;
            this.state.RightStickAngle = this.state.RightStickVector.DirectionToDegrees();

            OnControlsChanged(this.playerInput);
        }

        public InputState GetState()
        {
            return this.state;
        }

        private void LateUpdate()
        {
            this.state.CleanupTriggers();
        }

        private void OnControlsChanged(PlayerInput input)
        {
            this.state.IsUsingGamepad = input.currentControlScheme == "Gamepad";
        }

        private void OnAction(InputAction.CallbackContext context)
        {
            if (context.action.id == this.dashAction.action.id)
            {
                this.state.DashPressed = context.performed;
            }

            if (context.action.id == this.crouchAction.action.id)
            {
                this.state.Crouch = context.performed;
            }

            if (context.action.id == this.action0Action.action.id)
            {
                this.state.Action0Pressed = context.performed;
                this.state.Action0Released = context.canceled;
            }

            if (context.action.id == this.action1Action.action.id)
            {
                this.state.Action1Pressed = context.performed;
                this.state.Action1Released = context.canceled;
            }

            if (context.action.id == this.action2Action.action.id)
            {
                this.state.Action2Pressed = context.performed;
                this.state.Action2Released = context.canceled;
            }

            if (context.action.id == this.action3Action.action.id)
            {
                this.state.Action3Pressed = context.performed;
                this.state.Action3Released = context.canceled;
            }

            if (context.action.id == this.action4Action.action.id)
            {
                this.state.Action4Pressed = context.performed;
                this.state.Action4Released = context.canceled;
            }

            if (context.action.id == this.action5Action.action.id)
            {
                this.state.Action5Pressed = context.performed;
                this.state.Action5Released = context.canceled;
            }

            if (context.action.id == this.jumpAction.action.id)
            {
                this.state.JumpPressed = context.performed;
                this.state.JumpReleased = context.canceled;
            }

            if (context.action.id == this.pointerAction.action.id)
            {
                this.state.Pointer = context.ReadValue<Vector2>();
            }

            if (context.action.id == this.rightStickAction.action.id)
            {
                OnRightStick(context);
            }

            if (context.action.id == this.moveAction.action.id)
            {
                OnMove(context);
            }
        }

        private void OnMove(InputAction.CallbackContext context)
        {
            var value = context.ReadValue<Vector2>();

            this.state.MoveLeft = value.x < 0;
            this.state.MoveRight = value.x > 0;
            this.state.MoveUp = value.y > 0;
            this.state.MoveDown = value.y < 0;

            if (value == Vector2.zero)
            {
                return;
            }

            this.state.LeftStickVector = value;
            this.state.LeftStickAngle = value.DirectionToDegrees();
        }

        private void OnRightStick(InputAction.CallbackContext context)
        {
            var value = context.ReadValue<Vector2>();

            if (value == Vector2.zero)
            {
                return;
            }

            this.state.RightStickVector = value;
            this.state.RightStickAngle = this.state.RightStickVector.DirectionToDegrees();
        }
    }
}