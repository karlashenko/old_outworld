using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Outworld.Game.UI.Elements
{
    public class Clickable : Selectable, IPointerClickHandler, ISubmitHandler
    {
        public event Action<Clickable> ClickEvent;

        public virtual void OnPointerClick(PointerEventData eventData)
        {
            if (eventData.button != PointerEventData.InputButton.Left)
            {
                return;
            }

            TriggerClick();
        }

        public virtual void OnSubmit(BaseEventData eventData)
        {
            TriggerClick();

            if (!IsActive() || !IsInteractable())
            {
                return;
            }

            DoStateTransition(SelectionState.Pressed, false);
            StartCoroutine(OnFinishSubmit());
        }

        private void TriggerClick()
        {
            if (!IsActive() || !IsInteractable())
            {
                return;
            }

            ClickEvent?.Invoke(this);
        }

        private IEnumerator OnFinishSubmit()
        {
            var fadeTime = colors.fadeDuration;
            var elapsedTime = 0f;

            while (elapsedTime < fadeTime)
            {
                elapsedTime += Time.unscaledDeltaTime;
                yield return null;
            }

            DoStateTransition(currentSelectionState, false);
        }
    }
}