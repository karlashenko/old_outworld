using System;
using System.Collections.Generic;
using System.Linq;
using Outworld.Data;
using Outworld.Data.Types;
using Outworld.Game.Localization;
using UnityEngine;

namespace Outworld.Game.Properties
{
    public class Property
    {
        public event Action<Property> ValueChangeEvent;

        public readonly I18NString Name;
        public readonly I18NString Description;
        public readonly PropertyData Data;

        private readonly List<PropertyModifier> modifiers = new List<PropertyModifier>();

        public Property(PropertyData data)
        {
            this.Data = data;
            this.Name = new I18NString(data.NameKey);
            this.Description = new I18NString(data.DescriptionKey);
        }

        public float GetValue()
        {
            var flat = this.Data.Min;

            foreach (var modifier in this.modifiers.Where(x => x.Data.Type == ModifierType.Add))
            {
                flat += modifier.Modify(flat);
            }

            var fraction = 0f;

            foreach (var modifier in this.modifiers.Where(x => x.Data.Type == ModifierType.Multiply))
            {
                fraction += modifier.Modify(flat);
            }

            return Mathf.Clamp(flat + fraction, this.Data.Min, this.Data.Max);
        }

        public void AddModifier(PropertyModifier modifier)
        {
            this.modifiers.Add(modifier);
            ValueChangeEvent?.Invoke(this);
        }

        public void RemoveModifier(PropertyModifier modifier)
        {
            this.modifiers.Remove(modifier);
            ValueChangeEvent?.Invoke(this);
        }
    }
}