﻿namespace Outworld.Game.AI
{
    public abstract class BehaviourTreeNode
    {
        private BehaviourTreeStatus status;

        public BehaviourTreeStatus GetLastStatus()
        {
            return this.status;
        }

        public BehaviourTreeStatus Tick(float deltaTime, BehaviourTreeContext context)
        {
            // TODO: Use indices for add and remove operations

            if (!context.RunningNodes.Contains(this))
            {
                context.RunningNodes.Add(this);
                OnStart(context);
            }

            this.status = OnTick(deltaTime, context);

            if (this.status != BehaviourTreeStatus.Running)
            {
                context.RunningNodes.Remove(this);
                OnStop(context);
            }

            return this.status;
        }

        public virtual void DrawGizmos()
        {
        }

        protected abstract BehaviourTreeStatus OnTick(float deltaTime, BehaviourTreeContext context);

        protected virtual void OnStart(BehaviourTreeContext context)
        {
        }

        protected virtual void OnStop(BehaviourTreeContext context)
        {
        }
    }
}