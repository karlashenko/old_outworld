using Outworld.Game.Internationalization;
using Outworld.Game.Localization;
using Outworld.Game.Utilities;
using TMPro;
using UnityEngine;

namespace Outworld.Game.UI.Elements
{
    public class I18NText : MonoBehaviour
    {
        [SerializeField]
        private string key;

        private void Start()
        {
            I18n.LocaleChangeEvent += OnLocaleChanged;

            OnLocaleChanged(Context.Get<I18n>());
        }

        private void OnDestroy()
        {
            I18n.LocaleChangeEvent -= OnLocaleChanged;
        }

        private void OnLocaleChanged(I18n i18N)
        {
            var text = GetComponent<TextMeshProUGUI>();

            if (text == null)
            {
                return;
            }

            text.text = i18N.Translate(this.key);
        }
    }
}