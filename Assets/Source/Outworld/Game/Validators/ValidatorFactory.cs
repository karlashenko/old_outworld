using System;
using System.Collections.Generic;
using Outworld.Data;
using Outworld.Data.Types;
using Outworld.Game.Utilities;

namespace Outworld.Game.Validators
{
    public class ValidatorFactory
    {
        private readonly Dictionary<int, ValidatorData> validators;

        public ValidatorFactory()
        {
            this.validators = Context.Get<Library>().Validators;
        }

        public Validator Create(int validatorId)
        {
            var data = this.validators[validatorId];

            switch (data.Type)
            {
                case ValidatorType.CasterHealthFraction:
                    break;
                case ValidatorType.TargetHealthFraction:
                    break;
                case ValidatorType.TargetIsAlly:
                    break;
                case ValidatorType.TargetIsEnemy:
                    break;
            }

            throw new ArgumentOutOfRangeException();
        }
    }
}