using System;
using UnityEngine;

namespace Outworld.Game.Pathfinding
{
    public readonly struct Waypoint : IEquatable<Waypoint>
    {
        public static readonly Waypoint Empty = new Waypoint(-1, default, WaypointType.Empty);

        public readonly int Index;
        public readonly Vector2 Position;
        public readonly bool IsEdge;
        public readonly bool IsSlope;
        public readonly WaypointType Type;

        public Waypoint(int index, Vector2 position, WaypointType type, bool isEdge = false, bool isSlope = false)
        {
            this.Index = index;
            this.Position = position;
            this.Type = type;
            this.IsEdge = isEdge;
            this.IsSlope = isSlope;
        }

        public bool IsEmpty()
        {
            return this.Index == Empty.Index;
        }

        public Waypoint WithIsEdge(bool isEdge)
        {
            return new Waypoint(this.Index, this.Position, this.Type, isEdge, this.IsSlope);
        }

        public Waypoint WithIsSlope(bool isSlope)
        {
            return new Waypoint(this.Index, this.Position, this.Type, this.IsEdge, isSlope);
        }

        public static bool operator ==(Waypoint a, Waypoint b)
        {
            return a.Equals(b);
        }

        public static bool operator !=(Waypoint a, Waypoint b)
        {
            return !a.Equals(b);
        }

        public override bool Equals(object other)
        {
            return other is Waypoint waypoint && waypoint.Equals(this);
        }

        public bool Equals(Waypoint other)
        {
            return other.Index == this.Index;
        }

        public override int GetHashCode()
        {
            return this.Index;
        }
    }
}