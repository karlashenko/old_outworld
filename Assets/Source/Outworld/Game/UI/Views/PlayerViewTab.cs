using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UI;

namespace Outworld.Game.UI.Views
{
    public class PlayerViewTab : MonoBehaviour, IPointerClickHandler
    {
        public event Action<PlayerViewTab> ClickEvent;

        [SerializeField]
        private View view;

        [SerializeField]
        private Image outline;

        public void Activate(PlayerInput playerInput)
        {
            this.outline.gameObject.SetActive(true);
            this.view.Show(playerInput);
        }

        public void Deactivate()
        {
            this.outline.gameObject.SetActive(false);
            this.view.Hide();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            ClickEvent?.Invoke(this);
        }
    }
}