namespace Outworld.Game.Utilities
{
    public static class BinaryMask
    {
        public static bool ContainsAny(int mask, int value)
        {
            return (mask & value) > 0;
        }

        public static bool Contains(int mask, int value)
        {
            return (mask & value) == value;
        }
    }
}