using System;
using UnityEngine;

namespace Outworld.Game.Movement
{
    [Serializable]
    public struct GroundMovementBehaviourSettings
    {
        public float gravity;
        public float jumpForce;
        public float jumpHeight;
        public float timeToJumpApex;
        public float moveSpeed;
        public float crouchSpeed;
        public float wallSlideSpeed;
        public float accelerationTimeAirborne;
        public float accelerationTimeGrounded;
        public float impulseDamping;
        public Vector2 wallJumpForceLeap;
        public Vector2 wallJumpForceClimb;
        public bool disableAirControl;
        public int airJumpCount;

        public void Prepare()
        {
            this.gravity = 2 * this.jumpHeight / (this.timeToJumpApex * this.timeToJumpApex);
            this.jumpForce = this.gravity * this.timeToJumpApex;
        }
    }
}