using System.Collections;
using System.Diagnostics;
using Outworld.Data;
using Outworld.Game.Effects;
using Outworld.Game.Extensions;
using Outworld.Game.Managers;
using Outworld.Game.Utilities;
using UnityEngine;

namespace Outworld.Game.Weapons
{
    public abstract class Weapon
    {
        public readonly WeaponData Data;

        protected readonly GameObject Owner;
        protected readonly CoroutineRunner CoroutineRunner;

        protected float ChargeFraction;

        private readonly Stopwatch chargeTimer;
        private readonly EffectFactory effectFactory;

        private bool isStarted;
        private float cooldownTimer;
        private float preparationTimer;

        protected Weapon(in WeaponData data, GameObject owner)
        {
            this.Data = data;
            this.Owner = owner;
            this.CoroutineRunner = Context.Get<CoroutineRunner>();

            this.chargeTimer = new Stopwatch();
            this.effectFactory = Context.Get<EffectFactory>();
        }

        public virtual void Equip()
        {
        }

        public virtual void UnEquip()
        {
        }

        public void Start(Transform origin, Vector2 direction, float pitch)
        {
            if (this.isStarted)
            {
                return;
            }

            this.preparationTimer = this.Data.PreparationTime;
            this.ChargeFraction = 0;
            this.chargeTimer.Restart();
            this.isStarted = true;

            OnStart(origin, direction, pitch);
        }

        public void Update(Transform origin, Vector2 direction, float pitch, float deltaTime)
        {
            if (!this.isStarted)
            {
                return;
            }

            OnUpdate(origin, direction, pitch, deltaTime);

            if (this.cooldownTimer > 0)
            {
                this.cooldownTimer -= deltaTime;
                return;
            }

            if (this.preparationTimer > 0)
            {
                this.preparationTimer -= deltaTime;
                return;
            }

            if (this.Data.IsCharging)
            {
                return;
            }

            this.preparationTimer = this.Data.PreparationTime;
            this.cooldownTimer = this.Data.CooldownTime;

            if (this.Data.ShotBarrageCount > 0)
            {
                this.CoroutineRunner.StartCoroutine(ShootBarrage(origin, direction, pitch));
            }
            else
            {
                Shoot(origin, direction, pitch);
            }
        }

        protected virtual void Shoot(Transform origin, Vector2 direction, float pitch)
        {
            if (this.Data.ShotSpreadCount == 0)
            {
                OnShoot(origin, direction, pitch);
                return;
            }

            var stride = this.Data.ShotSpreadDegrees / this.Data.ShotSpreadCount;
            var offset = this.Data.ShotSpreadCount * stride / 2f - stride / 2f;

            for (var i = 0; i < this.Data.ShotSpreadCount; i++)
            {
                var angle = pitch - offset + stride * i;
                OnShoot(origin, angle.DegreesToDirection(), angle);
            }
        }

        private IEnumerator ShootBarrage(Transform origin, Vector2 direction, float pitch)
        {
            for (var i = 0; i < this.Data.ShotBarrageCount; i++)
            {
                Shoot(origin, direction, pitch);
                yield return new WaitForSeconds(this.Data.ShotBarrageInterval);
            }
        }

        public void Stop(Transform origin, Vector2 direction, float pitch)
        {
            if (!this.isStarted)
            {
                return;
            }

            if (this.Data.IsCharging)
            {
                this.ChargeFraction = Mathf.Min((float) this.chargeTimer.Elapsed.TotalSeconds / this.Data.ChargeDurationSeconds, 1);
                Shoot(origin, direction, pitch);
            }
            else
            {
                OnStop(origin, direction, pitch);
            }

            this.isStarted = false;
        }

        protected void HitEnvironment(Vector2 target)
        {
            this.effectFactory.Create(this.Data.EffectId).Apply(this.Owner, target);
        }

        protected void HitUnit(GameObject target)
        {
            this.effectFactory.Create(this.Data.EffectId).Apply(this.Owner, target);
        }

        protected abstract void OnStart(Transform origin, Vector2 direction, float pitch);
        protected abstract void OnShoot(Transform origin, Vector2 direction, float pitch);
        protected abstract void OnStop(Transform origin, Vector2 direction, float pitch);
        protected abstract void OnUpdate(Transform origin, Vector2 direction, float pitch, float deltaTime);
    }
}