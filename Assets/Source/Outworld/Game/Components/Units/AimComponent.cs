using Outworld.Game.Extensions;
using UnityEngine;

namespace Outworld.Game.Components.Units
{
    public class AimComponent : MonoBehaviour
    {
        public float Pitch { get; private set; }
        public Vector2 PitchDirection { get; private set; }
        public int FacingDirection { get; private set; }

        [SerializeField] private Transform graphics;
        [SerializeField] private Transform aimTransform;
        [SerializeField] private Transform weaponSlot;
        [SerializeField] private LineRenderer lineRenderer;

        public Transform GetAimTransform()
        {
            return this.aimTransform;
        }

        public Transform GetWeaponSlot()
        {
            return this.weaponSlot;
        }

        public void AimFacing()
        {
            var direction = FacingDirection > 0 ? Vector2.right : Vector2.left;
            Aim(direction.DirectionToDegrees(), direction);
        }

        public void Aim(Vector3 target)
        {
            var direction = (target - this.aimTransform.position).normalized;
            Aim(direction.DirectionToDegrees(), direction);
        }

        public void Aim(float pitch, in Vector2? direction = null)
        {
            this.Pitch = pitch;
            PitchDirection = direction ?? pitch.DegreesToDirection();
            FacingDirection = pitch > 90 || pitch < -90 ? -1 : 1;

            this.graphics.localEulerAngles = new Vector3(0, FacingDirection > 0 ? 0 : 180, 0);

            if (this.aimTransform == null)
            {
                return;
            }

            this.aimTransform.localEulerAngles = new Vector3(0, FacingDirection > 0 ? 0 : 180, pitch);
            this.aimTransform.localScale = this.aimTransform.localScale.With(y: FacingDirection);
        }

        private void Update()
        {
            this.lineRenderer.SetPosition(0, this.weaponSlot.transform.position);
            this.lineRenderer.SetPosition(1, this.weaponSlot.transform.position + (Vector3) (PitchDirection * 200f));
        }
    }
}