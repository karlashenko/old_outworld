using System.Collections.Generic;
using Outworld.Data;
using Outworld.Game.Extensions;
using Outworld.Game.Utilities;
using UnityEngine;

namespace Outworld.Game.Weapons
{
    public class BeamWeapon : Weapon
    {
        private readonly Configuration configuration;

        private readonly List<Beam> beams;

        public BeamWeapon(in WeaponData data, GameObject owner) : base(in data, owner)
        {
            this.configuration = Context.Get<Configuration>();
            this.beams = new List<Beam>(this.Data.ShotSpreadCount);

            for (var i = 0; i < this.Data.ShotSpreadCount; i++)
            {
                this.beams.Add(new Beam(this.Data.Range, this.Data.BeamReflectionCount, this.Data.BeamPrefab, this.Data.OriginParticlesPrefab, this.Data.TargetParticlesPrefab));
            }
        }

        public override void Equip()
        {
            // ...
        }

        public override void UnEquip()
        {
            foreach (var beam in this.beams)
            {
                beam.Dispose();
            }

            this.beams.Clear();
        }

        protected override void OnStart(Transform origin, Vector2 direction, float pitch)
        {
            foreach (var beam in this.beams)
            {
                beam.OnStart(origin, direction, pitch);
            }
        }

        protected override void OnShoot(Transform origin, Vector2 direction, float pitch)
        {
        }

        protected override void Shoot(Transform origin, Vector2 direction, float pitch)
        {
            foreach (var beam in this.beams)
            {
                foreach (var point in beam.HitPoints)
                {
                    HitEnvironment(point);
                }

                foreach (var unit in beam.HitUnits)
                {
                    HitUnit(unit);
                }
            }
        }

        protected override void OnUpdate(Transform origin, Vector2 direction, float pitch, float deltaTime)
        {
            var stride = this.Data.ShotSpreadDegrees / this.Data.ShotSpreadCount;
            var offset = this.Data.ShotSpreadCount * stride / 2f - stride / 2f;

            for (var i = 0; i < this.Data.ShotSpreadCount; i++)
            {
                var angle = pitch - offset + stride * i;
                this.beams[i].OnUpdate(origin, angle.DegreesToDirection(), angle, deltaTime);
            }
        }

        protected override void OnStop(Transform origin, Vector2 direction, float pitch)
        {
            foreach (var beam in this.beams)
            {
                beam.OnStop(origin, direction, pitch);
            }
        }
    }
}