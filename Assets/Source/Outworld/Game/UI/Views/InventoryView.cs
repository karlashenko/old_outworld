using System.Collections.Generic;
using System.Linq;
using Outworld.Game.Components.Units;
using Outworld.Game.Items;
using Outworld.Game.UI.Elements;
using Outworld.Game.Utilities;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

namespace Outworld.Game.UI.Views
{
    public class InventoryView : View
    {
        [SerializeField]
        private GridLayoutGroup grid;

        [SerializeField]
        private InventoryViewSlot slotPrefab;

        [SerializeField]
        private int cellSize;

        [SerializeField]
        private int cellSpacing;

        [SerializeField]
        private Image dragImage;

        [SerializeField]
        private RectTransform dragContainer;

        private readonly List<InventoryViewSlot> highlightedSlotViews = new List<InventoryViewSlot>();
        private readonly List<InventoryViewSlot> slotViews = new List<InventoryViewSlot>();

        private InventoryViewSlot draggingSlot;
        private Item draggingItem = Item.Empty;
        private int draggingItemSlotIndex = -1;
        private PlayerInput playerInput;
        private InventoryComponent inventory;

        public void Construct(PlayerInput playerInput, InventoryComponent inventory)
        {
            this.playerInput = playerInput;

            this.inventory = inventory;
            this.inventory.ItemPickEvent += ItemPickEvent;
            this.inventory.ItemRemoveEvent += ItemRemoveEvent;

            this.grid.constraint = GridLayoutGroup.Constraint.FixedColumnCount;
            this.grid.constraintCount = inventory.width;
            this.grid.spacing = new Vector2(this.cellSpacing, this.cellSpacing);
            this.grid.cellSize = new Vector2(this.cellSize, this.cellSize);

            foreach (var slot in this.inventory.Slots)
            {
                var slotView = Instantiate(this.slotPrefab, this.grid.transform);
                slotView.SlotClickEvent += SlotClickEvent;
                slotView.SlotPointerEnterEvent += SlotPointerEnterEvent;
                slotView.SlotPointerExitEvent += SlotPointerExitEvent;
                slotView.Construct(slot.Item, slot.Index, slot.X, slot.Y, this.cellSize, this.cellSpacing);

                this.slotViews.Add(slotView);
            }

            foreach (var slot in this.inventory.Slots)
            {
                if (slot.Item.IsEmpty())
                {
                    continue;
                }

                ItemPickEvent(slot.Item, slot.Index, this.inventory);
            }

            CreateSlotNavigation();
        }

        private void OnDestroy()
        {
            this.inventory.ItemPickEvent -= ItemPickEvent;
            this.inventory.ItemRemoveEvent -= ItemRemoveEvent;

            foreach (var slotView in this.slotViews)
            {
                slotView.SlotClickEvent -= SlotClickEvent;
                slotView.SlotPointerEnterEvent -= SlotPointerEnterEvent;
                slotView.SlotPointerExitEvent -= SlotPointerExitEvent;
                Destroy(slotView.gameObject);
            }

            this.slotViews.Clear();
        }

        private void OnEnable()
        {
            if (this.slotViews.Count == 0)
            {
                return;
            }

            this.slotViews[0].Select();
        }

        private void OnDisable()
        {
            Context.Get<Tooltip>().Hide();
            StopDrag();
        }

        private void ItemPickEvent(Item item, int index, InventoryComponent inventory)
        {
            this.slotViews[index].ChangeItem(item);
        }

        private void ItemRemoveEvent(int index, InventoryComponent inventory)
        {
            this.slotViews[index].ChangeItem(Item.Empty);
        }

        private void SlotPointerEnterEvent(InventoryViewSlot slotView)
        {
            var parentIndex = this.inventory.Slots[slotView.index].ParentIndex;

            if (parentIndex >= 0)
            {
                var parentSlot = this.slotViews[parentIndex];
                Context.Get<Tooltip>().Show(parentSlot.Item.Icon, parentSlot.container);
            }

            if (!this.dragContainer.gameObject.activeSelf)
            {
                return;
            }

            this.dragContainer.position = slotView.container.position;
        }

        private void SlotPointerExitEvent(InventoryViewSlot slot)
        {
            if (this.inventory.Slots[slot.index].ParentIndex >= 0)
            {
                Context.Get<Tooltip>().Hide();
            }
        }

        private void SlotClickEvent(InventoryViewSlot slotView)
        {
            if (this.draggingItemSlotIndex == -1)
            {
                if (this.inventory.Slots[slotView.index].IsEmpty())
                {
                    return;
                }

                this.draggingSlot = this.slotViews[this.inventory.Slots[slotView.index].ParentIndex];
                this.draggingSlot.outline.gameObject.SetActive(true);

                this.draggingItemSlotIndex = slotView.index;
                this.draggingItem = this.draggingSlot.Item;

                this.dragImage.sprite = Resources.Load<Sprite>(this.draggingItem.Icon);
                this.dragImage.SetNativeSize();

                this.dragContainer.sizeDelta = this.draggingSlot.container.sizeDelta;
                this.dragContainer.gameObject.SetActive(true);

                SlotPointerEnterEvent(slotView);

                return;
            }

            if (!this.inventory.MoveOrSwap(this.draggingItemSlotIndex, slotView.index))
            {
                return;
            }

            StopDrag();
        }

        private void StopDrag()
        {
            if (!this.dragContainer.gameObject.activeSelf)
            {
                return;
            }

            this.draggingSlot.outline.gameObject.SetActive(false);
            this.draggingSlot = null;

            this.draggingItem = Item.Empty;
            this.draggingItemSlotIndex = -1;

            this.dragContainer.gameObject.SetActive(false);
        }

        private void CreateSlotNavigation()
        {
            for (var i = 0; i < this.inventory.Slots.Length; i++)
            {
                var x = i % this.inventory.width;
                var y = i / this.inventory.width;

                var indexTop = x + (y + 1) * this.inventory.width;
                var indexDown = x + (y - 1) * this.inventory.width;
                var indexLeft = x - 1 + y * this.inventory.width;
                var indexRight = x + 1 + y * this.inventory.width;

                if (y == 0)
                {
                    indexDown = x + (this.inventory.height - 1) * this.inventory.width;
                }

                if (y == this.inventory.height - 1)
                {
                    indexTop = x;
                }

                if (x == 0)
                {
                    indexLeft = i + (this.inventory.width - 1);
                }

                if (x == this.inventory.width - 1)
                {
                    indexRight = i - (this.inventory.width - 1);
                }

                this.slotViews[i].navigation = new Navigation
                {
                    mode = Navigation.Mode.Explicit,
                    selectOnUp = this.slotViews.ElementAtOrDefault(indexTop),
                    selectOnDown = this.slotViews.ElementAtOrDefault(indexDown),
                    selectOnLeft = this.slotViews.ElementAtOrDefault(indexLeft),
                    selectOnRight = this.slotViews.ElementAtOrDefault(indexRight),
                    wrapAround = false,
                };
            }
        }
    }
}