using Outworld.Data;
using Outworld.Game.Utilities;
using Outworld.Game.Validators;
using UnityEngine;

namespace Outworld.Game.Effects
{
    public class SearchAreaEffect : Effect
    {
        private readonly Effect effect;
        private readonly Validator validator;
        private readonly Configuration configuration;

        public SearchAreaEffect(EffectData data) : base(data)
        {
            this.effect = Context.Get<EffectFactory>().Create(data.SearchAreaEffectId);
            this.validator = Context.Get<ValidatorFactory>().Create(data.SearchAreaValidatorId);
            this.configuration = Context.Get<Configuration>();
        }

        public override void CreateSnapshot(GameObject caster, GameObject target)
        {
            this.effect.CreateSnapshot(caster, target);
        }

        protected override void OnApply(GameObject caster, GameObject target)
        {
            OnApply(caster, target.transform.position);
        }

        protected override void OnApply(GameObject caster, Vector3 target)
        {
            var affected = 0;

            foreach (var collider in PhysicsUtility.OverlapCircle(target, this.Data.SearchAreaRadius, this.configuration.unitMask))
            {
                if (!this.validator.Validate(caster, collider.gameObject))
                {
                    continue;
                }

                this.effect.Apply(caster, collider.gameObject);
                affected += 1;

                if (this.Data.SearchAreaTargetLimit > 0 && affected >= this.Data.SearchAreaTargetLimit)
                {
                    break;
                }
            }
        }
    }
}