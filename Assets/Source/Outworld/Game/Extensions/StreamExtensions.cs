﻿using System.IO;

namespace Outworld.Game.Extensions
{
    public static class StreamExtensions
    {
        public static void CopyTo(this FileStream stream, byte[] bytes)
        {
            var fileSize = stream.Length;

            if (fileSize > int.MaxValue)
            {
                throw new IOException("File size is too big.");
            }

            var index = 0;
            var count = (int) fileSize;

            while (count > 0)
            {
                var bytesRead = stream.Read(bytes, index, count);

                if (bytesRead == 0)
                {
                    throw new EndOfStreamException("Unexpected end of file stream.");
                }

                index += bytesRead;
                count -= bytesRead;
            }
        }
    }
}