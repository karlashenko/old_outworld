using Outworld.Game.Managers;
using Outworld.Game.Utilities;
using UnityEngine;
using UnityEngine.UI;

namespace Outworld.Game.UI.Views
{
    public class MainMenuView : View
    {
        [SerializeField]
        private Button playButton;

        [SerializeField]
        private Button settingsButton;

        [SerializeField]
        private Button exitButton;

        private void Start()
        {
            this.playButton.Select();

            this.playButton.onClick.AddListener(OnPlayButtonClicked);
            this.settingsButton.onClick.AddListener(OnSettingsButtonClicked);
            this.exitButton.onClick.AddListener(OnExitButtonClicked);
        }

        private void OnDestroy()
        {
            this.playButton.onClick.RemoveAllListeners();
            this.settingsButton.onClick.RemoveAllListeners();
            this.exitButton.onClick.RemoveAllListeners();
        }

        private void OnPlayButtonClicked()
        {
            Context.Get<GameStateManager>().ToCharacterSelection();
        }

        private void OnSettingsButtonClicked()
        {
        }

        private void OnExitButtonClicked()
        {
            Application.Quit();
        }
    }
}