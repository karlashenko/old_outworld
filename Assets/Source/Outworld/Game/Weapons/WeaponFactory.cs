using System;
using System.Collections.Generic;
using Outworld.Data;
using Outworld.Data.Types;
using Outworld.Game.Utilities;
using UnityEngine;

namespace Outworld.Game.Weapons
{
    public class WeaponFactory
    {
        private readonly Dictionary<int, WeaponData> weapon;

        public WeaponFactory()
        {
            this.weapon = Context.Get<Library>().Weapon;
        }

        public Weapon Create(int weaponId, GameObject owner)
        {
            var data = this.weapon[weaponId];

            switch (data.Type)
            {
                case WeaponType.Hitscan:
                    return new HitscanWeapon(data, owner);
                case WeaponType.Projectile:
                    return new ProjectileWeapon(data, owner);
                case WeaponType.Beam:
                    return new BeamWeapon(data, owner);
            }

            throw new ArgumentOutOfRangeException();
        }
    }
}