using System;
using System.Collections.Generic;
using Outworld.Data;
using Outworld.Data.Types;
using Outworld.Game.Utilities;

namespace Outworld.Game.Effects
{
    public class EffectFactory
    {
        private readonly Dictionary<int, EffectData> effects;

        public EffectFactory()
        {
            this.effects = Context.Get<Library>().Effects;
        }

        public Effect Create(int effectId)
        {
            var data = this.effects[effectId];

            switch (data.Type)
            {
                case EffectType.Damage:
                    return new DamageEffect(data);
                case EffectType.Set:
                    return new EffectSet(data);
                case EffectType.SearchArea:
                    return new SearchAreaEffect(data);
                case EffectType.Projectile:
                    break;
            }

            throw new ArgumentOutOfRangeException();
        }
    }
}