using UnityEngine;

namespace Outworld.Game.Weapons
{
    public class WeaponView : MonoBehaviour
    {
        [SerializeField] private Transform barrel;

        public Transform GetBarrel()
        {
            return this.barrel;
        }

        public void OnShoot()
        {
            // ...
        }
    }
}