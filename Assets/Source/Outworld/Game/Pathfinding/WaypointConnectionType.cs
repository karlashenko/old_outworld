namespace Outworld.Game.Pathfinding
{
    public enum WaypointConnectionType
    {
        Walk,
        Fall,
        Jump
    }
}