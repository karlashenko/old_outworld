using System;
using Outworld.Data;
using UnityEngine;

namespace Outworld.Game.Behaviours
{
    public abstract class Behaviour
    {
        public event Action<Behaviour> ApplyEvent;
        public event Action<Behaviour> ExpireEvent;
        public event Action<Behaviour> RemoveEvent;

        public readonly BehaviourData Data;

        protected GameObject Caster;
        protected GameObject Target;

        private bool isApplied;
        private float durationCounter;

        protected Behaviour(BehaviourData data)
        {
            this.Data = data;
        }

        public void Apply(GameObject caster, GameObject target)
        {
            if (this.isApplied)
            {
                Debug.LogError($"Trying to re-apply behaviour: {this.Data.NameKey}");
                return;
            }

            this.isApplied = true;
            this.Caster = caster;
            this.Target = target;

            this.durationCounter = this.Data.Duration;

            OnApply();
        }

        public void Remove()
        {
            if (!this.isApplied)
            {
                Debug.LogError($"Trying to remove not applied behaviour: {this.Data.NameKey}");
                return;
            }

            this.isApplied = false;

            OnRemove();
        }

        public void Update(float deltaTime)
        {
            if (!this.isApplied)
            {
                return;
            }

            if (this.Data.Duration > 0)
            {
                this.durationCounter -= deltaTime;

                if (this.durationCounter <= 0)
                {
                    // TODO: Should expire trigger remove event?
                    Expire();
                    Remove();
                    return;
                }
            }

            OnUpdate(deltaTime);
        }

        private void Expire()
        {
            OnExpire();
            ExpireEvent?.Invoke(this);
        }

        protected virtual void OnApply()
        {
        }

        protected virtual void OnExpire()
        {
        }

        protected virtual void OnRemove()
        {
        }

        protected virtual void OnUpdate(float deltaTime)
        {
        }
    }
}