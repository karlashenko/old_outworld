﻿namespace Outworld.Game.AI.Composites
{
    public class Parallel : BehaviourTreeNodeContainer
    {
        protected override BehaviourTreeStatus OnTick(float deltaTime, BehaviourTreeContext context)
        {
            var succeeded = 0;

            foreach (var node in this.Nodes)
            {
                var status = node.Tick(deltaTime, context);

                if (status == BehaviourTreeStatus.Failure)
                {
                    return status;
                }

                if (status == BehaviourTreeStatus.Success)
                {
                    succeeded += 1;
                }
            }

            return succeeded == this.Nodes.Count ? BehaviourTreeStatus.Success : BehaviourTreeStatus.Running;
        }
    }
}