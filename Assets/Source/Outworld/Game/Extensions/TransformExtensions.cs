﻿using UnityEngine;

namespace Outworld.Game.Extensions
{
    public static class TransformExtensions
    {
        public static void ChangeAnchors(this RectTransform transform, Vector2 anchorMin, Vector2 anchorMax)
        {
            var origin = transform.position;
            transform.anchorMin = anchorMin;
            transform.anchorMax = anchorMax;
            transform.position = origin;
        }

        public static void ChangePivot(this RectTransform rectTransform, Vector2 pivot)
        {
            var rectSize = rectTransform.rect.size;
            var deltaPivot = rectTransform.pivot - pivot;
            var deltaPosition = new Vector3(deltaPivot.x * rectSize.x, deltaPivot.y * rectSize.y);

            rectTransform.pivot = pivot;
            rectTransform.localPosition -= deltaPosition;
        }

        public static void MoveTooltip(this RectTransform transform, RectTransform target, RectTransform parent)
        {
            var targetPosition = target.position;
            var targetRect = target.rect;

            var signX = targetPosition.x > Screen.width / 2f ? 1 : -1;
            var signY = targetPosition.y > Screen.height / 2f ? 1 : -1;

            transform.pivot = new Vector2(
                targetPosition.x > Screen.width / 2f ? 1 : 0,
                targetPosition.y > Screen.height / 2f ? 1 : 0
            );

            var parentLocalScale = parent.localScale;

            var offset = new Vector3(
                targetRect.center.x + targetRect.width / 2f * -signX * parentLocalScale.x,
                targetRect.center.y + targetRect.height / 2f * signY * parentLocalScale.y
            );

            transform.position = targetPosition + offset;
        }

        public static void ClampPositionToParent(this RectTransform transform)
        {
            transform.ClampPositionToParent(transform.parent.GetComponent<RectTransform>());
        }

        public static void ClampPositionToParent(this RectTransform transform, RectTransform parent)
        {
            var clamped = transform.localPosition;

            var parentRect = parent.rect;
            var childRect = transform.rect;

            var minPosition = parentRect.min - childRect.min;
            var maxPosition = parentRect.max - childRect.max;

            clamped.x = Mathf.Clamp(clamped.x, minPosition.x, maxPosition.x);
            clamped.y = Mathf.Clamp(clamped.y, minPosition.y, maxPosition.y);

            transform.localPosition = clamped;
        }
    }
}