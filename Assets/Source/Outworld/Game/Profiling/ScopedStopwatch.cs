using System;
using System.Diagnostics;
using Debug = UnityEngine.Debug;

namespace Outworld.Game.Profiling
{
    public readonly struct ScopedStopwatch : IDisposable
    {
        private readonly string label;
        private readonly Stopwatch stopwatch;

        public ScopedStopwatch(string label)
        {
            this.label = label;

            this.stopwatch = new Stopwatch();
            this.stopwatch.Start();
        }

        public void Dispose()
        {
            this.stopwatch.Stop();
            Debug.Log($"{this.label} - {this.stopwatch.Elapsed.TotalMilliseconds.ToString("F2")}ms");
        }
    }
}