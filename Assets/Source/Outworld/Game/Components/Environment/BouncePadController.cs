using Outworld.Game.Components.Units;
using Outworld.Game.Extensions;
using UnityEngine;

namespace Outworld.Game.Components.Environment
{
    [RequireComponent(typeof(BoxCollider2D))]
    public class BouncePadController : MonoBehaviour
    {
        [SerializeField] private float force;

        private new BoxCollider2D collider;

        private void Start()
        {
            this.collider = GetComponent<BoxCollider2D>();
        }

        private void OnTriggerEnter2D(Collider2D collider)
        {
            var movementComponent = collider.GetComponent<MovementComponent>();

            if (movementComponent == null)
            {
                return;
            }

            var origin = Mathf.Abs(transform.localRotation.z) > 0 ? this.collider.GetTransformedTopCenter() : this.collider.GetTransformedBottomCenter();

            if (Vector2.Dot(transform.up, (collider.bounds.GetBottomCenter() - origin).normalized) < 0)
            {
                return;
            }

            var velocity = transform.up * this.force;

            movementComponent.SetVelocity(velocity.With(movementComponent.GetVelocity().x + velocity.x));
        }
    }
}