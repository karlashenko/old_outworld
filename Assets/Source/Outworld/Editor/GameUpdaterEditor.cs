﻿using Outworld.Game.Managers;
using UnityEditor;
using UnityEngine;

namespace Outworld.Editor
{
    [CustomEditor(typeof(GameUpdater))]
    public class GameUpdaterEditor : UnityEditor.Editor
    {
        private GameUpdater updater;
        private bool isUpdating;

        private void OnEnable()
        {
            this.updater = (GameUpdater) target;
            this.updater.StartEvent += OnUpdateStarted;
            this.updater.CompleteEvent += OnUpdateCompleted;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            GUILayout.Space(25);

            if (this.isUpdating)
            {
                GUILayout.Label("Updating...");
                return;
            }

            if (GUILayout.Button("Update"))
            {
                this.updater.StartUpdate();
            }
        }

        private void OnUpdateStarted()
        {
            this.isUpdating = true;
        }

        private void OnUpdateCompleted()
        {
            this.isUpdating = false;
        }
    }
}
