using UnityEngine;

namespace Outworld.Game.Managers
{
    public class CursorManager : MonoBehaviour
    {
        [SerializeField] private Texture2D cursor;

        private void Start()
        {
            Cursor.SetCursor(this.cursor, new Vector2(0.5f, 0.5f), CursorMode.Auto);
        }
    }
}