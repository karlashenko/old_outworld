using Outworld.Game.Managers;
using Outworld.Game.Utilities;
using UnityEngine;
using UnityEngine.UI;

namespace Outworld.Game.UI.Views
{
    public class HubView : View
    {
        [SerializeField]
        private Button button1;

        [SerializeField]
        private Button button2;

        private void Start()
        {
            this.button1.onClick.AddListener(OnButton1Clicked);
            this.button2.onClick.AddListener(OnButton2Clicked);
        }

        private void OnDestroy()
        {
            this.button1.onClick.RemoveAllListeners();
            this.button2.onClick.RemoveAllListeners();
        }

        private void OnButton1Clicked()
        {
            Context.Get<GameStateManager>().ToLevel();
        }

        private void OnButton2Clicked()
        {
            Context.Get<GameStateManager>().ToMainMenu();
        }
    }
}