﻿using System.Collections.Generic;
using UnityEngine;

namespace Outworld.Game.AI
{
    public class BehaviourTreeContext
    {
        public readonly GameObject Unit;
        public readonly List<BehaviourTreeNode> RunningNodes = new List<BehaviourTreeNode>();
        public readonly Dictionary<BehaviourTreeNode, int> RunningNodeIndices = new Dictionary<BehaviourTreeNode, int>();

        public Vector3? TargetPoint;
        public GameObject TargetUnit;
        public GameObject PreviousTargetUnit;

        public BehaviourTreeContext(GameObject unit)
        {
            this.Unit = unit;
        }
    }
}