using Outworld.Game.Utilities;
using UnityEngine;

namespace Outworld.Game.Managers
{
    public class CameraManager : MonoBehaviour
    {
        [SerializeField]
        private new Camera camera;

        [SerializeField]
        private Vector2 offset;

        [SerializeField]
        private float smoothingFactor;

        private CharacterManager characterManager;
        private float defaultScale;

        private void Start()
        {
            this.characterManager = Context.Get<CharacterManager>();
            this.defaultScale = this.camera.orthographicSize;
        }

        public void Update()
        {
            if (this.characterManager.Characters.Count == 0)
            {
                return;
            }

            var minX = Mathf.Infinity;
            var minY = Mathf.Infinity;
            var maxX = Mathf.NegativeInfinity;
            var maxY = Mathf.NegativeInfinity;

            foreach (var character in this.characterManager.Characters)
            {
                var characterPosition = character.transform.position;

                minX = Mathf.Min(minX, characterPosition.x);
                maxX = Mathf.Max(maxX, characterPosition.x);
                minY = Mathf.Min(minY, characterPosition.y);
                maxY = Mathf.Max(maxY, characterPosition.y);
            }

            var bounds = new Vector2(maxX - minX, maxY - minY);

            var targetScale = Mathf.Clamp(Mathf.Max(bounds.x, bounds.y) * 0.5f, this.defaultScale, this.defaultScale * 2f);
            var targetPoint = new Vector2(minX, minY) + bounds * 0.5f;

            this.camera.transform.position = Vector3.Lerp(this.camera.transform.position, targetPoint + this.offset, this.smoothingFactor * Time.deltaTime);
            this.camera.orthographicSize = Mathf.Lerp(this.camera.orthographicSize, targetScale, this.smoothingFactor * Time.deltaTime);
        }
    }
}