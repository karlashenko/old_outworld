using System.Collections.Generic;
using System.Linq;
using Outworld.Data.Types;
using Outworld.Game.Properties;
using Outworld.Game.Utilities;
using UnityEngine;

namespace Outworld.Game.Components.Units
{
    public class PropertiesComponent : MonoBehaviour
    {
        private readonly Dictionary<PropertyType, Property> properties = new Dictionary<PropertyType, Property>();

        private void Start()
        {
            foreach (var data in Context.Get<Library>().Properties.Values)
            {
                this.properties.Add(data.Type, new Property(data));
            }
        }

        public Property Get(int propertyId)
        {
            return this.properties.Values.First(x => x.Data.Id == propertyId);
        }

        public Property Get(PropertyType type)
        {
            return this.properties[type];
        }

        public IReadOnlyCollection<Property> Get()
        {
            return this.properties.Values;
        }

        public void AddModifiers(IEnumerable<PropertyModifier> modifiers)
        {
            foreach (var modifier in modifiers)
            {
                AddModifier(modifier);
            }
        }

        public void RemoveModifiers(IEnumerable<PropertyModifier> modifiers)
        {
            foreach (var modifier in modifiers)
            {
                RemoveModifier(modifier);
            }
        }

        public void AddModifier(PropertyModifier modifier)
        {
            Get(modifier.Data.PropertyId).AddModifier(modifier);
        }

        public void RemoveModifier(PropertyModifier modifier)
        {
            Get(modifier.Data.PropertyId).RemoveModifier(modifier);
        }
    }
}