namespace Outworld.Game.Items
{
    public class Item
    {
        public static readonly Item Empty = new Item(-1, 0, 0);

        public readonly int ID;
        public readonly int Width;
        public readonly int Height;
        public readonly string Icon;

        public Item(int id, int width, int height, string icon = null)
        {
            this.ID = id;
            this.Width = width;
            this.Height = height;
            this.Icon = icon;
        }

        public bool IsEmpty()
        {
            return this.ID == Empty.ID;
        }
    }
}