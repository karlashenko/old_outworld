using System;
using Edgar.Unity;
using Outworld.Game.Pathfinding;
using UnityEngine;

namespace Outworld.Game.Levels.PostProcessors
{
    [CreateAssetMenu(menuName = "Edgar/Post Process - Create Waypoints", fileName = "CreateWaypoints")]
    public class CreateWaypointsPostProcess : DungeonGeneratorPostProcessBase
    {
        public override void Run(GeneratedLevel level, LevelDescription levelDescription)
        {
            var waypointController = WaypointGenerator.Instance;

            if (waypointController == null)
            {
                waypointController = FindObjectOfType<WaypointGenerator>();
            }

            foreach (var compositeCollider2D in level.RootGameObject.GetComponentsInChildren<CompositeCollider2D>())
            {
                compositeCollider2D.gameObject.SetActive(false);
                compositeCollider2D.gameObject.SetActive(true);
            }

            var minX = int.MaxValue;
            var maxX = int.MinValue;
            var minY = int.MaxValue;
            var maxY = int.MinValue;

            foreach (var tilemap in level.GetSharedTilemaps())
            {
                var cellBounds = tilemap.cellBounds;

                if (cellBounds.size.x + cellBounds.size.y == 0)
                {
                    continue;
                }

                minX = Math.Min(minX, cellBounds.xMin);
                maxX = Math.Max(maxX, cellBounds.xMax);
                minY = Math.Min(minY, cellBounds.yMin);
                maxY = Math.Max(maxY, cellBounds.yMax);
            }

            waypointController.Generate(new Vector2(minX, minY) * waypointController.GetCellSize(), maxX - minX, maxY - minY);
        }
    }
}