﻿using UnityEngine;

namespace Outworld.Game.Utilities
{
    public class Singleton<T> : MonoBehaviour where T : Singleton<T>
    {
        private static T instance;

        public static T Get()
        {
            if (instance != null)
            {
                return instance;
            }

            instance = FindObjectOfType<T>();

            if (instance == null)
            {
                Debug.LogError($"An instance of {nameof(T)} is needed in the scene, but there is none.");
            }

            return instance;
        }
    }
}