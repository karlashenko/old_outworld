﻿using Outworld.Game.Extensions;

namespace Outworld.Game.AI.Composites
{
    public class Selector : BehaviourTreeNodeContainer
    {
        protected override BehaviourTreeStatus OnTick(float deltaTime, BehaviourTreeContext context)
        {
            var index = context.RunningNodeIndices.GetValueOrDefault(this, 0);

            for (var i = index; i < this.Nodes.Count; i++)
            {
                var status = this.Nodes[i].Tick(deltaTime, context);

                if (status == BehaviourTreeStatus.Failure)
                {
                    continue;
                }

                if (status == BehaviourTreeStatus.Running)
                {
                    context.RunningNodeIndices[this] = i;
                }

                return status;
            }

            return BehaviourTreeStatus.Failure;
        }

        protected override void OnStop(BehaviourTreeContext context)
        {
            context.RunningNodeIndices[this] = 0;
        }
    }
}
