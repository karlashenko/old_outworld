using UnityEngine;

namespace Outworld.Game.Extensions
{
    public static class BoxCollider2DExtensions
    {
        public static Vector3 GetTransformedBottomLeft(this BoxCollider2D collider)
        {
            return collider.transform.TransformPoint(new Vector3(0, 0));
        }

        public static Vector3 GetTransformedBottomRight(this BoxCollider2D collider)
        {
            return collider.transform.TransformPoint(new Vector3(collider.size.x, 0));
        }

        public static Vector3 GetTransformedTopRight(this BoxCollider2D collider)
        {
            return collider.transform.TransformPoint(new Vector3(collider.size.x, collider.size.y));
        }

        public static Vector3 GetTransformedTopLeft(this BoxCollider2D collider)
        {
            return collider.transform.TransformPoint(new Vector3(0, collider.size.y));
        }

        public static Vector3 GetTransformedTopCenter(this BoxCollider2D collider)
        {
            return collider.transform.TransformPoint(new Vector3(collider.size.x / 2, collider.size.y));
        }

        public static Vector3 GetTransformedBottomCenter(this BoxCollider2D collider)
        {
            return collider.transform.TransformPoint(new Vector3(collider.size.x / 2, 0));
        }
    }
}