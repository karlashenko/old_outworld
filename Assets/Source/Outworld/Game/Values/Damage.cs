using Outworld.Data.Types;
using UnityEngine;

namespace Outworld.Game.Values
{
    public readonly struct Damage
    {
        public readonly float Amount;
        public readonly DamageType Type;

        public Damage(float amount, DamageType type)
        {
            this.Amount = amount;
            this.Type = type;
        }

        public static Damage operator +(Damage damage, float amount)
        {
            return new Damage(damage.Amount + amount, damage.Type);
        }

        public static Damage operator -(Damage damage, float amount)
        {
            return new Damage(Mathf.Max(0, damage.Amount - amount), damage.Type);
        }

        public static Damage operator *(Damage damage, float amount)
        {
            return new Damage(damage.Amount * amount, damage.Type);
        }

        public static Damage operator /(Damage damage, float amount)
        {
            return new Damage(damage.Amount / amount, damage.Type);
        }
    }
}