﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Outworld.Game.AI
{
    public class BehaviourTreeBuilder
    {
        private readonly Stack<BehaviourTreeNodeContainer> containers = new Stack<BehaviourTreeNodeContainer>();

        private BehaviourTreeNodeContainer top;

        public BehaviourTreeBuilder StartContainer(BehaviourTreeNodeContainer container)
        {
            if (this.containers.Count > 0)
            {
                this.containers.Peek().AddNode(container);
            }

            this.containers.Push(container);

            return this;
        }

        public BehaviourTreeBuilder AddNode(BehaviourTreeNode node)
        {
            if (node == null)
            {
                throw new ArgumentNullException(nameof(node));
            }

            if (this.containers.Count <= 0)
            {
                throw new Exception("There is no parent node in the tree.");
            }

            this.containers.Peek().AddNode(node);

            return this;
        }

        public BehaviourTreeBuilder EndContainer()
        {
            this.top = this.containers.Pop();

            return this;
        }

        public BehaviourTree Build(GameObject unit)
        {
            if (this.top == null)
            {
                throw new Exception("Can't create a behaviour tree with zero nodes");
            }

            return new BehaviourTree(this.top, unit);
        }
    }
}