using System;
using Outworld.Data;
using UnityEngine;

namespace Outworld.Game.Effects
{
    public abstract class Effect
    {
        public event Action<Effect> FinishEvent;

        protected readonly EffectData Data;

        protected Effect(EffectData data)
        {
            this.Data = data;
        }

        public virtual void CreateSnapshot(GameObject caster, GameObject target)
        {
        }

        public void Apply(GameObject caster, GameObject target)
        {
            OnApply(caster, target);
        }

        public void Apply(GameObject caster, Vector3 target)
        {
            OnApply(caster, target);
        }

        protected virtual void OnApply(GameObject caster, GameObject target)
        {
            throw new NotImplementedException();
        }

        protected virtual void OnApply(GameObject caster, Vector3 target)
        {
            throw new NotImplementedException();
        }

        protected void TriggerFinish()
        {
            FinishEvent?.Invoke(this);
        }
    }
}