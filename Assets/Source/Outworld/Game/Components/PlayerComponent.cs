using Outworld.Game.Components.Units;
using Outworld.Game.UI.Views;
using Outworld.Game.Utilities;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.UI;

namespace Outworld.Game.Components
{
    public class PlayerComponent : MonoBehaviour
    {
        private static View activePlayerView;

        [SerializeField]
        private string actionMap;

        [Header("Input")]
        [SerializeField]
        private PlayerInput playerInput;

        [SerializeField]
        private MultiplayerEventSystem eventSystem;

        [SerializeField]
        private InputActionReference menuAction;

        [SerializeField]
        private InputActionReference optionsAction;

        [SerializeField]
        private InputActionReference cancelAction;

        [Header("Components")]
        [SerializeField]
        private Canvas canvas;

        [SerializeField]
        private OptionsView optionsView;

        private PlayerView playerView;

        public void Construct(CharacterComponent character)
        {
            if (this.playerView != null)
            {
                Destroy(this.playerView.gameObject);
            }

            this.playerView = Context.InstantiatePrefab<PlayerView>(this.canvas.transform);
            this.playerView.Construct(character);
        }

        private void Start()
        {
            this.playerInput.actions[this.menuAction.action.name].performed += OnMenuAction;
            this.playerInput.actions[this.optionsAction.action.name].performed += OnOptionsAction;
            this.playerInput.actions[this.cancelAction.action.name].performed += OnCancelAction;

            this.actionMap = this.playerInput.currentActionMap.name;

            View.AnyViewShowEvent += AnyViewShowEvent;
            View.AnyViewHideEvent += AnyViewHideEvent;
        }

        private void OnDestroy()
        {
            View.AnyViewShowEvent -= AnyViewShowEvent;
            View.AnyViewHideEvent -= AnyViewHideEvent;
        }

        private void OnOptionsAction(InputAction.CallbackContext context)
        {
            if (activePlayerView != null)
            {
                return;
            }

            ShowView(this.optionsView);
        }

        private void OnMenuAction(InputAction.CallbackContext context)
        {
            if (activePlayerView != null)
            {
                return;
            }

            ShowView(this.playerView);
        }

        private void OnCancelAction(InputAction.CallbackContext context)
        {
            if (activePlayerView == null)
            {
                return;
            }

            activePlayerView.Hide();
        }

        private void ActiveViewHideEvent(View view)
        {
            activePlayerView.HideEvent -= ActiveViewHideEvent;
            activePlayerView = null;

            this.eventSystem.gameObject.SetActive(false);
        }

        private void AnyViewShowEvent(View view)
        {
            this.actionMap = "UI";
            this.playerInput.SwitchCurrentActionMap(this.actionMap);
        }

        private void AnyViewHideEvent(View view)
        {
            this.actionMap = "Gameplay";
            this.playerInput.SwitchCurrentActionMap(this.actionMap);
        }

        private void ShowView(View view)
        {
            activePlayerView = view;
            activePlayerView.HideEvent += ActiveViewHideEvent;

            this.eventSystem.gameObject.SetActive(true);
            EventSystem.current = this.eventSystem;
            view.Show(this.playerInput);
        }

        public void EnableEventSystem()
        {
            this.eventSystem.gameObject.SetActive(true);
        }
    }
}