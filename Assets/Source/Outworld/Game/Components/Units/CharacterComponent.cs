using Outworld.Game.Extensions;
using Outworld.Game.Input;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Outworld.Game.Components.Units
{
    public class CharacterComponent : MonoBehaviour
    {
        public PlayerInput playerInput;

        private InputComponent input;
        private AimComponent aim;
        private MovementComponent movement;
        private WeaponComponent weapon;
        private new Camera camera;

        private void Start()
        {
            this.camera = Camera.main;

            this.input = GetComponent<InputComponent>();
            this.input.Construct(this.playerInput);

            this.aim = GetComponent<AimComponent>();
            this.movement = GetComponent<MovementComponent>();
            this.weapon = GetComponent<WeaponComponent>();
        }

        private void Update()
        {
            var inputState = this.input.GetState();

            UpdateAimInput(inputState);

            this.movement.SetInput(inputState);
            this.weapon.SetInput(inputState);
        }

        private void UpdateAimInput(in InputState inputState)
        {
            if (inputState.IsUsingGamepad)
            {
                this.aim.Aim(inputState.LeftStickAngle, inputState.LeftStickVector);
            }
            else
            {
                var direction = (this.camera.ScreenToWorldPoint(inputState.Pointer) - this.aim.GetAimTransform().position).normalized;
                this.aim.Aim(direction.DirectionToDegrees(), direction);
            }
        }
    }
}