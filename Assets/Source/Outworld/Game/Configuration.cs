using UnityEngine;

namespace Outworld.Game
{
    public class Configuration : MonoBehaviour
    {
        [Header("UI")]
        public Canvas gameplayCanvas;
        public Canvas overlayCanvas;
        public Canvas mainPlayerCanvas;

        [Header("Gameplay")]
        public LayerMask collisionMask;
        public LayerMask platformMask;
        public LayerMask unitMask;
    }
}